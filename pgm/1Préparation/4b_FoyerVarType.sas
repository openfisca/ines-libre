/************************************************/
/*          Programme 4b_FoyerVarType           */ 
/************************************************/

/********************************************************************************/
/* Table en entr�e :															*/
/*  travail.foyer&anr.															*/     
/*																				*/
/* Table en sortie :															*/
/*  dossier.foyerVarList														*/
/*																				*/
/* PLAN																			*/
/* 1\ Macro %ListeCasesAgregatsERFS												*/
/* 2\ Macro %ListeCasesIndividualisees											*/
/* 3\ Cr�ation de la table dossier.foyerVarList									*/
/********************************************************************************/

/********************************************************************************/
/*	Ce programme organise les cases fiscales utilis�es dans le code d'Ines. 	*/
/*	Confer page Wiki pour plus de d�tails. 										*/
/*	Pour chaque case on d�finit ainsi : 										*/
/*	- � quel agr�gat elle appartient, le cas �ch�ant 							*/
/*		-> macro %ListeCasesAgregatsERFS										*/
/*	- � quel individu de la d�claration elle se r�f�re							*/
/*		-> macro %ListeCasesIndividualisees										*/
/*	Cette 2�me macro fait aussi la distinction entre les cases qui sont des 	*/
/* montants (donc � vieillir) et celles qui n'en sont pas (� cocher, heures...).*/
/*																				*/
/* Toutes les cases ne font pas partie d'un agr�gat (premi�re macro). 			*/
/* En revanche, toutes doivent comporter un type (deuxi�me macro). 				*/
/*																				*/
/* Les cases n'existant plus dans la brochure la plus r�cente (et donc plus 	*/
/* utilis�es dans Ines), ont le type "Variable disparue". 						*/
/* Cela permet qu'aucun bug ne soit g�n�r� en utilisant des vieux mill�simes. 	*/
/********************************************************************************/



/************************************/
/* 1\ Macro %ListeCasesAgregatsERFS */
/************************************/

%Macro ListeCasesAgregatsERFS;
	/* Cette macro d�finit des macrovariables globales contenant la liste des cases d�finissant les diff�rents agr�gats de l'ERFS. 
	Objectif : recr�er des agr�gats au niveau du foyer pour ne pas avoir � revenir toujours aux cases fiscales pour certains calculs o� il n'est 
	pas n�cessaire de le faire. Cela r�duit l�g�rement le nombre de modifications � apporter lors de la mise � jour annuelle. 

	A chaque nouvelle ERFS N il faut mettre � jour ces agr�gats en faisant comme si l'on travaillait sur l'ERFS N+1, 
	puisque dans init_foyer on cr�e les cases fiscales qui apparaissent dans la d�claration de l'IR N+2 sur les revenus de l'ann�e N+1 
	(on n'aura ces cases dans l'ERFS que pour le mill�sime suivant). 
	La mise � jour de cette macro s'effectue sur la base du nouveau contour des agr�gats transmis par RPM chaque ann�e. 

	NB : Toutefois, comme les cases qu'on cr�e sont g�n�ralement vides, on ne fait pas trop d'erreur en se contentant de 
	mettre � jour avec les agr�gats ERFS de l'ann�e N (d�crits dans le bilan de production). */
	%global zsalfP zchofP zrstfP zpifP zalrfP zrtofP zragfP zragfN cbicf_ zricfP zricfN cbncf_ zrncfP zrncfN zvalfP zavffP
			zvamfP zvamfN zfonfP zfonfN caccf_ zracfP zracfN zetrfP zetrfN zalvfP zglofP zquofP zdivfP zdivfN;

	/* 1. Revenus d'activit� et de remplacement */
	/* 1.a. Traitements et salaires au sens large */
	%let zsalfP=_1aj _1bj _1cj _1dj _1ej _1fj
				_1aq _1bq _8by _8cy;			/* traitements et salaires */ 
	%let zchofP=_1ap _1bp _1cp _1dp _1ep _1fp;	/* pr�retraites et revenus du ch�mage */

	/* 1.b. Pensions, retraites et rentes */
	%let zrstfP=_1as _1bs _1cs _1ds _1es _1fs _1at _1bt;	/* retraites au sens stricte */ 
	%let zpifP=_1az _1bz _1cz _1dz;							/* pensions d'invalidit� */
	%let zalrfP=_1ao _1bo _1co _1do _1eo _1fo;				/* pensions alimentaires recues */ 
	%let zrtofP=_1aw _1bw _1cw _1dw;						/* rentes viag�res � titre on�reux */ 

	/* 1.c. Revenus des professions non-salari�es */
			/* Revenus agricoles */
	%let zragfP=_5hn _5in _5jn _5ho _5io _5jo 				/* r�gime du forfait */ 
				_5hd _5id _5jd	 							/* revenu cadastral des exploitations foresti�res */ 
				_5hb _5hh _5ib _5ih _5jb _5jh 
				_5hc _5hi _5ic _5ii _5jc _5ji   			/* r�gime du b�n�fice r�el */ 
				_5hm _5hz _5im _5iz _5jm _5jz;				/* revenu non imposable pour les jeunes agriculteurs */ 
	%let zragfN=_5hf _5hl _5if _5il _5jf _5jl;				/* d�ficits de l'ann�e d'imposition */ 

			/* Abattement forfaitaire pour les revenus industriels et commerciaux professionnels */ 
	%let cbicf_= _5ko _5kp _5lo _5lp _5mo _5mp _5ta _5tb _5ua _5ub _5va _5vb;
		
			/* Revenus industriels et commerciaux professionnels */ 
	%let zricfP=_5ta _5ua _5va
				_5tb _5ub _5vb	 							/* r�gime de l'auto-entrepreneur */ 
				_5kn _5ln _5mn								/* revenus nets exon�r�s du r�gime micro-entreprise */
				_5ko _5lo _5mo _5kp _5lp _5mp				/* chiffres d'aff. bruts du r�gime micro-entreprise */
				_5kb _5kh _5lb _5lh _5mb _5mh
				_5kc _5ki _5lc _5li _5mc _5mi
			    _5ha _5ka _5ia _5la _5ja _5ma		 		/* r�gime du b�n�fice r�el */
				_5ks _5ls _5ms; 							/* revenus non imposables des artisans p�cheurs */
	%let zricfN=_5kf _5kl _5lf _5ll _5mf _5ml
				_5qa _5qj _5ra _5rj _5sa _5sj;				/* d�ficits de l'ann�e d'imposition */

			/* Abattement forfaitaire pour les revenus non commerciaux professionnels */ 
	%let cbncf_=_5hq _5iq _5jq _5te _5ue _5ve;
				
		  	/* Revenus non commerciaux professionnels */ 
	%let zrncfP=_5te _5ue _5ve 						/* r�gime de l'auto-entrepreneur */ 
				_5hp _5ip _5jp 						/* revenus nets exon�r�s du r�gime micro BNC */ 
				_5hq _5iq _5jq 						/* recettes brutes du r�gime micro BNC */ 
				_5qb _5qh _5rb _5rh _5sb _5sh 
				_5qc _5qi _5rc _5ri _5sc _5si 		/* r�gime de la d�claration contr�l�e */ 
				_5ql _5rl _5sl 						/* revenus non imposables des jeunes cr�ateurs */
				_5qm _5rm    						/* indemnit�s des agents g�n�raux d'assurances */
				_5tf _5ti _5uf _5ui _5vf _5vi;	 	/* prospection commerciale */
	%let zrncfN=_5qe _5qk _5re _5rk _5se _5sk;		/* d�ficits de l'ann�e d'imposition */

	/* 2. Revenus du patrimoine */
	/* 2.a. Revenus des valeurs et capitaux mobiliers */
	%let zvalfP=_2ee _2dh;						/* Revenus de valeurs mob. soumis au pr�l�v. lib. */
	%let zavffP=_2ab _2ck _2bg _8ta;			/* Cr�dits d'imp�ts */
	%let zvamfP=_2dc _2fu _2ch _2ts _2go _2tr _2fa _2dm;

	/* 2.b. Revenus fonciers */
	%let zfonfP=_4ba _4be; /* TODO : si on veut faire comme l'agr�gat ERFS, c'est 0.7*_4be et non 4be */
	%let zfonfN=_4bb _4bc;
	
	/* 3. Revenus accessoires et per�us � l'�tranger */
	/* 3.a. Revenus accessoires */
			/* Abattement forfaitaire pour les loueurs en meubl� non professionnel */
	%let caccf_=_5no _5ng _5nj _5np _5nd _5ku _5oo _5og _5oj _5op _5od _5lu _5po _5pg _5pj _5pp _5pd _5mu;
	%let zracfP=_5na _5nm _5nk _5km _5oa _5om _5ok _5lm _5pa _5pm _5pk _5mm 
				_5nn _5on _5pn 
				_5no _5ng _5nj _5oo _5og _5oj _5po _5pg _5pj 
				_5np _5nd _5op _5od _5pp _5pd 
				_5nb _5nh _5ob _5oh _5pb _5ph 
				_5nc _5ni _5oc _5oi _5pc _5pi 
				_5th _5uh _5vh 
				_5ku _5lu _5mu 
				_5hk _5ik _5jk _5kk _5lk _5mk 
				_5jg _5sn _5rf _5ns _5sf _5os 
				_5sv _5sw _5sx  
				_5tc _5uc _5vc; 	
	%let zracfN=_5ny _5nz _5oy _5oz _5py _5pz
				_5nf _5nl _5of _5ol _5pf _5pl			
				_5jj _5sp _5rg _5nu _5sg _5ou;

	/* 3.b. Revenus per�us � l'�tranger */
	%let zetrfP=_8ti _1dy _1ey _1ac _1bc _1cc _1dc _1ec _1fc _1ah _1bh _1ch _1dh _1eh _1fh;
	%let zetrfN=_1ad _1bd _1cd _1dd _1ed _1fd;

	/* 4. pensions alimentaires vers�es */
	%let zalvfP=_6gi _6gj _6gk _6gl _6el _6em _6en _6eq _6gp _6gu;
	
	/* 5. Revenus exceptionnels non retenus pour l'enqu�te */
	%let zglofP=_1tv _1tw _1tx _1uv _1uw _1ux _1tt _1ut 
				_3vd _3vi _3vf _3sd _3si _3sf _3vj _3vk; /* gains de lev�e d'options */
	%let zquofP=_0xx; 							/* revenus d'activit� impos�s au quotient */
	%let zdivfP=	/* Plus-values */
				_3vg _3vq _3se _3sg _3sh _3sl _3sm _3va _3vb
				_3vc _3vm _3sj _3sk _3vt
				_3we _3wm _3wb _3vz _3wh _3sb _3sc _3wc _3wd _3vw
				_5hw _5iw _5jw _5hx _5ix _5jx _5he _5ie _5je _5kx _5lx _5mx _5kq _5lq _5mq
				_5ke _5le _5me _5nx _5ox _5px _5nq _5oq _5pq _5ne _5oe _5pe _5hv _5iv _5jv 
				_5hr _5ir _5jr _5qd _5rd _5sd _5ky _5ly _5my _5kv _5lv _5mv _5so _5nt _5ot _5hg _5ig;
	%let zdivfN=	/* Moins-values */
				_3vh _3vr _5kr _5lr _5mr _5kj _5lj _5mj _5nr _5or _5pr _5iu _5hs _5is _5js _5kz _5lz _5mz _5kw _5lw _5mw _5ju;
	%Mend ListeCasesAgregatsERFS;



/***************************************/
/* 2\ Macro %ListeCasesIndividualisees */
/***************************************/

%macro ListeCasesIndividualisees;

	/* Plan de la macro : 
	- on �tablit un certain nombre de listes de cases fiscales en fonction de l'individu
		-- on consid�re 5 individus (vous, conj, pac1, pac2, foyer=non individualisable)
		-- pour chaque individu, on distingue les cases avec des montants des autres (� cocher, ou nombre d'heures ...)
		-- cas particuliers : VarAns, ListVarDisp, ListVarInconnues */

	/***************************************/
	/* COMMENT METTRE A JOUR CETTE MACRO ? */
	/***************************************/
	/* La mise � jour va de paire avec celle du programme init_foyer : lorsqu'une nouvelle case appara�t ou change de nom, il faut lui donner un type
	en l'ajoutant dans la liste idoine selon l'individu qu'elle concerne et le type de case (� cocher ou non). 

	De la m�me mani�re que la macro %ListeCasesAgregatsERFS, cette macro doit �tre mise � jour chaque ann�e sur la base de la brochure fiscale la plus r�cente
	(anleg-1, �gale � anr1 dans l'utilisation la plus classique o� anleg=anref+2). 
	Etant donn� le peu d'utilisations que l'on fait de ces deux macros, on accepte la perte de pr�cision sur le pass�. */

	/* TODO : V�rifier que les listes d�finies ici n'incluent bien que des variables de montants sommables, 
	et non d'autres quantit�s (type "nombre d'heures"). */

	%global
		ListVousRev ListVousAutres
		ListConjRev ListConjAutres
		ListPac1Rev ListPac1Autres
		ListPac2Rev ListPac2Autres ListPac3Rev ListPac3Autres ListPac4Rev
		ListNonIndivRev ListNonIndivAutres
		nomsExplicites
		listVarDisp listVarInconnues;

	/* VARIABLES DE 'VOUS' */
	%let ListVousRev=	_1aj _1ap _1ak _1as _1az _1ao _1aq _1at
						_1ac _1ad _1ae _1ag _1ah
						_1tv _1tw _1tx _1tt _1ny
						_5nv _5nn _5no _5np _5nx _5nq _5nr _5nb _5nc _5nd _5na _5nf _5ng _5ny 
						_5ne _5nh _5ni _5nj _5nk _5nl _5nm _5nz 
						_5hn _5ho _5hd _5hw _5hx _5hb _5hh _5hc _5hi _5hf _5hl _5he _5hm _5hp 
						_5hq _5hv _5hr _5hs _5hy _5hz _5hg _5ha
						_5ta _5tb _5tc _5te _5tf _5ti _5th 
						_5kn _5ko _5kp _5kx _5kq _5kr _5kb _5kh _5kc _5ki _5kd _5kj _5ka _5kf 
						_5kl _5km _5ke _5ks _5ku _5ky _5kv _5kw 
						_5qa _5qj _5qb _5qc _5qe _5qd _5ql _5qm _5qh _5qi _5qk 
						_1dy _1sm _6rs _6ss _6ps _6qs _7ac _8by _5hk _5ik _5jg _5sn _5jj 
						_5sp _5so _5sv _5tc
						_5tj _5tk _5tl _5tm;
	%let ListVousAutres=_1ai _1ax _5nw _1av /* Nombre d'heures */;


	/* VARIABLES DE 'CONJ' */
	%let ListConjRev=	_1bj _1bp _1bk  _1bs _1bz _1bo _1bq _1bt
						_1bc _1bd _1be _1bg _1bh
						_1uv _1uw _1ux _1ut _1oy
						_5ov _5on _5oo _5op _5ox _5oq _5or _5ob _5oc _5od _5oa _5of _5og _5oy 
						_5oe _5oh _5oi _5oj _5ok _5ol _5om _5oz 
						_5in _5io _5id _5iw _5ix _5ib _5ih _5ic _5ii _5if _5il _5ie _5im _5ip 
						_5iq _5iv _5ir _5is _5iy _5iz _5ig _5ia 
						_5ua _5ub _5uc _5ue _5uf _5ui _5uh 
						_5ln _5lo _5lp _5lx _5lq _5lr _5lb _5lh _5lc _5li _5ld _5lj _5la _5lf 
						_5ll _5lm _5le _5ls _5lu _5ly _5lv _5lw _5lz
						_5ra _5rj _5rb _5rc _5re _5rd _5rl _5rm _5rh _5ri _5rk
						_1ey _1dn _6rt _6st _6pt _6qt _7ae _8cy _5jk _5kk _5rf _5ns _5rg 
						_5nu _5nt _5sw _5uc 
						_5uj _5uk _5ul _5um;
	%let ListConjAutres=_1bi _1bx _5ow _1bv;

	/* VARIABLES DE 'PAC1' */
	%let ListPac1Rev=	_1cj _1cp _1ck _1cv _1cb _1cs _1cz _1co 
						_1cc _1cd _1ce _1cg _1ch
						_6gi _6el _6ru _6su _6pu _6qu 
						_7ag
						_5pv _5jn _5jo _5jd _5jw _5jx _5jb _5jh _5jc _5ji _5jf _5jl _5je _5jm _5va _5vb _5mn 
						_5mo _5mp _5mx _5mq _5mr _5mb _5mh _5mc _5mi _5mj _5ja _5ma _5mf _5ml _5mm _5sa 
						_5sj _5me _5ms _5vc _5pn _5po _5pp _5px _5pq _5pr _5pb _5ph _5pc _5pi _5pd _5pj _5pa 
						_5pk _5pf _5pl _5pg _5pm _5py _5pz _5pe _5ve _5jp _5jq _5jv _5jr _5js _5sb _5sh _5sc _5si
						_5se _5sk _5sd _5sl _5vf _5vi _5vh _5mu _5my _5mv _5mw _5mz _5lk _5mk _5sf _5os _5sg 
						_5ou _5ot _5sx _5jy _5jz _5vc;
	%let ListPac1Autres=_1ci _1cx _5pw _1cv;

	/* VARIABLES DE 'PAC2','PAC3' et 'PAC4' */
	 %let ListPac2Rev=	_6gj _6em _1dj _1dp _1dk _1dv _1dq _1ds _1dz _1do _1dc _1dd _1de _1dg _1dh
						_1dc _1dd _1de _1dg _1dh;
	 %let ListPac2Autres=_1di _1dx _1dv;
	 %let ListPac3Rev=_1ej _1eu _1ev _1ex _6gk _6en _1ep _1ek _1es _1eo;
	 %let ListPac3Autres=_1ei;
	 %let ListPac4Rev=_1fu _1fv _1fx _6gl _6eq;

	/* VARIABLES DE FOYER NON INDIVIDUALISEES */
	%let ListNonIndivRev=	_1bl _1aw _1bw _1cw _1dw
					_2ck _2dh _2ee _2dc _2fu _2ch _2ts _2go _2tr _2fa _2cg _2bh _2ca _2ab _2bg _2dm _2la _2lb
					_3sb _3sc _3sd _3se _3si _3sf _3sn _3sl _3sm
					_3vg _3vh _3sg _3sh _3vd _3vi _3vf _3vj _3vk _3vn _3sj _3sk _3ve _3vm _3vc 
					_3va _3vb _3vq _3vr _3vt _3vu _3vw _3vz _3wa _3wb _3wc _3wd _3ua 
					_3we _3wh _3wm
					_4be _4ba _4bb _4bc _4bd _4bf _4by _4bh
					_5iu _5kz _5ju
					_5ga _5gb _5gc _5gd _5ge _5gf _5gg _5gh _5gi _5gj
					_6ev _6eu _6cb _6gh
					_7db _7df _7dd
					_7cd _7ce _7cf _7cl _7cm _7cn _7cc _7cu _7cq _7cr _7cy
					_7tt _7tu _7tv _7tw _7tx _7ty
					_7ff _7fg _7fh _7fq _7fl _7fm _7fn
					_7gj _7gk _7gl _7gp
					_7gz _7gn _7gs
					_7ha _7hb _7hd _7he _7hf _7hg _7hh
					_7ja _7jf _7jk _7jo _7jb _7jg _7jl _7jp	_7jd _7jh _7jm _7jq _7je _7jj _7jn _7jr
					_7jt _7ju _7jv _7jw _7jx _7jy _7iy
					_7sy _7sx
					_7fa _7fb _7fc _7fd _7fi
					_7gh _7gi _7el _7ek
					_7qd _7qc _7qb _7qa
					_7na _7nf _7nk _7np _7nb _7ng _7nl _7nq _7nc _7nh _7nm _7nr
					_7nd _7ni _7nn _7ns _7ne _7nj _7no _7nt
					_7hj _7hk _7hn _7ho _7hl _7hm _7hr _7hs _7hv _7hw _7hx _7hz _7ht _7hu
					_7ij _7il _7id _7ie _7im _7ik _7is _7iu _7ix
					_7in _7iv _7if _7ig _7iw _7io _7ip _7iq  _7ir _7ia _7ib _7ic _7it _7ih _7iz _7ji
					_7kb _7kc _7kd _7ke
					_7la _7lb _7lc _7le _7ld _7lf _7ly _7lm _7ls _7ln _7lt _7lx _7lz
					_7my _7mh _7mg
					_7nz
					_7oa _7ob _7oc _7od _7oe _7ou 
					_7pa _7pb _7pc _7pd _7pe 
					_7gq
					_7ra _7rb _7rc _7rd _7re _7rf _7rj _7rk _7rl _7rn _7rp _7rr _7rs _7rt _7rq
					_7rg _7ri _7rh _7rv _7rw _7rz
					_7va _7vb _7vc _7vd _7vh
					_7sa _7sb _7sc _7sd _7se _7sf
					_7sg _7sh _7si _7sj _7sk _7sl
					_7sn _7sp _7sq _7sr _7ss _7st
					_7sv _7sw
					_7td  
					_7ua _7ub _7ui _7ut _7ud _7ue _7uf _7ug _7um _7un _7uo _7up _7uq _7ul _7uc _7uh _7uk _7us _7te _7uu _7uv _7tf _7uw _7tg _7ux _7th _7ti
					_7vx _7vz _7vv _7vu _7vt _7vg
					_7wb _7wu _7wv _7wt _7wc
					_7wj _7wl _7wm _7wn _7wo _7wp
					_7xi _7xj _7xk _7xr _7xc _7xf _7xl _7xm _7xp _7xq _7xn _7xv _7uy _7uz _7xn
					_7ya _7yb _7yc _7yd _7ye _7yf _7yg _7yh _7yi _7yj _7yk _7yl
					_8sa _8sb
					_8ut _8ti _8tk _8ta _8th
					_8tm _8tn _8tf _8te _8tg _8ts _8to _8tp _8tq _8tr _8tv _8tw _8tx _8tb _8tc _8wr _8wt _8wu _8uy
					_8tz _8uz _8wb _8wa _8wc _8wd _8we
					_6de _6gp _6gu _6dd
					_0xx
					_haa _hab _hac _had _hae _haf _hag _hah _hai _haj _hak _hal _ham _han _hao _hap
					_haq _har _has _hat _hau _hav _haw _hax _hay _hba _hbb _hbe _hbf _hbg
					_hra _hrb _hrc _hrd _hqc _hqd _hsz _hta _htb _htd
					_hkg _hkh _hki _hks _hkt _hku _hlg _hlh _hli _hma _hmb _hmc _hmm
					_hmn _hnu _hnv _hnw _hny _hoa _hob _hoc _hod _hoe _hof _hog _hoh
					_hoi _hoj _hok _hol _hom _hon _hoo _hop _hoq _hor _hos _hot _hou
					_hov _how _hox _hoy _hoz _hpa _hpb _hpd _hpe _hpf _hph _hpi _hpj
					_hpl _hpm _hpn _hpo _hpp _hpr _hps _hpt _hpu _hpw _hpx _hpy
					_hqb _hqe _hqf _hqg _hqi _hqj _hqk _hql _hqm _hqn _hqo
					_hqp _hqr _hqs _hqt _hqu _hqv _hqw _hqx
					_hrg _hri _hrj _hrk _hrl _hrm _hro _hrp _hrq _hrr _hrt _hru _hrv
					_hrw _hry _hsa _hsb _hsc _hse _hsf _hsg _hsh _hsj _hsk
					_hsl _hsm _hso _hsp _hsq _hsr _hst _hsu _hsv _hsw _hsy
					_hua _hub _huc _hud _hue _huf _hug
					_hxa _hxb _hxc _hxe
					_7xi _7xj _7xk 
					_7ly _7my _7sh
					_7qj _7qs _7qw _7qx
					_7gt _7gu _7gv _7gw _7gx _7jc _7js
					_8tl _8uw
					_9fg _9hi _9pv _9mx _9na _9nc _9ne _9nf _9ng _9rs
					_7ga _7gb _7gc _7ge _7gf _7gg
					_2aa _2al _2am _2an _2aq _2ar
					_4tq
					_5qf _5qg _5qn _5qo _5qp _5qq
					_5rn _5ro _5rp _5rq _5rr _5rw 
					_5ht _5it _5jt _5kt _5lt _5mt
					_6fa _6fb _6fc _6fd _6fe _6fl
					_6hj _6hk _6hl _6hm _6hn
					_7xs _7xt _7xu _7xw _7xy
					_7ql _7qt
					_7ol _7om _7on _7oo _7op _7oq _7or _7os _7ot _7ov _7ow
					_cred_loc _7wr _7ls
					_7nu _7nv _7nw _7nx _7ny
					;
	%let ListNonIndivAutres=	_4bz _6qr _6qw _7dq _7dg _7dl _7we _7xd _7xe _hqa _7ii 
								_8fv _8tt _8uu _9gl _9gm _7wg _7wk _7vo _8td
								_7ea _7ec _7ef _7eb _7ed _7eg
								_7rx;

	 /* CAS PARTICULIERS */

	/* Noms explicites : cases disparues un jour mais dont on a voulu garder l'information
	(si l'on souhaite garder la possibilit� de simuler le dispositif sur des vieilles l�gislations) */
	/* Attention rappel : tout doit �tre en minuscules. */
	%let nomsExplicites=	_cirechant	_cinouvtechn _report_ri_dom_entr
							_interet_pret_conso _vehicule_propre_simple _vehicule_propre_avec_destr 
							_relocalisation _perte_capital _perte_capital_passe
							_invdomaut1 _invdomaut2 _pvcessiondom _pertesci19 _total_credits_impot _nbconvcreentr11
							_dep_devldura_loc1 _dep_devldura_loc2 _dep_devldura_loc3 _dep_devldura_loc4
							_dep_invloc_tour_2011_1 _dep_invloc_tour_2011_2
							_dep_invloc_tour_ap2011_1 _dep_invloc_tour_ap2011_2
							_dep_invloc_tour_av2011_1 _dep_invloc_tour_av2011_2
							_dep_asc_traction _ci_debitant_tabac _pvcession_entrepreneur _pv_imposees_immed
							_1ere_annuite_lgt_neuf _1ere_annuite_lgt_ancien _protect_patnat
							_epargnecodev _credformation
							_demenagementemploivous _demenagementemploiconj _demenagementemploipac1 _demenagementemploipac2
							_revpea _ciformationsalaries _souscsofipeche _revimpcrds _sinistre
							_moit_fenetres _moit_murs _moit_toits
							_glovsup4ansvous _autoentrricnpmarchvous _autoentrricnpservvous 
							_defricsimplvouscga _defricsimplvoussscga
							_ricsimplvouscga _ricsimplvoussscga _hsupvous
							_glovsup4ansconj _autoentrricnpmarchconj _autoentrricnpservconj 
							_defricsimplconjcga _defricsimplconjsscga
							_ricsimplconjcga _ricsimplconjsscga _hsupconj
							_autoentrricnpmarchpac _autoentrricnpservpac 
							_defricsimplpaccga _defricsimplpacsscga
							_ricsimplpaccga _ricsimplpacsscga _hsuppac1 _bouquet_travaux _hsuppac2
							_glo_txfaible _glo_txmoyen;

	/* 	Liste des variables qui n'existent pas dans la version la plus r�cente de la brochure pratique mais qui ont exist� par le pass�
		Objectif de cette liste : pour que �a n'affiche pas d'erreurs quel que soit le mill�sime utilis�. 
		De plus sa constitution permet de rep�rer beaucoup de bugs. */
	%let listVarDisp=	
			/* 2005 */ 	_1fk _1fi _1rx _1rv
			/* 2007 */ 	_1qx _1qv _4bl _1er
			/* 2010 */ 	_2gr _8ws _8wx
			/* 2012 */ 	_5hu _7fy _7gy _7hy _7ky  _7qq _7qh _7pg _7pk _7oz _9mn _9mo
			/* 2013 */ 	_1ty _1uy _2da _7wq _7kh _7ki _7ad _7af _7ah
						_7qn _7qu _7qk _7qy _7qm 
						_7oh _7oi _7oj _7ok _7ol _7om _7on _7oo _7op _7oq _7or _7os _7ot _7ov _7ow
						_7pm _7pn _7po _7pp _7pq _7pr _7ps _7pt _7pu _7pv _7pw _7px _7py
						_7rm _7ro _7ru _7ry
						_7pz _7qz _7mm _7lg _7ma
						_7ks _7mn _7lh _7mb _7kt _7li _7mc _7ku _7qv _7qo _7qp _7qr _7qf _7qg _7qi _7qe
						_7pf _7ph _7pi _7pj _7pl _7wf _7ws _7wv _7ww _7wx _7wa _7ve _7vf
						_8wv _7wi _3sa _7xg _7xa _7xx _7xh _7xb _7xz _3vs _3ss _3vo _3so
						_7kg
			/* 2014 */	_3vv _1au _1bu _1cu _1du _7vy _7vw _3vl _3vp _3vy _3uv _3wf _3wg _3wj _3wi _7wh _7sz
						_7sm _7su _7so _1lz _1mz _7xo _7ka _hsd _hsi _hsn _hss _hsx _htc _hqz;
													
	/* Liste des variables qui ont �t� livr�es pour une raison inconnue � partir de l'ERFS 2013 (absentes de la brochure fiscale) */
	/* Piste : cases de la 2042-Mayotte ? */
	%let listVarInconnues=
					_2ctjcj _2ctjpc _2ctjvs _2ctpcj _2ctppc _2ctpvs _2dbarf _2dcrc _2dcrp _2dcrv _2diarf _2iavet _2ibaet _2icr _2ics
					_2ifire _2ifr _2imisp _2inrro _2ipsde _2ipsrf _2irmpo _2irpni _2irsba _2isfga _2iso _2iti1 _2iti2 _2p19sp _2pautr
					_2pbssp _2pgnab _2pimmo _2prcm _2prnnx _2prrts _2punim _2pvnab _2pvnah _2pvnai _2pvnaj _2pwng _2tbes _2tbesc 
					_2tshc1 _2tshcj _2tshvs _2tspc1 _2tspcj _2tspvs
					_cic _cii _cjc _daj _dbj _eaj _ebj
					_naw _nbo _nbw _ncj _ncp _ndo _nes _nfs _ngo _raw _rbo _rbw _rcj _rcp _rdo _res _rfs _rgo
					_1fc _1fd _29yd
					_7gm _7wd
					_8pa
					_8uv _8ux _8vv _8vw _8vx _8xf _8xg _8xh _8xk _8xv _8yd _8ye _8yf _8yh _8yj _8yk _8yl _8yt _8yz _8zb _8zh _8zi _8zo _8zt _8zu
					_9yt _9yu _9yz
					/* cases n'existant pas mais dont on a besoin dans Ines (p�c 4 ...) */
					_1fj _1fp _1fs _1fo;
	%Mend ListeCasesIndividualisees;

/************************************************/
/* 3\ Cr�ation de la table dossier.FoyerVarList */
/************************************************/

/* Macro qui cr�e une table dossier.FoyerVarList, qui vise � d�terminer pour chaque fiscale de la table foyer du mill�sime ERFS utilis� : 
	- dans quel agr�gat la case est englob�e (en lien avec la macro %ListeCasesAgregatsERFS du programme macros)
	- � quel individu la case se r�f�re (vous, conjoint, p�c1 ou 2, ou "ensemble du foyer fiscal"). 

	En fonction des listes d�finies par les deux macros ci-dessus, on cr�e la table dossier.FoyerVarList
	Cette table n'est pas utilis�e en tant que telle dans le mod�le Ines (sauf programme XYZ_foyer avant 2013), mais elle peut s'av�rer
	tr�s utile pour des fins d'�tude (d�s que l'on veut individualiser une op�ration � un niveau plus fin que l'agr�gat ERFS). 
	Elle est �galement utile pour rapidement retrouver des informations sur les variables fiscales. */



proc contents data=travail.foyer&anr.(keep=_:) out=FoyerVarList(keep=name length label) noprint; run;

%ListeCasesAgregatsERFS;
%ListeCasesIndividualisees;

options noquotelenmax; /* Pour pr�venir le warning sur la longueur des chaines de caract�res */
data dossier.FoyerVarList;
	set FoyerVarList;	
	length TypeContenu $100;
	length AgregatERFS $5.;
	if index("&ListVousRev.",lowcase(strip(name)))>0 then TypeContenu="Montant concernant le d�clarant (vous)";
	if index("&ListVousAutres.",lowcase(strip(name)))>0 then TypeContenu="Autre case concernant le d�clarant (vous)";
	if index("&ListConjRev.",lowcase(strip(name)))>0 then TypeContenu="Montant concernant le conjoint (conj)";
	if index("&ListConjAutres.",lowcase(strip(name)))>0 then TypeContenu="Autre case concernant le conjoint (conj)";
	if index("&ListPac1Rev.",lowcase(strip(name)))>0 then TypeContenu="Montant concernant pac1";
	if index("&ListPac1Autres.",lowcase(strip(name)))>0 then TypeContenu="Autre case concernant pac1";
	if index("&ListPac2Rev.",lowcase(strip(name)))>0 then TypeContenu="Montant concernant pac2";
	if index("&ListPac2Autres.",lowcase(strip(name)))>0 then TypeContenu="Autre case concernant pac2";
	if index("&ListPac3Rev.",lowcase(strip(name)))>0 then TypeContenu="Montant concernant pac3";
	if index("&ListPac3Autres.",lowcase(strip(name)))>0 then TypeContenu="Autre case concernant pac3";
	if index("&ListPac4Rev.",lowcase(strip(name)))>0 then TypeContenu="Montant concernant pac4";
	if index("&ListNonIndivRev.",lowcase(strip(name)))>0 then TypeContenu="Montant concernant le foyer";
	if index("&ListNonIndivAutres.",lowcase(strip(name)))>0 then TypeContenu="Autre case concernant le foyer";
	if index("&nomsExplicites.",lowcase(strip(name)))>0 then TypeContenu="Cases disparu dont on a transf�r� le contenu dans un nom explicite";
	if index("&listVarDisp.",lowcase(strip(name)))>0 then TypeContenu="Cases ne faisant plus partie de la brochure fiscale la plus r�cente";
	if index("&listVarInconnues.",lowcase(strip(name)))>0 then TypeContenu="Variable pr�sente mais dont on ne conna�t pas la signification";
	if TypeContenu='' then ERROR "Le type de case fiscale n'est pas renseign�";

	if index("&zsalfP.",lowcase(strip(name)))>0 then AgregatERFS="zsalf";
	else if index("&zchofP.",lowcase(strip(name)))>0 then AgregatERFS="zchof";
	else if index("&zrstfP.",lowcase(strip(name)))>0 then AgregatERFS="zrstf";
	else if index("&zpifP.",lowcase(strip(name)))>0 then AgregatERFS="zpif";
	else if index("&zalrfP.",lowcase(strip(name)))>0 then AgregatERFS="zalrf";
	else if index("&zrtofP.",lowcase(strip(name)))>0 then AgregatERFS="zrtof";
	else if index("&zragfP.",lowcase(strip(name)))>0 then AgregatERFS="zragf";
	else if index("&zragfN.",lowcase(strip(name)))>0 then AgregatERFS="zragf";
	else if index("&cbicf_.",lowcase(strip(name)))>0 then AgregatERFS="zricf";
	else if index("&zricfP.",lowcase(strip(name)))>0 then AgregatERFS="zricf";
	else if index("&zricfN.",lowcase(strip(name)))>0 then AgregatERFS="zricf";
	else if index("&cbncf_.",lowcase(strip(name)))>0 then AgregatERFS="zrncf";
	else if index("&zrncfP.",lowcase(strip(name)))>0 then AgregatERFS="zrncf";
	else if index("&zrncfN.",lowcase(strip(name)))>0 then AgregatERFS="zrncf";
	else if index("&zvalfP.",lowcase(strip(name)))>0 then AgregatERFS="zvalf";
	else if index("&zavffP.",lowcase(strip(name)))>0 then AgregatERFS="zavff";
	else if index("&zvamfP.",lowcase(strip(name)))>0 then AgregatERFS="zvamf";
	else if index("&zvamfN.",lowcase(strip(name)))>0 then AgregatERFS="zvamf";
	else if index("&zfonfP.",lowcase(strip(name)))>0 then AgregatERFS="zfonf";
	else if index("&zfonfN.",lowcase(strip(name)))>0 then AgregatERFS="zfonf";
	else if index("&caccf_.",lowcase(strip(name)))>0 then AgregatERFS="zracf";
	else if index("&zracfP.",lowcase(strip(name)))>0 then AgregatERFS="zracf";
	else if index("&zracfN.",lowcase(strip(name)))>0 then AgregatERFS="zracf";
	else if index("&zetrfP.",lowcase(strip(name)))>0 then AgregatERFS="zetrf";
	else if index("&zalvfP.",lowcase(strip(name)))>0 then AgregatERFS="zalvf";
	else if index("&zglofP.",lowcase(strip(name)))>0 then AgregatERFS="zglof";
	else if index("&zquofP.",lowcase(strip(name)))>0 then AgregatERFS="zquof";
	else if index("&zdivfP.",lowcase(strip(name)))>0 then AgregatERFS="zdivf";
	else if index("&zdivfN.",lowcase(strip(name)))>0 then AgregatERFS="zdivf";
	run;
options quotelenmax;

/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
