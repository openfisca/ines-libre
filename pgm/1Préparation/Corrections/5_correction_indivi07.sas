/* correction bien faite � partir de 2008, ce programme est fait pour voir si
on peut faire tourner ines sur ERFS 2007 et faire l'�tude panel*/

*on importe les modifications qui �taient faites � l'�poque mais elles sont
moins soign�es que celle qui sont faites � partir de anref 2008; 

data travail.indivi07; 
set travail.indivi07; 

%macro remplace(ancien,nouveau);
if declar1=&ancien then declar1=&nouveau;
if declar2=&ancien then declar2=&nouveau;
%mend remplace;
%macro rempl_d_en_v(decl);
if declar1=&decl then declar1= substr(&decl,1,12)!!'V'!!substr(&decl,14,10)!!'00Z'!!substr(&decl,27,3);
if declar2=&decl then declar2= substr(&decl,1,12)!!'V'!!substr(&decl,14,10)!!'00Z'!!substr(&decl,27,3);
%mend rempl_d_en_v;

if ident=07057738 & noi = 01 then declar1='02-07057738-O1985-1978-X00  -' ;
%RemplaceIndivi('01-07000006-M1956-1954-000  - J1988','01-07000006-M1956-1954-000  -J1988');
%RemplaceIndivi('01-07068212-M1961-1960-000  -F1991','01-07068212-M1961-1960-000  -F1991G1991');
%RemplaceIndivi('01-07058172-M1937-1938-000  -F1994F1987','01-07058172-M1937-1938-000  -F1994J1987');
%RemplaceIndivi('02-07071377-V1918-9999-000  -F1947','02-07071377-V1918-9999-000  -F1947G1947');
%RemplaceIndivi('01-07007876-D1966-9999-X00 M-F1992F1988','01-07007876-D1966-9999-X00 M-F1992F1988G1988');
%rempl_d_en_v('01-07012532-D1947-9999-0Y0  -');
%rempl_d_en_v('01-07018028-D1927-9999-0Y0  -');
%rempl_d_en_v('01-07021714-D1930-9999-0Y0  -');
%rempl_d_en_v('01-07023293-D1960-9999-0Y0  -');
%rempl_d_en_v('01-07027710-D1917-9999-0Y0  -');
%rempl_d_en_v('01-07042237-D1922-9999-0Y0  -');
%rempl_d_en_v('01-07066547-D1927-9999-0Y0  -');
%rempl_d_en_v('01-07074486-D1928-9999-0Y0  -');
%rempl_d_en_v('02-07015465-D1916-9999-0Y0  -');

if substr(declar1,30,1)='' & substr(declar1,31,1) ne '' then declar1=substr(declar1,1,29)!!substr(declar1,31,39);
if substr(declar2,30,1)='' & substr(declar2,31,1) ne '' then declar2=substr(declar2,1,29)!!substr(declar2,31,39);

run;

data travail.indfip07; 
set travail.indfip07; 
%RemplaceIndivi('01-07068212-M1961-1960-000  -F1991','01-07068212-M1961-1960-000  -F1991G1991');
%RemplaceIndivi('01-07058172-M1937-1938-000  -F1994F1987','01-07058172-M1937-1938-000  -F1994J1987');
%RemplaceIndivi('02-07071377-V1918-9999-000  -F1947','02-07071377-V1918-9999-000  -F1947G1947');
%RemplaceIndivi('01-07007876-D1966-9999-X00 M-F1992F1988','01-07007876-D1966-9999-X00 M-F1992F1988G1988');
%rempl_d_en_v('01-07012532-D1947-9999-0Y0  -');
%rempl_d_en_v('01-07018028-D1927-9999-0Y0  -');
%rempl_d_en_v('01-07021714-D1930-9999-0Y0  -');
%rempl_d_en_v('01-07023293-D1960-9999-0Y0  -');
%rempl_d_en_v('01-07027710-D1917-9999-0Y0  -');
%rempl_d_en_v('01-07042237-D1922-9999-0Y0  -');
%rempl_d_en_v('01-07066547-D1927-9999-0Y0  -');
%rempl_d_en_v('01-07074486-D1928-9999-0Y0  -');
%rempl_d_en_v('02-07015465-D1916-9999-0Y0  -');

if substr(declar1,30,1)='' & substr(declar1,31,1) ne '' then declar1=substr(declar1,1,29)!!substr(declar1,31,39);
if substr(declar2,30,1)='' & substr(declar2,31,1) ne '' then declar2=substr(declar2,1,29)!!substr(declar2,31,39);
run;



/*

*prob_noi_declar;
proc sort data=prob_noi_declar;by ident;run;
proc sort data=test;by ident;run;
data voir; 
merge test prob_noi_declar(in=a);by ident; if a; run;
* pas encore corrig�, car j'attends des r�ponses du p�le revenu fiscaux
de plus il faut regarder cela avec les revenus sous les yeux;

*prob_naia;
data voir; merge prob_naia(in=a drop=naia) erf&anr.&anr..irf&anr.e&anr.(keep=ident noi naia cstot);
by ident noi; if a; run;
*changement de la date de naissance du declar 01-08076061-D1999-9999-000  -
en 01-08076061-D1957-9999-000  -; *ce sera fait plus loin car, c'est une modif fiscale; 

*probrev;

*diffrev; 
*a voir en fonction de la r�ponse du p�le revenu fiscaux;
*/


/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
