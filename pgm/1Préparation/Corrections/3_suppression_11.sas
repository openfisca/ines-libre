/*****************************************************/
/*			programme 3_suppression_11               */ 
/*****************************************************/

/**************************************************************************************************************/
/* Tables en entr�e :                                                                                         */
/*	travail.foyer&anr.																						  */		
/*  travail.irf&anr.e&anr																					  */
/*  travail.mrf&anr.e&anr																					  */
/*  travail.indivi&anr.																						  */
/*  travail.indfip&anr.																						  */
/*  travail.menage&anr.																				          */
/* 																											  */
/* Tables en sortie :																						  */
/*	travail.foyer&anr.																						  */
/*  travail.irf&anr.e&anr																					  */
/*  travail.mrf&anr.e&anr																					  */
/*  travail.indivi&anr.																						  */
/*  travail.indfip&anr.																			              */
/*  travail.menage&anr.																						  */
/*																											  */
/* Objectif : Dans ce programme, on supprime des diff�rentes tables de la biblioth�que travail,               */
/*  les m�nages que l'on consid�re comme mal renseign�s.													  */	   
/**************************************************************************************************************/

%suppression('11100652' '11071110' '22018020' '11022595' '11049988' '11056636');
/* 11100652 = observations avec pond�ration aberrante de plus de 11000 */
/* 11071110 = ce m�nage avec des EE_NRT ne devrait pas �tre dans le champ de l'ERFS (acteupr ne '5') */
/* 4 derniers m�nages : constitu�s d'EE_CAF aux declar1 vides */



/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
