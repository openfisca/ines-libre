*******************************************************
correction qui font suite aux fichiers verif1; 


data travail.indivi10; 
set travail.indivi10; 


*a) changement de date de naissance du declarant dans le declar; 
%RemplaceIndivi('02-10096554-C1892-9999-000  -F2009F2009','02-10096554-C1982-9999-000  -F2009F2009');
%RemplaceIndivi('03-10102610-M1979-1987-000  -F1995F1998F2001F2002F2006','03-10102610-M1979-1987-000  -F995F1998F2001F2002F2006')
%RemplaceIndivi('06-10080927-V1965-9999-000  -F1995J1191','06-10080927-V1965-9999-000  -F1995J1991')


*b) Modification du statut mdcco du d�clarant; 

*mise en veuf les divorc�s;
* mise en coh�rence des declars suite � prob_even; 
* les gens mari�s qui d�clare �tre divorc� au lieu d'�tre veuf lorsque leur conjoint d�c�de;
%rempl_d_en_v('02-10049169-D1926-9999-0Y0  -');

* mis en divorc� les mari�s; 
%change_statut('01-10000253-M1953-9999-000  -',D);
%change_statut('01-10016857-M1952-9999-000  -',D);
%change_statut('01-10036783-M1954-9999-000  -',D);
%change_statut('01-10038500-M1971-9999-000  -F2005',D);
%change_statut('01-10058298-M1955-9999-000  -F1993',D);
%change_statut('01-10065399-M1953-9999-000  -',D);
%change_statut('01-10103571-M1975-9999-000  -F1993F1994F2005',D);

* mis en divorce de gens c�libataires;
%change_statut('02-10026315-C1973-9999-0Y0  -F2003',D);
%change_statut('02-10030187-C1969-9999-0Y0  -F2003',D);
%change_statut('03-10021413-C1980-9999-0Y0 S-',D);

*on remplace un divorc� en veuf;
%rempl_d_en_v('02-10049169-D1926-9999-0Y0  -');



run; 



/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
