*******************************************************
correction qui font suite aux fichiers verif2;

* on reprend aussi les modifications de correction1_&an qui ont une cons�quence sur les d�clarations.; 

 

data travail.foyer10; set travail.foyer10; 

	/* I - modification de dates de naissances absurdes */
	/*d�clarant */
	if declar='02-10096554-C1892-9999-000  -F2009F2009' then do;
		declar='02-10096554-C1982-9999-000  -F2009F2009';
		sif=substr(sif,1,5)!!'1982'!!substr(sif,11,84);
	end;
	/* modification de dates de naissance de pac incoh�rente*/
	if declar='03-10102610-M1979-1987-000  -F995F1998F2001F2002F2006' then do;
		declar='03-10102610-M1979-1987-000  -F1995F1998F2001F2002F2006';
		anaisenf='F1995F1998F2001F2002F2006';
	end;
	if declar='06-10080927-V1965-9999-000  -F1995J1191' then do;
		declar='06-10080927-V1965-9999-000  -F1995J1991';
		anaisenf='F1995J1991';
	end;

	/* II - modification des �v�nements */  

	/* On utilise v�rif2 pour rep�rer les incoh�rences entre les statuts et les �v�nements */

	/* a) modification de dates absurdes d'�v�nements dans le sif 
	      Dans ce premier cas, il y a un d�calage dans le sif */
	if declar='01-10052827-M1926-1931-00Z Z-' then sif=substr(sif,1,51)!!"0Z07122010"!!substr(sif,62,34);
	/*    Dans ce second cas, on a un mois d'�v�nement �gal � 20, je le remplace par 02 par analogie avec les 
	      autres d�clarations du m�nage. */
	if declar='05-10019958-D1967-9999-0Y0  -' then sif=substr(sif,1,46)!!"02"!!substr(sif,49,46); 

	/* b) Modification du statut mdcco du d�clarant. */

	/* on met en divorc� les gens mari�s qui n'ont pas de conjoints sur leur d�claration */ 
	%RemplaceStatutOld('01-10000253-M1953-9999-000  -',D);
	%RemplaceStatutOld('01-10016857-M1952-9999-000  -',D);
	%RemplaceStatutOld('01-10036783-M1954-9999-000  -',D);
	%RemplaceStatutOld('01-10038500-M1971-9999-000  -F2005',D);
	%RemplaceStatutOld('01-10058298-M1955-9999-000  -F1993',D);
	%RemplaceStatutOld('01-10065399-M1953-9999-000  -',D);
	%RemplaceStatutOld('01-10103571-M1975-9999-000  -F1993F1994F2005',D);

	/* on met en divorc� des gens qui ont d�clar� un divorce dans l'ann�e mais qui se d�clarent c�libataires */
	%RemplaceStatutOld('02-10026315-C1973-9999-0Y0  -F2003',D);
	%RemplaceStatutOld('02-10030187-C1969-9999-0Y0  -F2003',D);
	%RemplaceStatutOld('03-10021413-C1980-9999-0Y0 S-',D);

	/* c) modification d'�venements lorsqu'il y a diff�rence entre declar1 et declar2*/
	%div_en_veuf('02-10049169-D1926-9999-0Y0  -');

	* III - gestion des pac; 
	* voir programme verif 2; 

	/* cas ou plus de PAC dans le declar que dans le sif 
	=> on ajoute une ou plusieurs PAC dans le sif */
	if declar='01-10008608-C1970-9999-000  -F1995F2000F2008F1995' then sif=substr(sif,1,65)!!'03'!!substr(sif,68,27);
	if declar='01-10019471-M1962-1967-000  -F1993F1995F1991F1991J1991J1991' then sif=substr(sif,1,65)!!'04'!!substr(sif,68,27);
	if declar='01-10025137-C1961-9999-000  -J1990J1986' then sif=substr(sif,1,74)!!'02'!!substr(sif,77,18);
	if declar='01-10090714-C1968-9999-000  -F2000F2003' then sif=substr(sif,1,65)!!'02'!!substr(sif,68,27);
	if declar='02-10047235-D1967-9999-000  -J1990J1991' then sif=substr(sif,1,74)!!'02'!!substr(sif,77,18);
	if declar='03-10075484-C1983-9999-000  -J1991' then sif=substr(sif,1,74)!!'01'!!substr(sif,77,18);

	*IV - remplissage des cases fiscales; 
	%standard_foyer;

	run;



/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
