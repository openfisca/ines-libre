*suppression de certains m�nages; 


*il faut la verifier, elle n'a pas encore tourn�, en particulier, je ne suis pas sur de comment entrer
la liste, succession d'ident ou bien avec des guillemets, etc.;  
%macro suppression(liste); 
data travail.foyer&anr.; set travail.foyer&anr.; 
if ident not in (&liste); run; 
data travail.irf&anr.e&anr; set travail.irf&anr.e&anr; 
if ident not in (&liste); run; 
data travail.mrf&anr.e&anr; set travail.mrf&anr.e&anr; 
if ident not in (&liste); run; 
data travail.indivi&anr.; set travail.indivi&anr.; 
if ident not in (&liste); run; 
data travail.indfip&anr.; set travail.indfip&anr.; 
if ident not in (&liste); run; 
data travail.menage&anr.; set travail.menage&anr.; 
if ident not in (&liste); run; 
%mend;

%suppression('09075423' '09083751' '09035373' '09005688' '09067408'
'09064321' '09072842' '09063847' '09069640' '09060689' '09059582' '09090548'); 
*09075423 :;
*09083751 : d�claration pas du tout coh�rente : m�nage mal appari�. on a 5 personnes dans le m�nage. 
le noi=01 est divorc�, vit avec ses 3 enfants et noi=05 qui lui par ailleurs est mari� et a deux enfants.
seulement, au lieu d'attribuer aux enfants du m�nage la d�claration fiscal de noi=01,
on leur a attribu� la d�claration de noi=05. 
ce qui fait que les dates de naissances ne correspondent paset du coup on a un enfant EE. 
de plus, on a le conjoint de noi=5 qui est FIP (sans d�claration) 
mais il manque les enfants FIP de la d�claration fiscale du noi=05... 
* 09035373 : n'a pas de date de naissance; 
* 09067408 : cas de changements de situations 2 fois dans l'ann�e; 
* 09072842  09063847 09069640 09059582 09060689: il manque une d�claration dans foyer mais qui appara�t dans indivi; 
*09064321;
*09090548 : un marriage tr�s mal d�clar� et difficile � corriger; 


/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
