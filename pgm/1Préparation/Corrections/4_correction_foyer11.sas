/***************************/
/*  4_correction_foyer11   */
/***************************/

/****************************************/
/*	En entr�e : travail.foyer&anr.		*/
/*	En sortie : travail.foyer&anr.		*/
/****************************************/

/* Les corrections ci-dessous font suite au programme 2_verif_niveau_foyer */

proc sql;

	/* 1	Modification de dates de naissances absurdes */

	/*d�clarant */
	update travail.foyer&anr.
		set declar='02-11033769-C1982-9999-000  -F2009F2009', 
			sif=substr(sif,1,5)!!'1982'!!substr(sif,11,84)
		where declar='02-11033769-C1892-9999-000  -F2009F2009';
	/* conjoint */
	update travail.foyer&anr.
		set declar='01-11093635-M1950-1954-000  -',
			sif=substr(sif,1,10)!!'1954'!!substr(sif,15,75)
		where declar='01-11093635-M1950-1854-000  -';


	/* 2	Modification des �v�nements */  

	/* a) modification de dates absurdes d'�v�nements dans le sif */  
	update travail.foyer&anr.
		set sif=substr(sif,1,42)!!'0Y15072011'!!substr(sif,53,43)
		where declar='02-11028836-D1937-9999-0Y0  -';

	update travail.foyer&anr.
		set sif=substr(sif,1,34)!!'X'!!substr(sif,35,61)
		where declar='05-11052636-C1985-9999-X00  -F2009';

	update travail.foyer&anr.
		set sif=substr(sif,1,51)!!'0Z14102011'!!substr(sif,62,34)
		where declar='01-11025155-M1948-1962-00Z Z-';

	/* b) Modification du statut mcdvo du d�clarant. */

	/* on met en divorc� les gens mari�s qui n'ont pas de conjoints sur leur d�claration et qui n'ont pas d'�v�nements dans l'ann�e */
	%RemplaceStatut(D,	'01-11001567-M1954-9999-000  -F2009'
						'01-11002180-M1975-9999-000  -F1993F1994F2005'
						'01-11008781-M1964-9999-000  -'
						'01-11010208-M1953-9999-000  -'
						'03-11058052-M1945-9999-000  -'
						'01-11063244-M1952-9999-000  -'
						'03-11087106-M1972-9999-000  -'
						'01-11094958-M1968-9999-000  -'
						'01-11096792-M1959-9999-000  -');
	/* on met en divorc� des gens qui ont d�clar� un divorce dans l'ann�e mais qui se d�clarent c�libataires */
	%RemplaceStatut(D,	'01-11087258-C1989-9999-0Y0  -'
						'02-11092122-C1979-9999-0Y0  -H2001'
						'01-11092122-C1980-9999-0Y0  -F2005'
						'01-11092459-C1974-9999-0Y0  -'
						'02-11102479-C1974-9999-0Y0  -F2005F2008'
						'01-11110761-C1944-9999-0Y0  -'
						'01-11112182-C1979-9999-0Y0  -'
						'01-11112270-C1989-9999-0Y0  -');
	/* on met en veuf les gens mari�s qui n'ont pas de conjoints sur leur d�claration et qui d�clare un d�c�s dans l'ann�e*/
	%RemplaceStatut(V,	'01-11025119-M1934-9999-00Z Z-'
						'01-11029731-M1957-9999-00Z Z-'
						'01-11103263-M1924-9999-00Z Z-');
	/* on met en veuf les gens qui se d�clarent divorc� et qui d�clarent un d�c�s dans l'ann�e*/
	%RemplaceStatut(V,	'01-11053111-D1928-9999-00Z  -');

	/* on met en c�libataire les gens mari�s qui n'ont pas de conjoints sur leur d�claration et qui d�clare un 
	   mariage dans l'ann�e*/
	%RemplaceStatut(C,	'01-11112052-M1961-9999-X00  -');


	/* 3	Modification du SIF car erreur d'�criture */

	/*pb du nbenf qui ne commence pas par F*/
	update travail.foyer&anr.
		set sif=substr(sif,1,65)!!'F01'!!substr(sif,69,26)
		where declar='01-11012977-M1957-1963-000  -F2011';
	update travail.foyer&anr.
		set sif=substr(sif,1,65)!!'F01'!!substr(sif,69,26)
		where declar='02-11009601-C1986-9999-000  -F2011';

	quit;



/* 4	Standardisation table foyer */
data travail.foyer&anr.;
	set travail.foyer&anr.;
	%standard_foyer;
	run;



/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
