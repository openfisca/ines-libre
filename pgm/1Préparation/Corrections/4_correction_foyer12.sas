/***************************/
/*  4_correction_foyer12   */
/***************************/

/* Les corrections ci-dessous font suite au programme 4_Controles_Niveau_Foyer */
/****************************************/
/*	En entr�e : travail.foyer&anr.		*/
/*	En sortie : travail.foyer&anr.		*/
/****************************************/

data travail.foyer&anr.;
	set travail.foyer&anr.;
	

	/* 1	Modification de dates de naissances absurdes */
	%RemplaceDeclar(ancien=	'02-12064027-C1892-9999-000  -F2009F2009',
					nouveau='02-12064027-C1982-9999-000  -F2009F2009',
					sif=	'SIF C1982 9999 000000000000000    000000000000000000000000000    F02G00R00J00N00H00I00P00 00 00');

	/* Ajout d'un pac dans le d�clar pour �tre coh�rent avec le SIF et anaisenf */ 
	%RemplaceDeclar(ancien=	'02-12006907-M1966-1970-000  -F1996J1993',
					nouveau='02-12006907-M1966-1970-000  -F1996J1993J1990');
	%RemplaceDeclar(ancien=	'02-12018516-M1976-1974-000  -F1996F1998F1999F2002F2004F2006G1996G1998',
					nouveau='02-12018516-M1976-1974-000  -F1996F1998F1999F2002F2004F2006G1996G1998G2004');
	/* Correction d'un Pac majeur non handicap� */
	%RemplaceDeclar(ancien=	'01-12030591-M1962-1963-000  -F1996F1992J1992',
					nouveau='01-12030591-M1962-1963-000  -F1996J1992');
	/* Correction d'une interversion entre 01 et 02 pour la declaration */
	%RemplaceDeclar(ancien=	'02-12067990-M1968-1968-000  -F2005F2006',
					nouveau='01-12067990-M1968-1968-000  -F2005F2006');

	/* 2	Modification des �v�nements */  
	/* a) modification de dates absurdes d'�v�nements dans le sif */  
	if declar='02-12037817-M1966-1962-X00  -F1994F1996F1997' then do;
		sif=substr(sif,1,34)!!'X30122012'!!substr(sif,44,52);
		end;
	if declar='02-12031273-M1942-1951-00Z Z-' then do;
		sif=substr(sif,1,51)!!'0Z07122012'!!substr(sif,62,34);
		end;

	/* b) Modification du statut mcdvo du d�clarant. */
	%RemplaceStatutOld('01-12004323-M1954-9999-000  -F1997',D);

	/* 4	Standardisation table foyer */
	%standard_foyer;
	run;



/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
