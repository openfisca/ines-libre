/************************************************************************************/
/*  			5_correction_indivi13  												*/
/************************************************************************************/

/* mise en coh�rence en fonction des modifications effectu�es dans correction_foyer	*/
/************************************************************************************/
/*	En entr�e : travail.indivi&anr.	 												*/
/*	En sortie : travail.indivi&anr.	 												*/
/************************************************************************************/


data travail.indivi&anr.; 
   	length declar1 $79.;
	set travail.indivi&anr.; 

	/* Trop d'enfants => Le Declar est tronqu� par erreur */
	%RemplaceDeclar(ancien=	'01-13043148-M1949-1964-000  -F2006F2002F2000F1999F1997F1996J1994J1993J1',
					nouveau='01-13043148-M1949-1964-000  -F2006F2002F2000F1999F1997F1996J1994J1993J1992', var = declar1);

	/* Incoh�rence dans la d�claration d'un enfant (adulte handicap�) */
	%RemplaceIndivi(ancien=	'02-13004331-M1946-1944-000  -F1967G1967',
					nouveau='02-13004331-M1946-1944-000  -G1967');

	/* Incoh�rence entre Declar et VousConj (ici couple dont l'un est toujours mari�, et confusion dans le Declar entre l'ancien et le nouveau conjoint) */
	%RemplaceIndivi(ancien=	'02-13030088-M1960-1956-000  -',
					nouveau='02-13030088-M1960-1959-000  -');

	/* Incoh�rence dates de naissance des enfants */
	%RemplaceIndivi(ancien=	'01-13003108-C1972-9999-000  -F2001F2005F1994J1994',
					nouveau='01-13003108-C1972-9999-000  -F2001F2005J1994');
	%RemplaceIndivi(ancien=	'01-13007910-V1979-9999-000  -F2013F2005F2003F1996',
					nouveau='01-13007910-V1979-9999-000  -F2013F2005F2003F1996');
	%RemplaceIndivi(ancien=	'01-13021231-C1985-9999-000  -F2010F2010H2006',
					nouveau='01-13021231-C1985-9999-000  -F2013F2010H2006');

	/* Correction d'une naia */
	if ident="13048432" and noi="02" then naia="1949";
	run;

/*
data travail.indfip&anr.;
	set travail.indfip&anr.;
	run;
*/


/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
