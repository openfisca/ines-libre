*suppression de certains m�nages; 


*il faut la verifier, elle n'a pas encore tourn�, en particulier, je ne suis pas sur de comment entrer
la liste, succession d'ident ou bien avec des guillemets, etc.;  
%macro suppression(liste); 
data travail.foyer&anr.; set travail.foyer&anr.; 
if ident not in (&liste); run; 
data travail.irf&anr.e&anr; set travail.irf&anr.e&anr; 
if ident not in (&liste); run; 
data travail.mrf&anr.e&anr; set travail.mrf&anr.e&anr; 
if ident not in (&liste); run; 
data travail.indivi&anr.; set travail.indivi&anr.; 
if ident not in (&liste); run; 
data travail.indfip&anr.; set travail.indfip&anr.; 
if ident not in (&liste); run; 
data travail.menage&anr.; set travail.menage&anr.; 
if ident not in (&liste); run; 
%mend;

*pas de suppression en 2007;

/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
