/********************************************************
correction qui font suite aux fichiers verif1; 

Plan:
0 - Probl�me de revenu et de persfip; 
	a) probl�me de persfip;
	b) probl�me de revenus; 
	c) changement de date de naissance du declarant dans le declar;  
I - modification des �v�nements;  
	a) case 62; 
	b) Ajout/suppression d'un �v�nement; 
	c) Modification du statut mdcco du d�clarant;
	d) changement des num�ros de noi des d�clar; 	
II - gestion des pac;
III - Suppression des d�clar ou plut�t transformation du quelfic des gens;*/
/*****************************************************************/


data travail.indivi09; 
	set travail.indivi09; 

/*****************************************************************/
* 0 - Probl�me de revenu et de persfip; 
/*****************************************************************/

	* a) probl�me de persfip; 

*correction de personnes sans declar mais avec un persfip
suite de prob_declar dans verif1; 
if noindiv='0901435904' then do ; persfip = ''; declar1=''; end;
if noindiv='0903487302' then do ; persfip = 'vopa';  end;
if noindiv='0901704902' then do ; persfip = ''; persfipd=''; end; 
* ERFS a oubli� de leur donner un persfip car leur age est diff�rent dans EEC et dans la d�claration fiscale; 
if noindiv='0908180804' then do ; persfip = 'pac'; end; 
if noindiv='0908741003' then do ; persfip = 'pac'; end;  

	*b) probl�me de revenus; 

* on enl�ve les revenus n�gatifs imput�s lorsque le gars est �tudiant; 
if noindiv = '0902401301' or  noindiv = '0906115502' then zrici=0;*� verifier...;
* on garde les revenus des morts; 
if ident='09049141' and noi='02' then do; 
	zrsti=zrsto; persfip='vous'; 
	declar1='02'!!substr(declar1,3,28-3)!!' '!!substr(declar1,29,length(declar1)-29+1);
end;

	*c) changement de date de naissance du declarant dans le declar; 

%RemplaceIndivi('01-09014359-M1961-1958-000  -F1991F1992','01-09014359-M1950-1960-000  -F1991F1992');
* Certains n'ont pas forc�ment la m�me ann�e de naissance dans l'enqu�te emploi et dans les
fichiers fiscaux : l'information de l'enqu�te pr�vaut;
*En principe, il faudrait faire pareil pour le declar2 mais ca me parait compliqu�;
*if  (quelfic in ('EE&FIP','FIP') & persfip='vous' & substr(declar1,14,4) ne naia & naia ne '')
	then declar1=substr(declar1,1,13)!!naia!!substr(declar1,18,length(declar1)-18+1);
*if  (quelfic in ('EE&FIP','FIP') & persfip='conj' & substr(declar1,19,4) ne naia & naia ne '')
	then declar1=substr(declar1,1,18)!!naia!!substr(declar1,23,length(declar1)-23+1);
*Ne fonctionne pas du tout : ca cr�e des individus sans declar...;


/*****************************************************************/
* I - modification des �v�nements;  
/*****************************************************************/
*voir verif2; 

	*a) case 62; 

%RemplaceIndivi('01-09004056-D1964-9999-0Y0  -F1992','01-09004056-M1964-1964-0Y0 S-F1992');
%RemplaceIndivi('01-09018032-C1963-9999-X00 M-', '01-09018032-C1963-9999-000  -');

	*b) Ajout/suppression d'un �v�nement; 

*ajout d'un �v�nement (d�c�s); 
%RemplaceIndivi('01-09012994-M1922-1929-000  -','01-09012994-M1922-1929-00Z Z-');
%RemplaceIndivi('01-09024462-M1928-1926-000  -','01-09024462-M1928-1926-00Z Z-'); 
* on enl�ve la mort de quelqu'un pour simplifier la situation du vopa qui vit avec; 
%RemplaceIndivi('01-09034873-V1970-9999-00Z  -F1991F1995F1996F1997F1999F2008','01-09034873-V1970-9999-000  -F1991F1995F1996F1997F1999F2008');

	*c) Modification du statut mdcco du d�clarant; 

*mise en veuf les divorc�s;
* mise en coh�rence des declars suite � prob_even; 
* les gens mari�s qui d�clare �tre divorc� au lieu d'�tre veuf lorsque leur conjoint d�c�de;
%rempl_d_en_v('02-09003178-D1925-9999-0Y0  -');
%rempl_d_en_v('02-09010699-D1936-9999-0Y0  -');
%rempl_d_en_v('01-09016027-D1930-9999-0Y0  -');
%rempl_d_en_v('02-09016475-D1929-9999-0Y0  -');
%rempl_d_en_v('01-09017156-D1938-9999-0Y0  -');
%rempl_d_en_v('01-09026171-D1951-9999-0Y0  -');
%rempl_d_en_v('02-09029486-D1922-9999-0Y0  -');
%rempl_d_en_v('01-09031323-D1960-9999-0Y0  -');
%rempl_d_en_v('01-09033626-D1932-9999-0Y0  -');
%rempl_d_en_v('01-09080028-D1956-9999-0Y0  -');
%change_statut('01-09008722-D1926-9999-000  -',V);

* mis en divorc� les mari�s; 
%change_statut('01-09057845-M1952-9999-000  -',D);
%change_statut('01-09067061-M1959-9999-000  -',D);
%change_statut('01-09068643-M1944-9999-000  -',D);
%change_statut('01-09077104-M1974-9999-000  -F2001F2006F2009',D);
%change_statut('01-09093920-M1984-9999-000  -F2007',D);
%change_statut('02-09068643-M1943-9999-000  -',D);
%change_statut('01-09054317-M1951-9999-000  -F1991',D);
%change_statut('01-09095742-M1954-9999-000  -F1991',D);

*mis en mari� les gens divorc�s; 
%RemplaceIndivi('02-09089720-D1978-9999-0Y0  -','02-09089720-D1978-9999-X00 M-');
if declar2='01-09089720-O1978-1978-X00 M-' then declar2= '02'!!substr(declar2,3,10)!!'D'!!substr(declar2,14,5)!!'9999'!!substr(declar2,23,7);
%change_statut('01-09056557-D1967-1973-000  -',M);

	*d) changement des num�ros de noi des d�clar; 		   
%RemplaceIndivi('02-09044289-O1975-1975-0Y0 S-', '01-09044289-O1975-1975-0Y0 S-');


/*****************************************************************/
* II - gestion des pac; 
/*****************************************************************/

* ajout d'un pac dans le d�clar; 
if declar1='01-09095230-D1959-9999-000  -F1994' then declar1=substr(declar1,1,34)!!'J1987';
* suppression d'un pac d�j� d�clar� ailleurs; 
%RemplaceIndivi('01-09083789-D1971-9999-000  -F1990F1995F2006J1990','01-09083789-D1971-9999-000  -F1995F2006J1990');
%RemplaceIndivi('01-09043636-D1966-9999-000  -J1994','01-09043636-D1966-9999-000  -');
if ident='09043636' and noi='05' then declar1='02-09043636-D1960-9999-000  -J1994';
*changement de J en F pour les pac dans le d�clar; 
%RemplaceIndivi('01-09048526-D1982-9999-0Y0  -F1995', '01-09048526-D1982-9999-0Y0  -F2005');
%RemplaceIndivi('01-09079488-C1957-9999-000  -F1999F1986','01-09079488-C1957-9999-000  -F1999J1986');

/*****************************************************************/
* III - Suppression des d�clar ou plus t�t transformation du quelfic des gens;
/*****************************************************************/

* vient de verif3; 
* on met en EE les gens qui dont on a oubli� de nous donner les d�clarations fiscales; 
if noindiv in ('0907087902' '0909018801' '0907990501' '0907323801' '0907323803' '0907323805' 
'0906615604' '0906615601' '0905566601' '0905566603' '0905566604' '0909277901' '0909277904' '0908599903') then do; 
persfip=''; persfipd=''; declar1=''; quelfic='EE'; 
end; 

* un jeune qui est sur la declaration de sa mere; 
if declar1='03-09069953-D1962-9999-000  -J1989' then do; 
declar1='02'!!substr(declar1,3,length(declar1)-3+1); 
persfip='pac'; 
persfipd=''; 
end; 

if declar1='03-09095230-D1959-9999-000  -F1994J1987' then do;
declar1='01'!!substr(declar1,3,length(declar1)-3+1); 
persfip='pac'; 
persfipd=''; 
end; 

if declar1='03-09085999-M1948-1951-000  -F1991' then do;
	declar1='01'!!substr(declar1,3,length(declar1)-3+1); 
	if noi='03' then do; 
		persfip='pac'; 
		persfipd=''; 
	end; 
end; 
			
run; 

data travail.indfip09; 
set travail.indfip09; 

if noindiv='0900937891' or noindiv='0904134991' then persfipd = 'mad';
if noindiv='0907033481' then persfipd = 'pac';
if noindiv='0907152081' then persfip = 'pac'; 
if noindiv='0907506081' then persfipd = 'conj';
*cas d'un FIP ou la personne d�clare des revenus d'un conjoint mais �tre divorc�; * on marie les gens avec le FIP; 
* sinon il faudrait supprimer le FIP; 
%RemplaceIndivi('01-09004056-D1964-9999-0Y0  -F1992','01-09004056-M1964-1964-0Y0 S-F1992');
* cas d'un FIP dans un couple ou la personne d�clare �tre divorc�; 
%RemplaceIndivi('01-09056557-D1967-1973-000  -', '01-09056557-M1967-1973-000  -');

%RemplaceIndivi('01-09079488-C1957-9999-000  -F1999F1986','01-09079488-C1957-9999-000  -F1999J1986');
%change_statut('01-09077104-M1974-9999-000  -F2001F2006F2009',D);
%RemplaceIndivi('01-09083789-D1971-9999-000  -F1990F1995F2006J1990','01-09083789-D1971-9999-000  -F1995F2006J1990');
%change_statut('01-09093920-M1984-9999-000  -F2007',D);
%change_statut('01-09095742-M1954-9999-000  -F1991',D);
if declar1='03-09095230-D1959-9999-000  -F1994J1987' then declar1='01'!!substr(declar1,3,length(declar1)-3+1); 
if declar1='03-09069953-D1962-9999-000  -J1989' then declar1='02'!!substr(declar1,3,length(declar1)-3+1); 
if declar1='01-09049141-M1921-1928-000 Z-' and noi='01' then do; 
	declar1='02'!!substr(declar1,3,28-3)!!' '!!substr(declar1,29,length(declar1)-29+1); 
	persfip='conj'; 
end; 

*les FIP dont on ne retrouve pas la d�claration fiscale sont supprim�s;
if noi='82' and ident='09066156' then delete; 
if noi='82' and ident='09085999' then delete; 

run; 


/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
