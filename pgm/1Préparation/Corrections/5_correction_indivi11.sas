*******************************************************
correction qui font suite aux fichiers verif1; 

/* mise en coh�rence en fonction des modifications effectu�es dans correction_foyer11*/

data travail.indivi11; 
	set travail.indivi11; 

	*a) changement de date de naissance du declarant dans le declar; 
	%RemplaceIndivi('02-11033769-C1892-9999-000  -F2009F2009','02-11033769-C1982-9999-000  -F2009F2009');
	%RemplaceIndivi('01-11093635-M1950-1854-000  -','01-11093635-M1950-1954-000  -')

	*b) Modification du statut mdcco du d�clarant; 

	* les gens mari�s qui d�clare �tre divorc� au lieu d'�tre veuf lorsque leur conjoint d�c�de;
	%rempl_d_en_v('02-10049169-D1926-9999-0Y0  -');

	* mis en divorc� les mari�s; 
	%change_statut('01-11001567-M1954-9999-000  -F2009',D);
	%change_statut('01-11002180-M1975-9999-000  -F1993F1994F2005',D);
	%change_statut('01-11008781-M1964-9999-000  -',D);
	%change_statut('01-11010208-M1953-9999-000  -',D);
	%change_statut('03-11058052-M1945-9999-000  -',D);
	%change_statut('01-11063244-M1952-9999-000  -',D);
	%change_statut('03-11087106-M1972-9999-000  -',D);
	%change_statut('01-11094958-M1968-9999-000  -',D);
	%change_statut('01-11096792-M1959-9999-000  -',D);


	* mis en divorce de gens c�libataires;
	%change_statut('01-11087258-C1989-9999-0Y0  -',D);
	%change_statut('02-11092122-C1979-9999-0Y0  -H2001',D);
	%change_statut('01-11092122-C1980-9999-0Y0  -F2005',D);
	%change_statut('01-11092459-C1974-9999-0Y0  -',D);
	%change_statut('02-11102479-C1974-9999-0Y0  -F2005F2008',D);
	%change_statut('01-11110761-C1944-9999-0Y0  -',D);
	%change_statut('01-11112182-C1979-9999-0Y0  -',D);
	%change_statut('01-11112270-C1989-9999-0Y0  -',D);

	/* mis en c�libataire des gens mari�s sans conjoint */
	%change_statut('01-11112052-M1961-9999-X00  -',C);

	/*on met en veuf les gens mari�s qui n'ont pas de conjoints sur leur d�claration et qui d�clare un d�c�s dans l'ann�e*/
	%change_statut('01-11025119-M1934-9999-00Z Z-',V);
	%change_statut('01-11029731-M1957-9999-00Z Z-',V);
	%change_statut('01-11103263-M1924-9999-00Z Z-',V);

	/* On corrige quelques declar */
	%RemplaceIndivi(ancien='01-11000343-M1939-1937-000  -',
					nouveau='01-11000343-M1939-1937-00Z Z-',
					sif='SIF M1939 1937 000000000000000    000000000000000000000000000D   F00G00R00J00N00H00I00P00 00 00');
	%RemplaceIndivi(ancien='01-11013121-M1922-1919-000  -',
					nouveau='01-11013121-M1922-1919-00Z Z-',
					sif='SIF M1922 1919 000000000000000    000000000000000000000000000D   F00G00R00J00N00H00I00P00 00 00');
	%RemplaceIndivi(ancien='01-11031949-M1936-1940-000  -',
					nouveau='01-11031949-M1936-1940-00Z Z-',
					sif='SIF M1936 1940 000000000000000    000000000000000000000000000D   F00G00R00J00N00H00I00P00 00 00');	
	%RemplaceIndivi(ancien='01-11042716-M1950-1953-000  -',
					nouveau='01-11042716-M1950-1953-00Z Z-',
					sif='SIF M1950 1953 000000000000000    000000000000000000000000000    F00G00R00J00N00H00I00P00 00 00');
	%RemplaceIndivi(ancien='02-11035060-M1944-1945-000  -',
					nouveau='02-11035060-M1944-1945-00Z Z-',
					sif='SIF M1944 1945 0F0000000000000  F 000000000000000000000000000D   F00G00R00J00N00H00I00P00 00 00');
	%RemplaceIndivi(ancien='01-11045002-M1926-1944-000  -',
					nouveau='01-11045002-M1926-1944-00Z Z-',
					sif='SIF M1926 1944 000000000000000    000000000000000000000000000D   F00G00R00J00N00H00I00P00 00 00');

	%RemplaceIndivi(ancien='02-11053111-M1923-1928-000  -',
					nouveau='02-11053111-M1928-1928-00Z Z-',
					sif='SIF M1928 1928 000000W00  W 000000000000000000Z07082011Z   F00G00R00J00N00H00I00P00 00');
	%RemplaceIndivi(ancien='01-11053111-D1928-9999-0Y0  -',
					nouveau='01-11053111-V1928-9999-00Z  -',
					sif='SIF V1928 9999 000000W00  W 000000000000000000Z07082011    F00G00R00J00N00H00I00P00 00');
	run; 



/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
