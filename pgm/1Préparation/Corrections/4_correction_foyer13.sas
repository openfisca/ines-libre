/***************************/
/*  4_correction_foyer13   */
/***************************/

/* Les corrections ci-dessous font suite au programme 4_Controles_Niveau_Foyer */
/****************************************/
/*	En entr�e : travail.foyer&anr.		*/
/*	En sortie : travail.foyer&anr.		*/
/****************************************/

data travail.foyer&anr.;
	set travail.foyer&anr.;
	
	/* Incoh�rence dans la d�claration d'un enfant (adulte handicap�) */
	%RemplaceDeclar(ancien=	'02-13004331-M1946-1944-000  -F1967G1967',
					nouveau='02-13004331-M1946-1944-000  -G1967',
					sif=	'SIF M1946 1944 000000000    000000000000000000000000000    F00G01R00J00N00H00I00P00 00 00');

	/* Incoh�rence entre Declar et VousConj (ici couple dont l'un est toujours mari�, et confusion dans le Declar entre l'ancien et le nouveau conjoint) */
	%RemplaceDeclar(ancien=	'02-13030088-M1960-1956-000  -',
					nouveau='02-13030088-M1960-1959-000  -');

	/* Incoh�rence dates de naissance des enfants */
	%RemplaceDeclar(ancien=	'01-13003108-C1972-9999-000  -F2001F2005F1994J1994',
					nouveau='01-13003108-C1972-9999-000  -F2001F2005J1994');
	%RemplaceDeclar(ancien=	'01-13007910-V1979-9999-000  -F2013F2005F2003F1996',
					nouveau='01-13007910-V1979-9999-000  -F2013F2005F2003F1996',
					sif=	'SIF V1979 9999 00000000N000000    000000000000000000000000000    F04G00R00J00N00H00I00P00 00 00');
	%RemplaceDeclar(ancien=	'01-13021231-C1985-9999-000  -F2010F2010H2006',
					nouveau='01-13021231-C1985-9999-000  -F2013F2010H2006',
					sif=	'SIF C1985 9999 000000000000000    000000000000000000000000000    F02G00R00J00N00H01I00P00 00 00');

	/* Standardisation table foyer */
	%Standard_foyer;
	run;


/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
