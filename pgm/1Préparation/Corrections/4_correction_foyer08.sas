*******************************************************
correction qui font suite aux fichiers verif2;

* on reprend aussi les modifications de correction1_08 qui ont une cons�quence sur les d�clarations.; 
 
data travail.foyer08; set travail.foyer08; 
	*il faut changer le sif avant le declar puisqu'on regarde change en quand declar=tant;
	%suppr_even('01-08023726-M1962-1969-X00  -');
	if declar='01-08023726-M1962-1969-0Y0 S-' then delete;
	%RemplaceDeclar('01-08023726-M1962-1969-X00  -','01-08023726-M1962-1969-000   ')*retirer l'even dans le sif;


	*veufs qui se mettent en divorc�;
	%RemplaceEvenement('01-08014146-D1937-9999-0Y0  -',V,Y,Z);
	%RemplaceEvenement('02-08025374-D1931-9999-0Y0  -',V,Y,Z);
	%RemplaceEvenement('02-08027744-D1933-9999-0Y0  -',V,Y,Z);
	%RemplaceEvenement('01-08033344-D1933-9999-0Y0  -',V,Y,Z);
	%RemplaceEvenement('01-08078064-D1929-9999-0Y0  -',V,Y,Z);
	%RemplaceEvenement('01-08084982-D1946-9999-0Y0  -',V,Y,Z);


	*mari� puis veuf en toute fin d'ann�e, on supprime le veuvage (il est en fin d'ann�e et il n'y 
	a pas de d�claration apr�s);
	%RemplaceEvenement('01-08020047-M1931-1942-X0Z Z-',M,X,X,);

	*erreur un marriage et non un divorce;
	%RemplaceEvenement('02-08023477-D1978-9999-0Y0  -',D,Y,X,M);

	*un veuf est en fait divorc�, on le fait � la main parce qu'il n'y a pas de date remplie; 
	if declar='01-08030367-V1949-9999-00Z  -'
	then sif= substr(sif,1,4)!!"D" !!substr(sif,6,29)!!"000000000"!!"Y" !!
	"01012008"!!"000000000"!!" " !!substr(sif,63,35);
	%RemplaceDeclar('01-08030367-V1949-9999-00Z  -','01-08030367-D1949-9999-0Y0  -');


	*statut ne va pas avec le fit qu'il y ait un seul d�clarant;
	%RemplaceStatutOld('01-08082673-M1960-9999-000  -',C);
	%RemplaceDeclar('01-08082673-M1960-9999-000  -','01-08082673-C1960-9999-000  -');

	*naia;
	if declar = '01-08076061-D1999-9999-000  -' then sif= substr(sif,1,5)!!"1957"!!substr(sif,10,88);
	%RemplaceDeclar('01-08076061-D1999-9999-000  -','01-08076061-D1957-9999-000  -');

	*verif avant/apr�s evenement;
	if declar ='02-08050520-C1964-9999-X00 M-F1994J1989' 
	then sif=substr(sif,1,61)!!"M" !!substr(sif,63,35);

	*diff�rence entre declar et sif;
	%RemplaceEvenement('01-08009997-V1964-9999-00Z  -F1999',V,X,Z, );
	%RemplaceEvenement('02-08057169-D1958-9999-0Y0  -',D,Y,Y, );
	%ajout_date('01-08030311-C1986-9999-X00 M-',C,X,16122008,M);
	%ajout_date('02-08077036-C1981-9999-X00 M-',C,X,08122008,M);

	%RemplaceDeclar('02-08077911-C1983-9999-000  -','02-08077911-C1983-9999-X00 M-');
	%ajout_date('02-08081940-C1978-9999-X00 M-',C,X,25102008,M);
	%ajout_date('01-08083247-C1930-9999-X00 M-',C,X,11092008,M);




	*un D qui n'a pas lieu d'�tre.;
	if declar ='01-08004724-M1934-1935-00Z Z-' then sif=substr(sif,1,61)!!"Z" !!substr(sif,63,35);
	if declar ='01-08014536-M1925-1930-00Z Z-' then sif=substr(sif,1,61)!!"Z" !!substr(sif,63,35);
	if declar ='01-08016042-M1934-1938-00Z Z-' then sif=substr(sif,1,61)!!"Z" !!substr(sif,63,35);
	if declar ='01-08019414-M1930-1932-00Z Z-' then sif=substr(sif,1,52)!!"Z29112008Z" !!substr(sif,63,35);
	if declar ='02-08028191-M1930-1931-00Z Z-' then sif=substr(sif,1,52)!!"Z06122008Z" !!substr(sif,63,35);
	if declar ='01-08032782-M1932-1940-00Z Z-' then sif=substr(sif,1,61)!!"Z" !!substr(sif,63,35);


	if declar ='01-08009725-M1919-1921-000  -' then sif=substr(sif,1,61)!!" " !!substr(sif,63,35);
	if declar ='01-08011147-M1925-1930-000  -' then sif=substr(sif,1,61)!!" " !!substr(sif,63,35);
	if declar ='01-08009725-M1919-1921-000  -' then sif=substr(sif,1,61)!!" " !!substr(sif,63,35);
	if declar ='01-08033375-M1922-1934-000  -' then sif=substr(sif,1,61)!!" " !!substr(sif,63,35);
	if declar ='01-08045572-M1947-1947-000  -' then sif=substr(sif,1,61)!!" " !!substr(sif,63,35);
	if declar ='01-08070898-M1934-1934-000  -' then sif=substr(sif,1,61)!!" " !!substr(sif,63,35);

	%RemplaceDeclar('08-08003555-M1955-1955-000  -F1991J1986J1985J1983','08-08003555-M1955-1955-000  -F1991J1986J1985');
	%RemplaceDeclar('01-08046013-D1956-9999-X00 M-','01-08046013-D1956-9999-X00 M-F1990');
	%RemplaceDeclar('01-08072910-M1968-1961-000  -F1990F1991F1993F1995F1997F2000J1986J198',
	'01-08072910-M1968-1961-000  -F1990F1991F1993F1995F1997F2000J1986J1989');
	* jeune en trop;
	if declar='02-08068024-C1984-9999-000  -J1984' then anaisenf=''; 
	if declar='01-08068024-D1961-9999-000  -F1994J1984' then anaisenf='F1994'; 
	if declar='02-08070737-M1950-1957-000  -J1986' then anaisenf=''; 
	if declar='03-08070737-C1986-9999-000  -J1986' then anaisenf=''; 
	if declar='01-08073039-M1957-1959-000  -F1994J1985J1989' then anaisenf='F1994J1989'; 
	if declar='03-08073039-C1985-9999-000  -J1989' then anaisenf=''; 
	%RemplaceDeclar('02-08068024-C1984-9999-000  -J1984','02-08068024-C1984-9999-000  -');
	%RemplaceDeclar('01-08068024-D1961-9999-000  -F1994J1984','01-08068024-D1961-9999-000  -F1994');
	%RemplaceDeclar('03-08070737-C1986-9999-000  -J1986','03-08070737-C1986-9999-000  -');
	%RemplaceDeclar('02-08070737-M1950-1957-000  -J1986','02-08070737-M1950-1957-000  -');
	%RemplaceDeclar('01-08073039-M1957-1959-000  -F1994J1985J1989','01-08073039-M1957-1959-000  -F1994J1989');
	%RemplaceDeclar('03-08073039-C1985-9999-000  -J1989','03-08073039-C1985-9999-000  -');

	*une d�claration en trop dans ce m�nage; 
	if declar='02-08061787-D1972-9999-000  -F1993' then delete;

	*on change un d�calage qu'il y a parfois sur anaisenf des enfants; 
	if substr(anaisenf,1,1)='' & substr(anaisenf,2,1) ne '' then anaisenf=substr(anaisenf,2,39);
	if substr(declar,30,1)='' & substr(declar,31,1) ne '' then declar=substr(declar,1,29)!!substr(declar,31,39);


	*IV - remplissage des cases fiscales; 
	%standard_foyer;

	run;



/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
