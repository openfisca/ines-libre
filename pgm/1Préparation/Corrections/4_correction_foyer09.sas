*******************************************************
correction qui font suite aux fichiers verif2;

* on reprend aussi les modifications de correction1_&an qui ont une cons�quence sur les d�clarations.; 

 

data travail.foyer09; set travail.foyer09; 

	*c) changement de date de naissance du declarant dans le declar; 
	%RemplaceDeclar('01-09014359-M1961-1958-000  -F1991F1992','01-09014359-M1950-1960-000  -F1991F1992');

	* I - modification des �v�nements;  
	*v�rif2 : coh�rence des statuts et des �v�nements;

	*a) modification de la case 62 dans le SIF; 
	if declar='02-09042517-C1983-9999-X00 M-' then sif=substr(sif,1,61)!!"M"!!substr(sif,63,35); 
	if declar='02-09045326-C1981-9999-X00 M-' then sif=substr(sif,1,61)!!"M"!!substr(sif,63,35); 
	if declar='02-09096017-C1958-9999-X00 M-' then sif=substr(sif,1,61)!!"M"!!substr(sif,63,35); 
	if declar='01-09080456-C1968-9999-X00 M-F2001' then sif=substr(sif,1,61)!!"M"!!substr(sif,63,35); 
	if declar='01-09026245-D1961-9999-0Y0  -J1990' then sif=substr(sif,1,61)!!" "!!substr(sif,63,35);
	if declar='01-09017798-V1942-9999-00Z  -' then sif=substr(sif,1,61)!!" "!!substr(sif,63,35); 
	if declar='02-09045785-M1931-1934-000  -' then sif=substr(sif,1,61)!!" "!!substr(sif,63,35); 
	if declar='02-09023220-M1924-1929-000  -' then sif=substr(sif,1,61)!!" "!!substr(sif,63,35); 
	if declar='01-09077373-M1936-1938-000  -' then sif=substr(sif,1,61)!!" "!!substr(sif,63,35); 
	if declar='01-09064465-M1925-1929-000  -' then sif=substr(sif,1,61)!!" "!!substr(sif,63,35); 
	if declar='01-09035254-M1923-1932-000  -' then sif=substr(sif,1,61)!!" "!!substr(sif,63,35); 
	if declar='01-09049141-M1921-1928-000  -' then do; sif=substr(sif,1,61)!!" "!!substr(sif,63,35);
	declar='02'!!substr(declar,3,42); noindiv='0904914102'; end; 
	* en vrai c'est faux car si on inverse declarant et conjoin, on n'inverse pas les revenus dans indivi!; 
	if declar='02-09086623-C1981-9999-000  -' then sif=substr(sif,1,61)!!" "!!substr(sif,63,35); 
	if declar='01-09035390-M1921-1923-00Z Z-' then sif=substr(sif,1,61)!!"Z"!!substr(sif,63,35); 
	* suppression de case 62 dans le d�clar; 
	if declar='01-09018032-C1963-9999-X00 M-' then
	 sif='SIF C1963 9999 000000000000000    000000000000000000000000000   F00G00R00J01N00H00I00P00 00 00';
	%RemplaceDeclar('01-09018032-C1963-9999-X00 M-', '01-09018032-C1963-9999-000  -');
	if declar='01-09008722-D1926-9999-000  -' then 
	sif='SIF V1926 9999 00000P000000000  P 000000000000000000000000000   F00G00R00J00N00H00I00P00 00 00';

	*b) Ajout/suppression d'un �v�nement; 

	* ajout d'une �v�nement (d�c�s); 
	%ajout_date('01-09012994-M1922-1929-00Z Z-',M,Z,01102009,Z); 
	%RemplaceDeclar('01-09024462-M1928-1926-000  -','01-09024462-M1928-1926-00Z Z-');
	%ajout_date('01-09024462-M1928-1926-00Z Z-',M,Z,25102009,Z); 
	* on enl�ve la mort de quelqu'un pour simplifier la situation du vopa qui vit avec; 
	%RemplaceDeclar('01-09034873-V1970-9999-00Z  -F1991F1995F1996F1997F1999F2008','01-09034873-V1970-9999-000  -F1991F1995F1996F1997F1999F2008');
	if declar='01-09034873-V1970-9999-000  -F1991F1995F1996F1997F1999F2008' then 
	sif='SIF V1970 9999 000000000000000    000000000000000000000000000   F06G00R00J00N00H00I00P00 00 00';


	*c) Modification du statut mdcco du d�clarant; 

	*mise en veuf les divorc�s;
	%RemplaceEvenement('02-09003178-D1925-9999-0Y0  -',V,Y,Z);
	%RemplaceEvenement('02-09010699-D1936-9999-0Y0  -',V,Y,Z);
	%RemplaceEvenement('01-09016027-D1930-9999-0Y0  -',V,Y,Z);
	%RemplaceEvenement('02-09016475-D1929-9999-0Y0  -',V,Y,Z);
	%RemplaceEvenement('01-09017156-D1938-9999-0Y0  -',V,Y,Z);
	%RemplaceEvenement('01-09026171-D1951-9999-0Y0  -',V,Y,Z);
	%RemplaceEvenement('02-09029486-D1922-9999-0Y0  -',V,Y,Z);
	%RemplaceEvenement('01-09031323-D1960-9999-0Y0  -',V,Y,Z);
	%RemplaceEvenement('01-09033626-D1932-9999-0Y0  -',V,Y,Z);
	%RemplaceEvenement('01-09080028-D1956-9999-0Y0  -',V,Y,Z);

	%RemplaceStatutOld('01-09008722-D1926-9999-000  -',V);

	* ajout du divorce dans la d�claration; 
	%RemplaceDeclar('01-09004056-D1964-9999-0Y0  -F1992','01-09004056-M1964-1964-0Y0 S-F1992');
	if declar ='01-09004056-M1964-1964-0Y0 S-F1992' then sif=substr(sif,1,4)!!'M1964-1964'!!
	substr(sif,15,47)!!"S"!!substr(sif,63,35);

	*mis en divorc� les gens mari�s; 
	%RemplaceStatutOld('01-09067061-M1959-9999-000  -',D);
	%RemplaceStatutOld('01-09068643-M1944-9999-000  -',D);
	%RemplaceStatutOld('01-09077104-M1974-9999-000  -F2001F2006F2009',D);
	%RemplaceStatutOld('01-09093920-M1984-9999-000  -F2007',D);
	%RemplaceStatutOld('01-09095742-M1954-9999-000  -F1991',D);
	%RemplaceStatutOld('02-09068643-M1943-9999-000  -',D);
	%RemplaceStatutOld('01-09057845-M1952-9999-000  -',D);
	%RemplaceStatutOld('01-09054317-M1951-9999-000  -F1991',D);
	*mis en mari� les gens divorc�; 
	%RemplaceDeclar('02-09089720-D1978-9999-0Y0  -','02-09089720-D1978-9999-X00 M-');
	%RemplaceEvenement('02-09089720-D1978-9999-X00 M-',D,Y,X,M);
	%RemplaceStatutOld('01-09056557-D1967-1973-000  -',M);

	*d) changement des num�ros de noi des d�clar; 
	if declar='02-09044289-O1975-1975-0Y0 S-' then declar='01'!!substr(declar,3,length(declar)-3+1); 
	if declar='03-09095230-D1959-9999-000  -F1994J1987' then declar='01'!!substr(declar,3,length(declar)-3+1); 
	if declar='03-09069953-D1962-9999-000  -J1989' then declar='02'!!substr(declar,3,length(declar)-3+1); 
	if declar='03-09085999-M1948-1951-000  -F1991' then declar='01'!!substr(declar,3,length(declar)-3+1); 

	* II - gestion des pac; 
	* voir programme verif 2; 

	* ajout d'un pac dans le d�clar; 
	%RemplaceDeclar('01-09095742-M1954-9999-000  -','01-09095742-M1954-9999-000  -F1991');
	%RemplaceDeclar('01-09093920-M1984-9999-000  -','01-09093920-M1984-9999-000  -F2007');
	%RemplaceDeclar('01-09077104-M1974-9999-000  -','01-09077104-M1974-9999-000  -F2001F2006F2009');

	* suppression d'un pac d�j� d�clar� ailleurs; 
	%RemplaceDeclar('01-09043636-D1966-9999-000  -J1994','01-09043636-D1966-9999-000  -');
	if declar ='01-09043636-D1966-9999-000  -' then do; 
		anaisenf='';
		sif=substr(sif,1,23)!!'0'!!substr(sif,25,8)!!' '!!substr(sif,34,61);
	end; 
	%RemplaceDeclar('01-09083789-D1971-9999-000  -F1990F1995F2006J1990','01-09083789-D1971-9999-000  -F1995F2006J1990');
	if declar='01-09083789-D1971-9999-000  -F1995F2006J1990' then anaisenf='F1995F2006J1990';

	* ajout d'un pac dans le SIF; 
	if declar='02-09057158-M1957-1958-000  -J1985J1988' then sif=substr(sif,1,74)!!'02'!!substr(sif,77,18);
	if declar='01-09071059-C1963-9999-000  -J1989J1990' then sif=substr(sif,1,74)!!'02'!!substr(sif,77,18);
	*changement de J en F pour les pac dans le d�clar; 
	if declar='01-09079488-C1957-9999-000  -F1999F1986' then do; 
	declar='01-09079488-C1957-9999-000  -F1999J1986'; anaisenf='F1999J1986'; 
	end; 
	%RemplaceDeclar('01-09048526-D1982-9999-0Y0  -F1995','01-09048526-D1982-9999-0Y0  -F2005');

	*changement de J en F pour les pac dans le SIF; 
	if declar='01-09085383-D1963-9999-000  -F1994' then sif=substr(sif,1,65)!!'01'!!substr(sif,68,7)!!'00'!!substr(sif,77,18); 
	if declar='01-09070879-V1950-9999-000  -F2003' then sif=substr(sif,1,65)!!'01'!!substr(sif,68,7)!!'00'!!substr(sif,77,18); 
	if declar='02-09049439-M1962-1962-000  -J1990J1988' then sif=substr(sif,1,74)!!'02'!!substr(sif,77,1)!!'00'!!substr(sif,80,15); 

	if declar='01-09048526-D1982-9999-0Y0  -F2005' then anaisenf='F2005';

	* III - Suppression de la d�claration;

	* vient de verif3; 
	* des declarations qui renvoient � des m�nages EE avec d�j� des revenus imput�s; 
	if declar not in('01-09068803-C1985-9999-000  -' '03-09056769-C1989-9999-000  -' '01-09078902-C1986-9999-000  -'
	'03-09057443-C1989-9999-000  -' '03-09058249-O1986-1986-X00  -' '02-09058943-C1984-9999-000  -' '04-09064005-C1989-9999-000  -'
	'02-09066517-C1987-9999-000  -' '02-09073702-C1986-9999-000  -' '03-09073702-C1988-9999-000  -' '01-09079567-C1989-9999-000  -' 
	'02-09080451-C1988-9999-000  -' '01-09089217-C1990-9999-000  -' '01-09090659-C1991-9999-000  -' '02-09094775-C1984-9999-000  -' 
	'01-09095278-C1987-9999-000  -' '03-09058249-C1986-9999-X00 M-');

	*IV - remplissage des cases fiscales; 
	%standard_foyer;

	run;


/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
