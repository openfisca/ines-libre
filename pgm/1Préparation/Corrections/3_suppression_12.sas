/*****************************************************/
/*			programme 3_suppression_12               */ 
/*****************************************************/

/**************************************************************************************************************/
/* Tables en entr�e :                                                                                         */
/*	travail.foyer&anr.																						  */		
/*  travail.irf&anr.e&anr																					  */
/*  travail.mrf&anr.e&anr																					  */
/*  travail.indivi&anr.																						  */
/*  travail.indfip&anr.																						  */
/*  travail.menage&anr.																				          */
/* 																											  */
/* Tables en sortie :																						  */
/*	travail.foyer&anr.																						  */
/*  travail.irf&anr.e&anr																					  */
/*  travail.mrf&anr.e&anr																					  */
/*  travail.indivi&anr.																						  */
/*  travail.indfip&anr.																			              */
/*  travail.menage&anr.																						  */
/*																											  */
/* Objectif : Dans ce programme, on supprime des diff�rentes tables de la biblioth�que travail,               */
/*  les m�nages que l'on consid�re comme mal renseign�s.													  */	   
/**************************************************************************************************************/


/* 1ere ligne : La pr�sence de 10 observations de la table indiv2012_ela sans correspondance dans la table foyer12_ela 
provient d�un d�faut de la collecte 2013 de l�enqu�te Emploi. En effet, certains m�nages enqu�t�s 6 fois 
auraient �t� de nouveau enqu�t�s sous un identifiant(nomen) diff�rent.
Des doublons sont ainsi pr�sents dans la table indiv2012_ela. Ces 10 observations (8 m�nages) sont � supprimer. 

2e ligne : 9 m�nages pr�sents dans foyer12_ela ont les variables Declar1 et Declar2 � blanc dans indiv2012_ela.
Il s�agit l� d�une erreur qui persiste depuis la mise en place d�Ines. 
Le souci se situe au niveau de la correction de la non r�ponse pour les m�nages dont la personne de r�f�rence 
est �tudiante (table EEC : cstotprm= � 84 �). Toutes les personnes du m�nage sont consid�r�es non appari�es. 
Declar1 et Declar2 se retrouvent � blanc, les montants de revenus �galement et ceci pour tous les membres de ces m�nages. 
Or dans ces 9 m�nages, au moins une personne est d�clarante. */

%suppression(	'12087434' '12087879' '12090212' '12093380' '12094155' '12095263' '12094121' '12099207'
				'12064152' '12075285' '12076696' '12078285' '12078382' '12080793' '12105363' '12110967' '12111048');


/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
