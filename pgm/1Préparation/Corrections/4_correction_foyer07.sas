/* correction bien faite � partir de 2008, ce programme est fait pour voir si
on peut faire tourner ines sur ERFS 2007 et faire l'�tude panel*/

data travail.foyer07(drop=nof_f);; set travail.foyer07;


if substr(anaisenf,1,1)='' & substr(anaisenf,2,1) ne '' then anaisenf=substr(anaisenf,2,39);
if substr(declar,30,1)='' & substr(declar,31,1) ne '' then declar=substr(declar,1,29)!!substr(declar,31,39);


nof_f=ident!!noi!!mcdvo!!xyz;

*---- corrections incosifdec ------------------------------;
 
if declar= '01-07053001-V1950-9999-00Z  -' then 
sif= substr(sif,1,33)!!'000000000'!!substr(sif,43,9)!!'Z23062007 '!!substr(sif,63,32);
if declar= '01-07053001-M1950-1947-00Z Z-' then 
sif= substr(sif,1,33)!!'000000000'!!substr(sif,43,9)!!'Z23062007Z'!!substr(sif,63,32);

if nof_f='0700014502VZ' then sif=substr(sif,1,61)!!' '!!substr(sif,63,32);
if nof_f='0700583502VZ' then sif=substr(sif,1,61)!!' '!!substr(sif,63,32);
if nof_f='0701249401VZ' then sif=substr(sif,1,61)!!' '!!substr(sif,63,32);
if nof_f='0701709401VZ' then sif=substr(sif,1,61)!!' '!!substr(sif,63,32);

if nof_f='0700916007DX' then sif=substr(sif,1,61)!!'M'!!substr(sif,63,32);
if nof_f='0701636501MY' then sif=substr(sif,1,61)!!'S'!!substr(sif,63,32);
if nof_f='0703196701MZ' then sif=substr(sif,1,61)!!'D'!!substr(sif,63,32);

if nof_f='0700506701DY' then sif=substr(sif,1,43)!!'Y30062007'!!substr(sif,53,42);
if nof_f='0700870902DY' then sif=substr(sif,1,43)!!'Y31102007'!!substr(sif,53,42);
if nof_f='0701995302DY' then sif=substr(sif,1,43)!!'Y01032007'!!substr(sif,53,42);
if nof_f='0704384703DY' then sif=substr(sif,1,43)!!'Y30062007'!!substr(sif,53,42);
if nof_f='0705055601DY' then sif=substr(sif,1,43)!!'Y01052007'!!substr(sif,53,42);

if nof_f='0700357002CX' then sif=substr(sif,1,34)!!'X27012007'!!substr(sif,44,17)!!'M'!!substr(sif,63,32);
if nof_f='0705284501CX' then sif=substr(sif,1,34)!!'X17082007'!!substr(sif,44,17)!!'M'!!substr(sif,63,32);
if nof_f='0700357002CX' then 
sif = 'SIF C1978 9999 000000000000000    X27012007000000000000000000M  F00G00R00J00N00H00I00P00 00 00';
if nof_f='0705284501CX' then 
sif=  'SIF C1980 9999 000000000000000    X17082007000000000000000000M  F00G00R00J00N00H00I00P00 00 00';

if nof_f='0701200002VZ' then sif=substr(sif,1,52)!!'Z25122007'!!substr(sif,62,33);

if nof_f='0701950601MZ' then sif=substr(sif,1,52)!!'Z30122007Z'!!substr(sif,63,32);
if nof_f='0702230802MZ' then sif=substr(sif,1,52)!!'Z29062007Z'!!substr(sif,63,32);
if nof_f='0703541201MZ' then sif=substr(sif,1,52)!!'Z13112007Z'!!substr(sif,63,32);

*---- corrections tropdat ----------------------------------;

if nof_f='0700331101M0' then do; sif=substr(sif,1,66)!!'3'!!substr(sif,68,27); nbf=3; end;
if nof_f='0707276601D0' then do; sif=substr(sif,1,66)!!'2'!!substr(sif,68,27); nbf=2; end;

*---- corrections invalf ----------------------------------;
if nof_f='0700787601DX' then do;
	anaisenf='F1992F1988G1988';
	nbf=2; nbg=1;
	declar=substr(declar,1,29)!!anaisenf;
end;
if nof_f='0705817201M0' then do;
	anaisenf='F1994J1987';
	nbf=1; nbj=1;
	declar=substr(declar,1,29)!!anaisenf;
end;
if nof_f='0706821201M0' then do;
	anaisenf='F1991G1991';
	nbf=1; nbg=1;
	declar=substr(declar,1,29)!!anaisenf;
end;
if nof_f='0707137702V0' then do;
	anaisenf='F1947G1947';
	nbf=1;nbg=1;
	declar=substr(declar,1,29)!!anaisenf;
end;

%macro div_en_veuf(decl);
if declar = &decl then do; 
	sif= substr(sif,1,4)!!'V'!!substr(sif,6,38)!!'000000000'!!'Z'!!substr(sif,45,8)!!' '!!substr(sif,63,32);
   declar = substr(&decl,1,12)!!'V'!!substr(&decl,14,10)!!'00Z'!!substr(&decl,27,3);
end; 
%mend div_en_veuf;
%macro div_en_veuf(decl);
if declar = &decl then do; 
	sif= substr(sif,1,4)!!'V'!!substr(sif,6,38)!!'000000000'!!'Z'!!substr(sif,45,8)!!' '!!substr(sif,63,35);
   declar = substr(&decl,1,12)!!'V'!!substr(&decl,14,10)!!'00Z'!!substr(&decl,27,3);
end; 
%mend div_en_veuf;
*---- gens qui se trompent dans une d�claration quand il
y en a deux (cf. prog nofind) ----------------------------------;
%div_en_veuf('01-07012532-D1947-9999-0Y0  -');
%div_en_veuf('01-07018028-D1927-9999-0Y0  -');
%div_en_veuf('01-07021714-D1930-9999-0Y0  -');
%div_en_veuf('01-07023293-D1960-9999-0Y0  -');
%div_en_veuf('01-07027710-D1917-9999-0Y0  -');
%div_en_veuf('01-07042237-D1922-9999-0Y0  -');
%div_en_veuf('01-07066547-D1927-9999-0Y0  -');
%div_en_veuf('01-07074486-D1928-9999-0Y0  -');
%div_en_veuf('02-07015465-D1916-9999-0Y0  -');

*IV - remplissage des cases fiscales; 
%standard_foyer;

run;


/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
