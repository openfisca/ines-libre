/*****************************************************/
/*			programme 3_suppression_10               */ 
/*****************************************************/

/**************************************************************************************************************/
/* Tables en entr�e :                                                                                         */
/*	travail.foyer&anr.																						  */		
/*  travail.irf&anr.e&anr																					  */
/*  travail.mrf&anr.e&anr																					  */
/*  travail.indivi&anr.																						  */
/*  travail.indfip&anr.																						  */
/*  travail.menage&anr.																				          */
/* 																											  */
/* Tables en sortie :																						  */
/*	travail.foyer&anr.																						  */
/*  travail.irf&anr.e&anr																					  */
/*  travail.mrf&anr.e&anr																					  */
/*  travail.indivi&anr.																						  */
/*  travail.indfip&anr.																			              */
/*  travail.menage&anr.																						  */
/*																											  */
/* Objectif : Dans ce programme, on supprime des diff�rentes tables de la biblith�que travail,                */
/*  les m�nages que l'on consid�re comme mal renseign�s.													  */	   
/**************************************************************************************************************/

/* 10/04/2012 JD : pas vraiment d'id�es pour rep�rer ces m�nages probl�matiques donc pour l'instant 
   je ne supprime aucune observation. A faire plus tard */

*il faut la verifier, elle n'a pas encore tourn�, en particulier, je ne suis pas sur de comment entrer
la liste, succession d'ident ou bien avec des guillemets, etc.;  
%macro suppression(liste); 
	data travail.foyer&anr.; set travail.foyer&anr.; 
	if ident not in (&liste); run; 
	data travail.irf&anr.e&anr; set travail.irf&anr.e&anr; 
	if ident not in (&liste); run; 
	data travail.mrf&anr.e&anr; set travail.mrf&anr.e&anr; 
	if ident not in (&liste); run; 
	data travail.indivi&anr.; set travail.indivi&anr.; 
	if ident not in (&liste); run; 
	data travail.indfip&anr.; set travail.indfip&anr.; 
	if ident not in (&liste); run; 
	data travail.menage&anr.; set travail.menage&anr.; 
	if ident not in (&liste); run; 
	%mend;

%suppression('10057394' '10064012' '10028278' '10037406' '10033118' '10067534'
		     '10022576' '10026364' '10027607' '10035210' '10044711' '10050285'
			 '10055173' '10097007' '10104040' '10050285'
			 '10023257'
			 '10028278'); 

/* 10057394, 10064012 : cas d'un mariage et d'un d�c�s dans l'ann�e
   10028278, 10037406, 10033118, 10067534 : cas d'un mariage et d'un divorce dans l'ann�e
   10022576, 10026364, 10027607, 10035210, 10044711, 10050285, 10055173, 10097007, 10104040, 10050285 : ces cas
   correspondent � des d�cla avec des �v�nements dans l'ann�e. 
   Ils ont deux d�clarations, l'�v�nement d�clar� est diff�rent dans chacune des d�clarations et leur statut matri
   ne correspond pas aux �v�nements d�clar�s 
   10023257 : m�nage avec un individu EE&FIP et un individu EE sans date de naissance
   10028278 : m�nage avec un wpela manquant => fait bugger un cumul (calage_AEEH) */




/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
