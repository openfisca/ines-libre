/********************************************************/
/*          Programme 2a_macros_init_foyer              */ 
/********************************************************/

/* Ce programme regroupe une s�rie de macros appel�es ensuite dans 2b_init_foyer : 
	%InitialisationSimple
	%ApparitionSimple
	%DisparitionSimple
	%TransfererCases
	%EchangerCases
	%SuppressionVariablesInutiles
	%Standard_Foyer
*/


%macro InitialisationSimple(listeCases);
    %let variables = %sysfunc(STRIP(&listeCases.)) ;
    %let variables = %sysfunc(COMPBL(&variables.)) ;
    %let nbVariables = %eval(%sysfunc( count( &variables.,%str( ) ) )+1);
    %do i=1 %to &nbVariables.;
        %let casesInutiles = %sysfunc(TRANWRD(&casesInutiles., %scan(&variables.,&i.,%str( )), %str()));
        %end;
    %let variables = %sysfunc(TRANWRD(&variables.,%nrquote( ),%str(=0;)));
    %let variables = &variables.%str(=0;);
    &variables.;
    %mend;

%macro ApparitionSimple(anneeApparition=,listeCases=);
	/**    Lorsque des cases apparaissent une ann�e donn�e dans le formulaire fiscal, ...
	  *    @param	anneeApparition		ann�e � partir de laquelle les cases sont pr�sentes dans le formulaire fiscal
	  *    @param	listeCases  		liste des cases qui apparaissent dans le formulaire fiscal  */
    %if &anref.<&anneeApparition. %then %do;
        %InitialisationSimple(&listeCases.);
        %end;
    %mend;

%macro DisparitionSimple(anneeDisparition=,listeCases=);
	/**    Lorsque des cases dispara�ssent une ann�e donn�e du formulaire fiscal, ...
	  *    @param    anneeDisparition  ann�e � partir de laquelle les cases ne sont plus pr�sentes dans le formulaire fiscal
	  *    @param    listeCases  liste des cases qui disparaissent du formulaire fiscal */
    %if &anref.>=&anneeDisparition. %then %do;
        %initialisationSimple(&listeCases.);
        %end;
    %mend;


%macro TransfererCases(transfert,dateApparitionCasesArrivee,cumulAvecArrivee=N,ponderations=,garderCaseorigine=N);
    
    %let positionFleche=%sysfunc(find(&transfert.,->));

    %let casesOrigine = %sysfunc( substrn(&transfert.,0,&positionFleche.) );
    %let casesOrigine = %sysfunc( strip(&casesOrigine.) );
    %let casesOrigine = %sysfunc( compbl(&casesOrigine.) );

    %let casesArrivee = %sysfunc( substrn(&transfert.,&positionFleche.+2) );
    %let casesArrivee = %sysfunc( strip(&casesArrivee.) );
    %let casesArrivee = %sysfunc( compbl(&casesArrivee.) );

    %let nbCasesOrigine = %eval(%sysfunc( count( &casesOrigine.,%str( ) ) )+1);
    %let nbCasesArrivee = %eval(%sysfunc( count( &casesArrivee.,%str( ) ) )+1);

	%let sommeCasesOrigine = 0;
    %if %length(&ponderations.)=0 %then %do i=1 %to &nbCasesOrigine.;
        %let sommeCasesOrigine = &sommeCasesOrigine. + %scan(&casesOrigine.,&i.,%str( )) ;
        %end;

	%else %do;
        %let ponderations = %sysfunc(strip(&ponderations.));
        %let ponderations = %sysfunc(compbl(&ponderations.));

        /* On v�rifie qu'il y a autant de pond�rations que de cases d'origne */
        %let nbponderations=%eval(%sysfunc( count( &ponderations.,%str( ) ) )+1);
        %if &nbponderations. ne &nbCasesOrigine. %then %do; 
            error "Il faut autant de pond�rations que de cases d'origine";
            abort(nolist);
            %end;

        %do i=1 %to &nbCasesOrigine.;
            %let sommeCasesOrigine = &sommeCasesOrigine. + %sysevalf(%scan(&ponderations.,&i.,%str( )))*%scan(&casesOrigine.,&i.,%str( )) ;
            %end;
        %end;

    %if &anref. < &dateApparitionCasesArrivee. %then %do;
        %do i=1 %to &nbCasesArrivee.;
            %let somme = (&sommeCasesOrigine.)/&nbCasesArrivee.;
            %if &cumulAvecArrivee=O %then %do;
                %let somme = sum(%scan(&casesArrivee.,&i.), &somme.);
                %end;
            %scan(&casesArrivee.,&i.)=&somme.;
            %let casesInutiles = %sysfunc(TRANWRD(&casesInutiles., %scan(&casesArrivee.,&i.,%str( )), %str()));
            %end;
        %if &garderCaseorigine =O %then %do; 
            %do i=1 %to &nbCasesOrigine.;
                %let casesInutiles = &casesInutiles. %scan(&casesOrigine.,&i.);
                %end;
            %end;
        %end;
    %mend;


%macro EchangerCases(echange,anneeEchange);

	%let positionFleche=%sysfunc(find(&echange.,<->));

    %let caseGauche = %sysfunc( substrn(&echange.,0,&positionFleche.) );
    %let caseGauche = %sysfunc( strip(&caseGauche.) );
    %let caseGauche = %sysfunc( compbl(&caseGauche.) );

    %let caseDroite = %sysfunc( substrn(&echange.,&positionFleche.+3) );
    %let caseDroite = %sysfunc( strip(&caseDroite.) );
    %let caseDroite = %sysfunc( compbl(&caseDroite.) );

 	 %if &anref. < &anneeEchange. %then %do;
		%transfererCases(&caseGauche. -> sauve,&anneeEchange.);
		%transfererCases(&caseDroite. -> &caseGauche.,&anneeEchange.);
		%transfererCases(sauve -> &caseDroite.,&anneeEchange.);
		%let casesInutiles = &casesInutiles. sauve;
        %end;
	%mend;

%macro SuppressionVariablesInutiles();
    drop &casesInutiles. ;
    %mend;


%macro Standard_Foyer;
	/* ATTENTION : 	les modifications de cette partie doivent �tre r�percut�es dans le 
					programme de cr�ation des foyers de EE et EE_CAF */
	length  xyz mcdvo case_e case_f case_g case_k case_l case_p case_s case_w case_n case_t case_l2 $1  
			nbf nbg nbr nbj nbn nbh nbi agec aged ageh 3
			anaisd anaisc anaih $4
			vousconj $9 
			nbenf $24 	
			jourev moisev $2;	

	/* Cr�ation de variables � partir du SIF */
	mcdvo=substr(sif,5,1);
	xyz='0'; 
	if substr(sif,35,1)='X' then xyz='X';/* on consid�re que X prime pour les doubles d�clarations */
	else if substr(sif,44,1)='Y' then xyz='Y';
	else if substr(sif,53,1)='Z' then xyz='Z';
	vousconj=substr(sif,6,4)!!"-"!!substr(sif,11,4);

	label 	case_e='E' case_f='F' case_g='G' case_k='K' case_l='L' case_p='P' 
			case_s='S' case_w='W' case_n='N' case_t='T' case_l2='L2';

	case_e=substr(sif,16,1);
	case_f=substr(sif,17,1);
	case_g=substr(sif,18,1);
	case_k=substr(sif,19,1);
	case_l=substr(sif,20,1);
	case_p=substr(sif,21,1);
	case_s=substr(sif,22,1);
	case_w=substr(sif,23,1);
	case_n=substr(sif,24,1);
	case_t=substr(sif,30,1);
	case_l2=substr(sif,25,1);
	array case case_e case_f case_g case_k case_l case_p case_s case_w case_n case_t case_l2;
	do over case; if case='' then case='0'; end;

	/*Parfois il existe un ' F0' en d�but de sif (1 cas en 2011)mais ce n'est celui que l'on cherche ie ce n'est pas 
	  celui	� partir duquel on extrait la variable nbenf. on sait que le ' F0' que l'on cherche est dans une position au 
	  del� de 20 au moins donc on uitilise un find plut�t qu'un index*/
	debut_F=find(sif,' F0',20);
	if debut_F=0 then debut_F=find(sif,' F1',20);
	nbenf=substr(sif,debut_F+1,24);
	nbf=input(substr(nbenf,2,2),2.);
	nbg=input(substr(nbenf,5,2),2.);
	nbr=input(substr(nbenf,8,2),2.);
	nbj=input(substr(nbenf,11,2),2.);
	nbn=input(substr(nbenf,14,2),2.);
	nbh=input(substr(nbenf,17,2),2.);
	nbi=input(substr(nbenf,20,2),2.);
		  
	anaisd=substr(vousconj,1,4);  
	anaisc=substr(vousconj,6,4);  
	anaih=substr(sif,26,4); 
	if anaih ne '0000' then ageh=&anref.-input(anaih,4.); else ageh=99; 
	if anaisd not in ('9999','9998') then aged=&anref.-input(anaisd,4.);else aged=0;
	if mcdvo in ('M','O') & anaisc not in ('9999','9998') then agec=&anref.-input(anaisc,4.); else agec=0;
	/* Dans le cas d'un couple mari� ou pacs� mais dont on n'a qu'un des deux membres dans le m�nage, 
	l'ann�e de naissance du FIP est � 9998 (cf pgm 5a_ee_foyer)*/

	if xyz='X' then do;/*normalement le mois du mariage est en %eval(35+3) mais incoherence 
	possible sif-dec donc on cherche ailleurs*/
		moisev=substr(sif,%eval(35+3),2);
		jourev=substr(sif,%eval(35+1),2);
		if moisev='00' then do;
			moisev=substr(sif,%eval(44+3),2);
			jourev=substr(sif,%eval(44+1),2);
			end;
		if moisev='00' then do;
			moisev=substr(sif,%eval(53+3),2);
			jourev=substr(sif,%eval(53+1),2);
			end;
		end;
	if xyz='Y' then do;
		moisev=substr(sif,%eval(44+3),2);
		jourev=substr(sif,%eval(44+1),2);
		if moisev='00' then do;
			moisev=substr(sif,%eval(35+3),2);
			jourev=substr(sif,%eval(35+1),2);
			end;
		if moisev='00' then do;
			moisev=substr(sif,%eval(53+3),2);
			jourev=substr(sif,%eval(53+1),2);
			end;
		end;
	if xyz='Z' then do;
		moisev=substr(sif,%eval(53+3),2);
		jourev=substr(sif,%eval(53+1),2);
		if moisev='00' then do;
			moisev=substr(sif,%eval(44+3),2);
			jourev=substr(sif,%eval(44+1),2);
			end;
		if moisev='00' then do;
			moisev=substr(sif,%eval(35+3),2);
			jourev=substr(sif,%eval(35+1),2);
			end;
		end;
	if xyz='0' then do;moisev='00';jourev='00';end;
	%mend; 

/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
