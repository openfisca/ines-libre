/********************************************************************************************/
/*																							*/
/*										6_cal_typcho										*/
/*								 															*/
/********************************************************************************************/

/********************************************************************************************/
/* Constitution d'un r�capitualif individuel trimestriel des r�ponses � l'enqu�te emploi 	*/
/* En entr�e : 	emploi																		*/ 
/* En sortie : 	travail.cal_typcho															*/
/********************************************************************************************/

%macro typ_chomage;
	data travail.cal_typcho(keep=ident noi cal_alcho); 
		set emploi (keep=ident&anr. noi datqi: alct: alcnc: rename=(ident&anr.=ident)); 

		/* Initialisation des calendriers cal_alct, cal_alcnc et cal_alcho*/
		format cal_alct cal_alcnc cal_alcho $40.;
			cal_alct=	'0000000000000000000000000000000000000000';
			cal_alcnc=	'0000000000000000000000000000000000000000';
			cal_alcho=	'0000000000000000000000000000000000000000';

		/* On it�re sur chaque trimestre d'enqu�te */ 
		%let l=1;
		%do %while(%scan(&liste_trim.,&l.)> );
			%let trim=%scan(&liste_trim.,&l.);
		    %let l=%eval(&l.+1);

			if datqi_&trim. ne '' then do;
				/* on r�cup�re l'ann�e et le mois de chaque interrogation */
				ancoll=		input(substr(datqi_&trim.,1,4),4.);
				moiscoll=	input(substr(datqi_&trim.,5,2),4.);
				place=12*(%eval(&anref.+1)-ancoll)+12-moiscoll+1;

				if alct_&trim.='' then alct_&trim.='0';
				if alcnc_&trim='' then alcnc_&trim.='0';
				%calend(cal_alct,alct_&trim.,place,1);
				%calend(cal_alcnc,alcnc_&trim.,place,1);
				end;
			%end;

			if 	cal_alct ne '0000000000000000000000000000000000000000' or cal_alcnc ne '0000000000000000000000000000000000000000';
			%macro ass; 
				%do i=1 %to 40; 
				if substr(cal_alct,&i.,1)='2' then do;
					substr(cal_alct,&i.,1)='0';
					substr(cal_alcnc,&i.,1)='5'; 
					end;
				if substr(cal_alct,&i.,1) ne '0' then substr(cal_alcho,&i.,1)='1';
				if substr(cal_alcnc,&i.,1) ne '0' then substr(cal_alcho,&i.,1)='2';
				%end; 
				%mend; 
			%ass; 
		run;
	%mend typ_chomage;
%typ_chomage;

/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
