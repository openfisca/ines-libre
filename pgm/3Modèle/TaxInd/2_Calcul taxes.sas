/********************************************************************************************/
/*																							*/
/*	                          CALCUL DES TAXES INDIRECTES DANS INES		                    */
/*																							*/
/********************************************************************************************/

/* Table d'entr�e : 
				- work.basemen_BDF 
				- taxind.prix_moyen_&anBDF. 
				- taxind.prix_&anBDF._vin
				- taxind.prix_&anBDF._biere  
*/

/* Fichiers param�tre : 
		- prix.xls (prix moyens et taux d'inflation)
		- Taux_TVA_nomen5.xls (taux de taxe applicables)                                         */
		                                              
/* Table de sortie : 
		- taxind.basemen_taxes
         */
 
/* Le programme comporte 2 �tapes :		
/* I  - Importation des prix et des taux									                 */
/* II - Calcul des quantit�s et des taxes (TVA, TCA, droits sur le tabac,       */
/*                                     les alcools et TICPE)     */

/*********************************************************************************************/


/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
/*                                                                                      																												     */
/*                      I - Importation des prix et des taux de taxes et cr�ation des macrovariables                      */
/*                                                                                      																							  			              */
/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/

/***** Transformation des prix issus des carnets BDF 2011 en macro-variables *****/

/*on cr�� la liste de MV � partir de la table et des listes de liquides*/
%Cree_Liste_MV(taxind.prix_moyen_&anBDF. , &liste_liquides.,  prix_&anBDF., nomen6, &liste_liquides_nomen., px_li_);

data _null_ ;
	set taxind.prix_&anBDF._vin  ;
	decile = 'v'!!dec ;
	call symputx(decile, px_li_vin, 'G');
run ;

data _null_ ;
	set taxind.prix_&anBDF._biere  ;
	decile = 'b'!!dec ;
	call symputx(decile, px_li_biere, 'G');
run ;

/***** Importation des prix issus de sources externes et transformation en macro-variables *****/
PROC IMPORT OUT=px_moyen_ext DATAFILE="&dossier.\prix.xls" replace dbms=&excel_imp. ; sheet = "PRIX_MOYEN_EXTERNE" ;RUN;
data _null_  ;
	set px_moyen_ext  ;
	call symputx(Produits, Prix_moyen_20&anr2., 'G');
run ;

/***** Importation des taux d'inflation et transformation en macro-variables *****/
PROC IMPORT OUT=taux_inflation DATAFILE="&dossier.\prix.xls" replace dbms=&excel_imp. ; sheet = "TAUX_INFLATION_ANNUEL" ; RUN;
data _null_  ;
	set taux_inflation ;
	call symputx(produit, infl_20&anr2., 'G');
run ;

/***** Importation des taux de TVA *****/
PROC IMPORT OUT=taux_tva DATAFILE="&dossier.\Taux_TVA_nomen5.xls" replace dbms=&excel_imp.; sheet = "Taux TVA" ; RUN;

%MACRO taux_nomen(Liste, table, colonne, taxe);
	%do i=1 %to %sysfunc(countw(&Liste.));
		%let n5_temp= %scan(&Liste.,&i.); /*nomen5*/
		%let nomen_temp = %substr(&n5_temp.,2) ; /*on enl�ve le C en d�but */
		%CreeMacrovarAPartirTable(taux_&taxe._&n5_temp., &table.,&colonne. , nomen, &nomen_temp.);
	%end;
%MEND taux_nomen;
%taux_nomen(&liste_conso., taux_tva, tva_20&anr2.,tva);

/* Importation des taux de TCA */
PROC IMPORT OUT=taux_assu (keep = nomen taxes_assu_20&anr2. I_assu_20&anr2.) 
DATAFILE="&dossier.\Taux_TVA_nomen5.xls" replace dbms=&excel_imp.; sheet = "Taux assurances" ; RUN;
%let liste_assu = C12511 C12521 C12531 C12541 C125511 C125512 C125513 C125514 C125515 C125516 C125517 C125518 C125519 C12551A C12551B C12551C;
%taux_nomen(&liste_assu., taux_assu, taxes_assu_20&anr2., assu);

/***** Importation des taux de TICPE *****/
PROC IMPORT OUT=taux_ticpe  (keep = nomen ticpe_20&anr2.)
DATAFILE="&dossier.\Taux_TVA_nomen5.xls" replace dbms=&excel_imp.; sheet = "TICPE" ; RUN;
%let liste_ticpe = C04531 C072211 C072212;
%taux_nomen(&liste_ticpe., taux_ticpe, ticpe_20&anr2.,ticpe);

/* Importation des taux d'accises sur les tabacs */
PROC IMPORT OUT=taux_tabac (keep = nomen Tx_tabac_20&anr2. Tx_min_tabac_20&anr2.) 
DATAFILE="&dossier.\Taux_TVA_nomen5.xls" replace dbms=&excel_imp.; sheet = "Acc_tabac" ; RUN;
%taux_nomen(C02211 C02212 C022131, taux_tabac, Tx_tabac_20&anr2.,tabac);
data _null_  ;
	set taux_tabac ;
	name_mv = 'Tx_min_tabac_C'!!nomen ;
	call symputx(name_mv, Tx_min_tabac_20&anr2., 'G');
run ;

/***** Importation des taux d'accises sur les alcools *****/
PROC IMPORT OUT=taux_alcool (keep = nomen Accises_alcool_20&anr2. Cotisation_20&anr2. Premix_20&anr2.) 
DATAFILE="&dossier.\Taux_TVA_nomen5.xls" replace dbms=&excel_imp.; sheet = "Acc_alcool" ; RUN;
%let liste_alco = C021111 C021112 C021113 C021114 C021115 C021116 C021117 C021118 C021211 C021212 C021221 C021222 C021223 C02131;
%taux_nomen(&liste_alco., taux_alcool, Accises_alcool_20&anr2., alcool);
data _null_  ;
	set taux_alcool ;
	name_mv1 = 'Cotisation_C'!!nomen ;
	name_mv2 = 'Premix_C'!!nomen ;
	call symputx(name_mv1, Cotisation_20&anr2., 'G');
	call symputx(name_mv2, Premix_20&anr2., 'G');
run ;

/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
/*                                                                                           */
/*             II - Calcul des quantit�s et des taxes (TVA, TCA, droits sur le tabac,       */
/*                                     les alcools et TICPE)                                 */
/*                                                                                           */
/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/

/***** Calcul des quantit�s et des montants de taxes *****/
data basemen_taxes  ;
	set basemen_BDF (drop = part:  C01--C12);
	
	/***** Calcul des quantit�s pour les produits sur lesquels il y a des accises *****/
	/*tabac*/
	q_C02211 = C02211 / &px_paq_cigtte. ; /* prix externe donc pas besoin de taux d'inflation (au moins pour 2012) */
	q_C02212 = C02212 / &px_cigare. ; /* prix externe donc pas besoin de taux d'inflation (au moins pour 2012) */
	q_C022131 = C022131 / &px_kilo_tabac. ; /* prix externe donc pas besoin de taux d'inflation (au moins pour 2012) */
	/* carburants */
	q_C04531 = C04531 / &px_litre_fioul. ; 
	q_C072211 = C072211 / (&px_li_essence.*&energie.) ; /* prise en compte de l'inflation */
	q_C072212  = C072212 / (&px_li_diesel.*&energie.)  ; /* prise en compte de l'inflation */
	/*TODO : il y a px_li_essence et px_li_diesel � partir de prix_moyen_BDF211 et px_litre_diesel � partir de prix_moyen_externe*/

	/* alcool */
	q_C021111 = C021111 / (&px_li_anisette.*&tabac_alcool.) ; /* prix issu de la table carnet 2011 donc on applique l'inflation (taux fix� � 0 pr 2011) */
	q_C021112 = C021112 / (&px_li_apero.*&tabac_alcool.) ; 
	q_C021113 = C021113 / (&px_li_whisky.*&tabac_alcool.) ; 
	q_C021114 = C021114 / (&px_li_eaudevie.*&tabac_alcool.) ;
	q_C021115 = C021115 / (&px_li_rhum.*&tabac_alcool.) ;
	q_C021116 = C021116 / (&px_li_punch.*&tabac_alcool.) ; 
	q_C021117 = C021117 / (&px_li_liqueur.*&tabac_alcool.) ;
	q_C021118 = C021118 / (&px_li_cocktail.*&tabac_alcool.) ;
	q_C021212 = C021212 / (&px_li_cidre.*&tabac_alcool.) ;
	q_C021221 = C021221 / (&px_li_mousseux.*&tabac_alcool.) ;
	q_C021222 = C021222 / (&px_li_champ.*&tabac_alcool.) ; 
	q_C021223 = C021223 /(&px_li_vindoux.*&tabac_alcool.) ;

	/*vin et bi�re : quantit� par d�ciles */
	%MACRO quantite_dec;
		%do i=1 %to 9;
			if decile =  %str(0&i.) then do ;
				q_C021211 = C021211 / (&&v0&i..*&tabac_alcool.) ;	
				q_C02131 = C02131 / (&&b0&i..*&tabac_alcool.) ;
			end;
		%end;
	%mend;
	%quantite_dec;
	if decile = '10' then do;
		q_C021211 = C021211 / (&v10.*&tabac_alcool.) ;	
		q_C02131 = C02131 / (&b10.*&tabac_alcool.) ;
	end;
run;

proc sort data = basemen_taxes; by ident; run;
data basemen_taxes (keep = ident poi revdisp NdV conso_tot decile typmen7 occlog strate montant: )  ;
	set basemen_taxes;
	by ident;
	if first.ident then do ;
		montant_tva=0; montant_assu=0; montant_ticpe=0; montant_tabac=0; montant_alcool=0;
	end;
	
	/*macro pour calculer les montants de tva et tca*/
	%MACRO montant_taxe(Liste, taxe);
		%do i=1 %to %sysfunc(countw(&Liste.));
			%let n5_temp= %scan(&Liste.,&i.); /*nomen5*/
			montant_&taxe.= montant_&taxe.+ &n5_temp.*&&taux_&taxe._&n5_temp../(1+&&taux_&taxe._&n5_temp..) ;
		%end;
	%MEND montant_taxe;
	/***** Calcul des montants de TVA pour chaque m�nage *****/ 
	%montant_taxe(&liste_conso.,tva) ;
	label montant_tva = "Montant annuel de TVA acquitt�" ;
	/***** Calcul des montants de taxes sur les assurances *****/ 
	%montant_taxe(&liste_assu.,assu) ;
	label montant_assu = "Montant annuel de cotisation sp�ciale sur les conventions d'assurance acquitt�" ; 
	/***** Calcul des montants de TICPE *****/ 
	%MACRO montant_ticpe(Liste, taxe);
		%do i=1 %to %sysfunc(countw(&Liste.));
			%let n5_temp= %scan(&Liste.,&i.); /*nomen5*/
			montant_&taxe.= montant_&taxe.+ q_&n5_temp.*&&taux_&taxe._&n5_temp../100 ; /*montants en hecto-litres*/
		%end;
	%MEND montant_ticpe;
	%montant_ticpe(&liste_ticpe., ticpe);
	label montant_ticpe = "Montant annuel de taxe int�rieure sur les produits �nerg�tiques acquitt�" ;
	/***** Calcul des montants droits sur le tabac *****/ 
	mtab1= max(&Tx_min_tabac_C02211.*q_C02211*(20/1000),C02211*&taux_tabac_C02211./(1+&taux_tabac_C02211.)); /* cigarette */
	mtab2= max(&Tx_min_tabac_C02212.*q_C02212/1000,C02212*&taux_tabac_C02212./(1+&taux_tabac_C02212.)); /* cigares et cigarillos */
	mtab3= max(&Tx_min_tabac_C022131.*q_C022131,C022131*&taux_tabac_C022131./(1+&taux_tabac_C022131.)); /* Tabac */
	montant_tabac = mtab1+mtab2+mtab3 ;
	label montant_tabac = "Montant annuel de droits sur le tabac (cigarettes, cigares et cigarillos et tabac) acquitt�" ;
	drop mtab1-mtab3;
	/***** Calcul des montants de droits sur les alcools *****/ 
	%MACRO montant_alcool1(Liste);
		%do i=1 %to %sysfunc(countw(&Liste.));
			%let n5_temp= %scan(&Liste.,&i.); /*nomen5*/
			montant_alcool = montant_alcool + (&&taux_alcool_&n5_temp..*q_&n5_temp.*0.01*0.40)+(&&Cotisation_&n5_temp.*q_&n5_temp.*0.01); 
			/* on suppose le degr� d'alcool � 40� pour 2011: cotisations sociales � part (0 apr�s 2011) */
		%end;
	%MEND montant_alcool1;
	%let list_alc1 = C021111 C021112 C021113 C021114 C021115;
	%montant_alcool1(&list_alc1.);
	%MACRO montant_alcool2(Liste);
		%do i=1 %to %sysfunc(countw(&Liste.));
			%let n5_temp= %scan(&Liste.,&i.); /*nomen5*/
			montant_alcool = montant_alcool + (&&taux_alcool_&n5_temp..*q_&n5_temp.*0.01); 
			/* on suppose le degr� d'alcool � 40� pour 2011: cotisations sociales � part (0 apr�s 2011) */
		%end;
	%MEND montant_alcool2;
	%let list_alc2 = C021116 C021117 C021211 C021212 C021221 C021222 C021223;
	%montant_alcool2(&list_alc2.);
	/* taxe premix : assiette = d�cilitre d'alcool pur, en supposant une boisson � 15� */
	montant_alcool = montant_alcool +((&Premix_C021118.*q_C021118*10*0.15)+(&taux_alcool_C021118.*q_C021118*0.01)) ;
	/* on suppose le degr� d'alcool � 6� et seulement des bi�res industrielles */
	montant_alcool = montant_alcool +(&taux_alcool_C02131.*q_C02131*0.01*6) ;
	label montant_alcool = "Montant annuel des droits sur les alcools acquitt�" ;
run ;

/*copie de la table dans la librairie taxind*/
data taxind.basemen_taxes ;	set basemen_taxes; run;


/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
