/****************************************************************************************/
/*																						*/
/*									1_RBG												*/
/*																						*/
/****************************************************************************************/
/* Calcul du revenu brut global															*/
/* En entr�e : 	base.foyer&anr1.															*/
/* En sortie : 	modele.rbg&anr1.														*/
/****************************************************************************************/
/* PLAN 																				*/
/*	Macro appel�e dans ce programme														*/
/*	A - traitements salaires et pensions 												*/
/*		1 	salaires + salaires d'associ�s + ....+ indemnit�s journali�res				*/
/*		2 	frais professionnels														*/
/*		3 	pensions retraites et rentes � titre gratuit								*/
/*		4 	rentes viag�res � titre on�reux												*/
/*	B - revenus des valeurs et capitaux mobiliers										*/
/*		1	Produits des contrats d'assurance-vie d'au moins 6 ou 8 ans					*/
/*		2	Revenus ouvrant droit � l'abattement de 40 %								*/
/*		3	Revenus bruts des capitaux mobiliers n'ouvrant pas droit � abattement		*/
/*		4	D�ficit RCM ant�rieur � d�duire												*/
/*	C - Plus-values																		*/
/*	D - revenus fonciers																*/
/*	E - revenus des professions non salari�es											*/
/*		1	Benefices et plus values de court terme										*/
/*		2	Revenus industriels et commerciaux professionnel							*/
/*		3	Revenus industriels et commerciaux professionnels							*/
/*		4	Revenus industriels et commerciaux non professionnels						*/
/*		5	Revenus non commerciaux professionnels										*/
/*		6	Revenus non commerciaux non professionnels									*/
/*		7	Plus values des r�gimes micros												*/
/*	F - D�finition de RBG																*/
/*	G - Agr�gats et variables utiles pour la suite										*/
/*	H - D�finition d'un RBG individuel													*/	
/****************************************************************************************/
/* REMARQUE : 																			*/
/* Ce programme suit la section "calcul de l'imp�t", en particulier la fiche facultative*/ 
/* de calculs, de la Brochure Pratique de la d�claration des revenus.					*/						
/* Article 13 du CGI : Le revenu global net annuel servant de base � l'imp�t sur le 	*/
/* revenu est d�termin� en totalisant les b�n�fices ou revenus nets vis�s aux I � VII bis*/ 
/* de la 1�re sous-section de la pr�sente section, compte tenu, le cas �ch�ant, du		*/ 
/* montant des d�ficits vis�s aux I et I bis de l'article 156, des charges �num�r�es au */
/* II dudit article et de l'abattement pr�vu � l'article 157 bis						*/
/****************************************************************************************/

/*****************/
/*	   MACRO 	 */
/*****************/
%Macro DefinitCasesDeclarant(declarant);
	/*  PROBLEME : 	le code du programme RBG est vite complexe car il faut r�p�ter les m�mes op�rations pour 
					le vous, le conj et les pac, avec des cases fiscales qui changent de nom selon le d�clarant
		IDEE : 	n'�crire les calculs qu'une seule fois pour r�duire les risques d'erreur 
				et s'assurer de faire le m�me calcul pour chacun des d�clarants

		* PREMIER NIVEAU DE COMPREHENSION *
		PRINCIPE : 	si une case s'appelle A pour le vous (=d), B pour le conj (=c) et C pour le pac1 (=p1), on cr�e 
					une variable qui s'appelle A_ (nom de la case du vous suivi d'un '_'), et qui vaut A, B ou C
					selon le cas (d�clarant pass� en param�tre de la macro). 
					Ensuite les calculs de revenus sont fait � partir de A_ pour simplifier. 

		EXEMPLE 1 (simple) : cas d'un calcul o� on n'a besoin que de la case de l'individu
			si d�clarant=vous(d) alors _5hn_=_5hn, si d�clarant=conjoint(c) alors _5hn_=_5in

		TODO : 	- ce premier niveau de compr�hension n'est pas encore g�n�ralis� � l'ensemble du programme (seulement pour les revenus des professions non salari�es). 
				- � terme, voir si l'on ne peut pas se servir de la table dossier.FoyerVarList

		* DEUXIEME NIVEAU DE COMPREHENSION *
		pour certains calculs on a aussi besoin du revenu du conj ... cela complexifie donc le principe �nonc� ci-dessus
		-> On d�finit un ordre entre les personnes : d, puis c, puis p1. 
			La personne � qui l'on s'int�resse est le n�1, les deux autres sont ordonn�s selon cet ordre. 
			Donc si la case s'appelle A pour d, B pour c, et C pour p1, et que declarant=c, 
			alors on va mettre 	A_ (d=n�1 pour d fix�)=B (le conj est le n�1 puisque declarant=c)
								B_ (c=n�2 pour d fix�)=A (le vous est le n�2 puisque declarant=c)
								C_ (p1=n�3 pour d fix�)=C (le pac1 est le n�3 puisque declarant=c)
	
		EXEMPLE 2 (complexe) : dans le calcul individuel on a aussi besoin des cases des autres personnes du foyer
			exemple pvind plus bas (en fait le seul exemple)

		TODO : ce principe est beaucoup trop complexe et il faut le simplifier / l'abandonner (Ma�lle)
	*/

	%if &declarant. eq d %then %do;
		_5hn_=_5hn;	_5ho_=_5ho;	_5hb_=_5hb;	_5hd_=_5hd;	_5hf_=_5hf;	_5hh_=_5hh;	_5hi_=_5hi;	_5hl_=_5hl;	_5hx_=_5hx;	_5he_=_5he;
		_5hc_=_5hc;
		_5ta_=_5ta;	_5tb_=_5tb;	_5kn_=_5kn; _5kb_=_5kb; _5kh_=_5kh; _5ki_=_5ki;
		_5ka_=_5ka; _5kl_=_5kl; _5km_=_5km; _5qj_=_5qj; _5kq_=_5kq; _5ke_=_5ke; _5kr_=_5kr;
		_5kc_=_5kc; _5kf_=_5kf; _5ha_=_5ha; _5qa_=_5qa;
		_5ni_=_5ni; _5nk_=_5nk; _5nl_=_5nl; _5nm_=_5nm; _5nz_=_5nz; _5nq_=_5nq; _5ne_=_5ne; _5nr_=_5nr;
		_5nc_=_5nc; _5nd_=_5nd; _5na_=_5na; _5nf_=_5nf; _5ng_=_5ng; _5nj_=_5nj; _5ny_=_5ny;
		_5sn_=_5sn; _5sp_=_5sp; _5kv_=_5kv; _5kw_=_5kw; _5so_=_5so; _5tc_=_5tc;
		_5jg_=_5jg; _5jj_=_5jj;
		_5te_=_5te; _5hp_=_5hp; _5qb_=_5qb; _5qh_=_5qh; _5qi_=_5qi; _5qk_=_5qk;
		_5qc_=_5qc; _5qe_=_5qe; _5qd_=_5qd;
		_5ko_=_5ko; _5kp_=_5kp;
		_5no_=_5no; _5np_=_5np; 
		_5hq_=_5hq; _5te_=_5te;
		_5ku_=_5ku;
		_5hw_=_5hw; _5kx_=_5kx; _5hv_=_5hv; _5kj_=_5kj; _5lx_=_5lx; _5iv_=_5iv; _5iw_=_5iw; _5jv_=_5jv; _5mx_=_5mx; _5jw_=_5jw; _5kz_=_5kz;  
		_hsupVous_=_hsupVous;
		%end;
	%else %if &declarant. eq c %then %do;
		_5hn_=_5in;	_5ho_=_5io;	_5hb_=_5ib;	_5hd_=_5id;	_5hf_=_5if;	_5hh_=_5ih;	_5hi_=_5ii;	_5hl_=_5il;	_5hx_=_5ix;	_5he_=_5ie;
		_5hc_=_5ic;
		_5ta_=_5ua;	_5tb_=_5ub;	_5kn_=_5ln; _5kb_=_5lb; _5kh_=_5lh; _5ki_=_5li;
		_5ka_=_5la; _5kl_=_5ll; _5km_=_5lm; _5qj_=_5rj; _5kq_=_5lq; _5ke_=_5le; _5kr_=_5lr;
		_5kc_=_5lc; _5kf_=_5lf; _5ha_=_5ia; _5qa_=_5ra;
		_5ni_=_5oi; _5nk_=_5ok; _5nl_=_5ol; _5nm_=_5om; _5nz_=_5oz; _5nq_=_5oq; _5ne_=_5oe; _5nr_=_5or;
		_5nc_=_5oc; _5nd_=_5od; _5na_=_5oa; _5nm_=_5om; _5nf_=_5of; _5ng_=_5og; _5nj_=_5oj; _5ny_=_5oy;
		_5sn_=_5ns; _5sp_=_5nu; _5kv_=_5lv; _5kw_=_5lw; _5so_=_5nt; _5tc_=_5uc;
		_5jg_=_5rf; _5jj_=_5rg;
		_5te_=_5ue; _5hp_=_5ip; _5qb_=_5rb; _5qh_=_5rh; _5qi_=_5ri; _5qk_=_5rk;
		_5qc_=_5rc; _5qe_=_5re; _5qd_=_5rd;
		_5ko_=_5lo; _5kp_=_5lp;
		_5no_=_5oo; _5np_=_5op; 
		_5hq_=_5iq; _5te_=_5ue;
		_5ku_=_5lu;
		_5hw_=_5iw; _5kx_=_5lx; _5hv_=_5iv; _5kj_=_5lj; _5lx_=_5kx; _5iv_=_5hv; _5iw_=_5hw; _5jv_=_5jv; _5mx_=_5mx; _5jw_=_5jw; _5kz_=_5lz; 
		_hsupVous_=_hsupConj;
		%end;
	%else %if &declarant. eq p1 %then %do;
		_5hn_=_5jn; _5ho_=_5jo;	_5hb_=_5jb;	_5hd_=_5jd;	_5hf_=_5jf;	_5hh_=_5jh;	_5hi_=_5ji;	_5hl_=_5jl;	_5hx_=_5jx;	_5he_=_5je;
		_5hc_=_5jc;
		_5ta_=_5va;	_5tb_=_5vb;	_5kn_=_5mn; _5kb_=_5mb; _5kh_=_5mh; _5ki_=_5mi;
		_5ka_=_5ma; _5kl_=_5ml; _5km_=_5mm; _5qj_=_5sj; _5kq_=_5mq; _5ke_=_5me; _5kr_=_5mr;
		_5kc_=_5mc; _5kf_=_5mf; _5ha_=_5ja; _5qa_=_5sa;
		_5ni_=_5pi; _5nk_=_5pk; _5nl_=_5pl; _5nm_=_5pm; _5nz_=_5pz; _5nq_=_5pq; _5ne_=_5pe; _5nr_=_5pr;
		_5nc_=_5pc; _5nd_=_5pd; _5na_=_5pa; _5nm_=_5pm; _5nf_=_5pf; _5ng_=_5pg; _5nj_=_5pj; _5ny_=_5py;
		_5sn_=_5os; _5sp_=_5ou; _5kv_=_5mv; _5kw_=_5mw; _5so_=_5ot; _5tc_=_5vc;
		_5jg_=_5sf; _5jj_=_5sg;
		_5te_=_5ve; _5hp_=_5jp; _5qb_=_5sb; _5qh_=_5sh; _5qi_=_5si; _5qk_=_5sk;
		_5qc_=_5sc; _5qe_=_5se; _5qd_=_5sd;
		_5ko_=_5mo; _5kp_=_5mp;
		_5no_=_5po; _5np_=_5pp;
		_5hq_=_5jq; _5te_=_5ve;
		_5ku_=_5mu;
		_5hw_=_5jw; _5kx_=_5mx; _5hv_=_5jv; _5kj_=_5mj; _5lx_=_5kx; _5iv_=_5hv; _5iw_=_5hw; _5jv_=_5iv; _5mx_=_5lx; _5jw_=_5iw; _5kz_=_5mz; 
		_hsupVous_=_hsupPac1;
	%end;
	%Mend DefinitCasesDeclarant;

%Macro RBG;

	data modele.rbg&anr1.	(keep=	declar rbg: rev: assuvie rev2abat rev2ssabat deficit2
									_6: _2bh _2dh _2ch _2fa nb: sif age: case_t case_k case_l 
									mcdvo _2dc _2fu FraisDeductibles_Restants _7uh _perte_capital _perte_capital_passe 
									quotient: rev2: _3va _3vb _3vz _3wb _5ql _5rl _5sl _5sv _5sw _5sx
									_hsupVous _hsupConj _hsupPac1 _hsupPac2 EXO _8ti _1ac _1bc _1cc _1dc _1ad _1bd _1cd _1dd _1ah _1bh _1ch _1dh
									_8by _8th _8cy _2ee _2tr PVmicro
									PVT _PVCessionDom _3vm _3vt _PVCession_entrepreneur _3vg _3vh _3sg _3sh _3sl _3sm _3vi _3vf _3vj _3vk 
									chiffaff _interet_pret_conso _vehicule_propre_simple _vehicule_propre_avec_destr 
									_relocalisation ident _8tk _4by _4bh _4bc
									anaisenf rev_cat:
									_7db _7df _7g: _souscSofipeche _cred_loc _7fn _1ao _1bo _1co _1do _1eo _1as _1bs _1cs _1ds _1es
									_1az _1bz _1cz _1dz	_3se _3wa _3vd _3sb _3sc _3sd _3sf _3si
									_1at _1bt AE: RevCessValMob_HorsAbatt RevCessValMob_Abatt _3sj _3sk _8tm
									_1ere_annuite_lgt_ancien _1ere_annuite_lgt_neuf _bouquet_travaux _cred_loc _protect_patnat
									_epargneCodev _credFormation
									_demenagementemploivous _demenagementemploiconj _demenagementemploipac1 _demenagementemploipac2
									_revPEA _CIFormationSalaries);
		set base.foyer&anr1.;

		%Init_Valeur(rev1 rev2 rev3 rev4 rev5);

		/*************************************************************************/
		/* A - Traitements, salaires, pensions (Cadre 1 D�claration de revenus)  */
		/*************************************************************************/

		/***********************************************************************/
		/* A1	Salaires + salaires d'associ�s + ....+ indemnit�s journali�res */
		/* Salaires, traitements, gains de lev�e d'options dans cat�gories des salaires ... */
		%let sal1=	_1aj _1ap _3vj _1tv _1tw _1tx _glovSup4ansVous;
		%let sal2=	_1bj _1bp _3vk _1uv _1uw _1ux _glovSup4ansConj;
		%let sal3=	_1cj _1cp;
		%let sal4=	_1dj _1dp;
		%let sal5=	_1ej _1ep;
		/* Pensions, retraites et rentes et pensions alimentaires re�ues  */ 
		%let prb1=	_1as _1az _1ao;
		%let prb2=	_1bs _1bz _1bo;
		%let prb3=	_1cs _1cz _1co;
		%let prb4=	_1ds _1dz _1do;
		%let prb5=	_1es  _1eo;
		/* Frais r�els */
		%let FraisReels=_1ak _1bk _1ck _1dk _1ek;
		/* Etre demandeur d'emploi depuis plus d'un an (cases qui se cochent) */ 
		%let Chomeur=	_1ai _1bi _1ci _1di _1ei;

		%do i=1 %to 5;
			sal&i.=sum(of &&sal&i.);
			FraisReels&i.=%scan(&FraisReels.,&i.);
			Chomeur&i.=%scan(&Chomeur.,&i.)>0;
			PRB&i.=sum(of &&prb&i.);
			%end;

		/* D�fiscalisation compl�te des heures sup r�alis�es entre octobre 2007 (anleg=2008) et ao�t 2012 (anleg=2013) */
		%if &anleg.=2008 %then %do;
			sal1=sal1-_hsupVous*3/12;
			sal2=sal2-_hsupConj*3/12;
			sal3=sal3-_hsupPac1*3/12;
			sal4=sal4-_hsupPac2*3/12;
			%end;
		%else %if &anleg.>2008 and &anleg.<2013 %then %do;
			sal1=sal1-_hsupVous;
			sal2=sal2-_hsupConj;
			sal3=sal3-_hsupPac1;
			sal4=sal4-_hsupPac2;
			%end;
		%else %if &anleg.=2013 %then %do;
			sal1=sal1-_hsupVous*7/12;
			sal2=sal2-_hsupConj*7/12;
			sal3=sal3-_hsupPac1*7/12;
			sal4=sal4-_hsupPac2*7/12;
			%end;
		
		/* Fiscalisation de la participation employeur au contrat collectif � partir de 2014 (sur revenus 2013) */
	   	 %if &anleg.<2014 %then %do;
			 sal1=sal1-part_employ_vous;
			 sal2=sal2-part_employ_conj;
			 sal3=sal3-part_employ_pac1;
			 sal4=sal4-part_employ_pac2;
			 sal5=sal5-part_employ_pac3;

			 _1aj=_1aj-part_employ_vous;
			 _1bj=_1bj-part_employ_conj;
			 _1cj=_1cj-part_employ_pac1;
			 _1dj=_1dj-part_employ_pac2;
			 _1ej=_1ej-part_employ_pac3;
			 %end;

		/*********************************************************************/
		/* A2	D�duction de 10 % de frais professionnels ou des frais r�els */
		%do i=1 %to 5; /* On cr�e deduc1 � deduc5 */
			/* Abattement de 10 % avec un minimum */
			minimum=&abat10_min.;
			%if &anleg.>1998 %then %do;
				if chomeur&i. ne 0 then minimum=&abat10_min_deld.;
				%end;
			deduc&i.=max(&abat10.*sal&i.,minimum); drop minimum;
			/* Plafonnement de l'abattement */
			deduc&i.=min(deduc&i.,&abat10_plaf.);
			/* Possibilit� de choix de d�duire les frais r�els � la place */
			deduc&i.=max(deduc&i.,fraisreels&i.);
			/* On ne peut pas avoir plus de d�duc que de salaire */
			deduc&i.=min(deduc&i.,sal&i.);
			%end;
		SAL=sum(of sal1-sal5)-sum(of deduc1-deduc5);

		/*****************************************************/
		/* A3	Pensions retraites et rentes � titre gratuit */

		/* Imposition des majorations de pension de retraite � partir de 2014
		Note : la variable majo_vous (& majo_conj) n'est cr��e que pour les ann�es o� la majoration 
		n'est pas dans la d�claration fiscale, c'est � dire avant 2013 */
		%if &anleg.<2014 %then %do;
			_1as=_1as-majo_vous;
			_1bs=_1bs-majo_conj;
			_1cs=_1cs-majo_pac1;

			prb1=prb1-majo_vous;
			prb2=prb2-majo_conj;
			prb3=prb3-majo_pac1;
			%end;

		/* D�duction de 10 % avec un minimum calcul� pour chaque individu */
		/* On cr�e ABATT et PRB */
		%do i=1 %to 5;
			/* Abattement de 10 % avec un minimum */
			abatt&i.=min(max(&abat10.*PRB&i.,&min_abatt_prb_indiv.),PRB&i.);
			%end;
		/* l'abattement maximal se calcule sur l'ensemble et non pas par d�clarant */
		Abatt=min(sum(of abatt1-abatt5),&max_abatt_prb_foyer.);
		PRB=sum(of prb1-prb5)-abatt;
		%do i=1 %to 5;
			/* Les abattements sont r�partis sur chaque individu au prorata */
			if sum(of prb1-prb5) ne 0 then abatt&i.=abatt*prb&i./sum(of prb1-prb5);
			/* On ne peut pas avoir plus d'abattement que de montant */
			abatt&i.=min(abatt&i.,prb&i.);
			%end;

		/**************************************/
		/* A4 Rentes viag�res � titre on�reux */
		RV1=_1aw*&rvto_tx1.;
		RV2=_1bw*&rvto_tx2.;
		RV3=_1cw*&rvto_tx3.;
		RV4=_1dw*&rvto_tx4.;
		RV=sum(of RV1-RV4);

		/* Abattement de 20 % avant 2006 (et 2 taux possibles au d�but des ann�es 90 */
		if 0<SAL+PRB<&abat_avt2006_lim1. then abattement=&abat_avt2006_t1.*(SAL+PRB);
		else if 0<SAL+PRB<&abat_avt2006_lim2. then abattement=&abat_avt2006_t1.*&abat_avt2006_lim1.+&abat_avt2006_t2.*(SAL+PRB-&abat_avt2006_lim1.);
		else abattement=0;

		rev1=Sal+PRB+RV-abattement;

		/********************************************************************************/
		/* B Revenus des valeurs et capitaux mobiliers (Cadre 2 D�claration de revenus) */
		/********************************************************************************/
		
		/********************************************************************/
		/* B1	Produits des contrats d'assurance-vie d'au moins 6 ou 8 ans */
		assuvie=max(_2ch-&abatassvie_plaf.*(1+(mcdvo in ('M','O'))),0);

		/* R�partition des frais d�ductibles (_2ca) entre les revenus des cat�gories ci-dessous (appel�es ici B2 et B3) */
		%Init_Valeur(r_b2 r_b3);
		if _2dc+_2ts>0 then do;
			r_b2=(_2dc+_2fu)/(_2dc+_2fu+_2ts+_2go+_2tr);
			r_b3=1-r_b2;
			end;
		FraisDeductibles_B2=_2ca*r_b2;
		FraisDeductibles_B3=_2ca*r_b3;
		drop r_b2 r_b3;
		
		/*****************************************************/
		/* B2	Revenus ouvrant droit � l'abattement de 40 % */
		rev2abat=_2dc+_2fu;
		FraisDeductibles_Res2=max(0,FraisDeductibles_B2-(1-&abat_rev_mob.)*rev2abat);
		/* Si FraisDeductibles n'est pas utilis� enti�rement (parce que major� par AutRev_Abatt-40%), 
		l'exc�dent pourra �tre utilis� dans les revenus sans abattement */
		rev2abat=max((1-&abat_rev_mob.)*rev2abat-FraisDeductibles_B2,0);
		rev2abat=max(rev2abat-&P0285.*(1+(mcdvo in ('M' 'O'))),0); /* abattement forfaitaire avant 2013 */

		/******************************************************************************/
		/* B3	Revenus bruts des capitaux mobiliers n'ouvrant pas droit � abattement */
		rev2ssabat=_2ts+_2go*&E2001.+_2tr;
		/* on ne met pas la case _2fa car pour ces revenus <2000 euros option pour le PF � 24 % sans condition sur le RFR */
		/* Reprise des frais d�ductibles non enti�rement utilis�s en limitant si frais sup�rieurs aux revenus */
		rev2ssabat=max(rev2ssabat-FraisDeductibles_B3,0);
		FraisDeductibles_Res3=max(0,FraisDeductibles_B3-rev2ssabat);
		FraisDeductibles_Restants=FraisDeductibles_Res2+FraisDeductibles_Res3;

		/**************************************************************************************************/
		/* B4	D�ficits ant�rieurs � d�duire (mais on ne peut pas d�duire plus que la somme des revenus) */
		deficit2=min(_2aa+_2al+_2am+_2an+_2aq+_2ar,assuvie+rev2abat+rev2ssabat);

		/* FINAL	Revenus de capitaux mobiliers nets imposables*/
		rev2=assuvie+rev2abat+rev2ssabat-deficit2;

		/**************************************************/
		/* C Plus values (Cadre 3 D�claration de revenus) */
		/**************************************************/

		/* Avant 2014 ces plus-values ne sont pas soumises au bar�me : elles sont trait�es directement dans 6_impot */
		/* Plus-values de cession de valeurs mobili�res  */
		RevCessValMob_HorsAbatt=(_3vg-_3vh); /* Montant net apr�s abattements pour dur�e de d�tention d�duits */
		RevCessValMob_Abatt=	(_3sg-_3sh); /* Abattements nets pour dur�e de d�tention (_3sg et _3sh)*/
		%if &anleg.>=2014 %then %do;
			rev3=RevCessValMob_HorsAbatt;
			%end;

		/*******************************************************/
		/* D revenus fonciers (Cadre 4 D�claration de revenus) */
		/*******************************************************/

		/*********************/
		/* D1	Microfoncier */
		%Init_Valeur(foncplus microfonc microfonc_caf);
		if _4be <= &plafmicrof. then do; 
			microfonc=max((_4be*(1-&tx_microfonc.)-_4bd),0);
			microfonc_caf=_4be*(1-&tx_microfonc.);
			end;
		/* Opter pour le micro-foncier exclut l'application des d�ficits de l'ann�e. 
		Mais les d�ficits des ann�es ant�rieures peuvent �tre imput�s sur les revenus nets
		d�termin�s selon le r�gime micro-foncier (brochure pratique IR 2002 rev 2001 : bas de page 124)*/
		else do; 
			foncplus=_4be - &plafmicrof.;
			microfonc_caf=&plafmicrof.*(1-&tx_microfonc.);
			end;
		/*si on est au dessus du plafond (comme quand ce r�gime n'existait pas) alors on bascule 
		du c�t� des revenus fonciers.*/

		/*************************/
		/* D2	Revenus fonciers */
		/* Se r�f�rer � la fiche facultative de calculs dans la brochure pratique pour suivre chaque �tape */
		_4bc=min(_4bc,&max_deficit_fonc.);/*limite au d�ficit global, on passe parfois au dessus � cause de la d�rive*/
		rev4c =_4ba+foncplus-_4bb;
		rev4e=0;
		if rev4c >=0 then do;
			rev4e=rev4c-_4bc;
			if rev4e >=0 then rev4e=max(rev4e-_4bd,0);
			end;
		else if rev4c<0 then do;
			if _4bc>0 then rev4e=-_4bc; 
			else rev4=0;
			end;

		rev4=rev4e+microfonc;
		rev4_caf=microfonc_caf+max(_4ba-_4bb-_4bc,0);


		/***********************************************************************/
		/* E	Revenus des professions non salari�es (D�claration 2042 C PRO) */
		/***********************************************************************/

		/************************************************/
		/* E1	BENEFICES et plus values de court terme */
		 
		/* B�n�fice agricole, forfait, r�el sans CGA, forfait avec CGA*/
		BA_forfait		=&E2001.*(_5ho+_5io+_5jo);	/*BA forfait*/
		BA_forfait_foret=_5hw+_5iw+_5jw;			/*BA forfait mais dissoci�s*/
		BA_forfait_PV	=_5hd+_5id+_5jd;			/*BA forfait : plus-values de court terme*/
		BA_reel1 	 	=&E2001.*(_5hi+_5ii+_5ji);	/* non adh�rant CGA*/
		BA_reel2	 	=_5hc+_5ic+_5jc;			/* adh�rant CGA*/

		/**********************************************************/
		/* E2	Revenus industriels et commerciaux professionnels */
		/*benef r��l sans cga*/
		BICRns=&E2001.*((_5ki+_5li+_5mi)-(_5kl+_5ll+_5ml));/*normal ou simplifi�*/
		BICRl=&E2001.*((_5ka+_5la+_5ma)-(_5qj+_5rj+_5sj));/*locations meubl�es*/
		/*benef r��l avec cga*/
		BICRcgans=(_5kc+_5lc+_5mc)-(_5kf+_5lf+_5mf);/*normal ou simplifi�*/
		BICRcgal=(_5ha+_5ia+_5ja)-(_5qa+_5ra+_5sa);/*locations meubl�es*/

		defagr=(_5hf+_5if+_5jf)+(_5hl+_5il+_5jl);

		/**********************************************************/
		/* E3	Revenus industriels et commerciaux professionnels */
		/*regime micro*/
		KOtax=max((_5ko-max(&E2000.,(_5ko*&abatmicmarch.))),0);
		LOtax=max((_5lo-max(&E2000.,(_5lo*&abatmicmarch.))),0);
		MOtax=max((_5mo-max(&E2000.,(_5mo*&abatmicmarch.))),0);
		KPtax=max((_5kp-max(&E2000.,(_5kp*&abatmicserv.))),0);
		LPtax=max((_5lp-max(&E2000.,(_5lp*&abatmicserv.))),0);
		MPtax=max((_5mp-max(&E2000.,(_5mp*&abatmicserv.))),0);

		BicMicP=KOtax+LOtax+MOtax+KPtax+LPtax+MPtax;

		/*Auto-entrepreneurs*/
		TAtax=max((_5ta-max(&E2000.,(_5ta*&abatmicmarch.))),0);
		UAtax=max((_5ua-max(&E2000.,(_5ua*&abatmicmarch.))),0);
		VAtax=max((_5va-max(&E2000.,(_5va*&abatmicmarch.))),0);
		TBtax=max((_5tb-max(&E2000.,(_5tb*&abatmicserv.))),0);
		UBtax=max((_5ub-max(&E2000.,(_5ub*&abatmicserv.))),0);
		VBtax=max((_5vb-max(&E2000.,(_5vb*&abatmicserv.))),0);

		BicAuto=TAtax+UAtax+VAtax+TBtax+UBtax+VBtax;

		/**************************************************************/
		/* E4	Revenus industriels et commerciaux non professionnels */
		/*regime micro*/
		NONGtax=max(((_5no+_5ng+_5nj)-max(&E2000.,((_5no+_5ng+_5nj)*&abatmicmarch.))),0);
		OOOGtax=max(((_5oo+_5og)-max(&E2000.,((_5oo+_5og)*&abatmicmarch.))),0);
		POPGtax=max(((_5po+_5pg)-max(&E2000.,((_5po+_5pg)*&abatmicmarch.))),0);
		NPNDtax=max(((_5np+_5nd)-max(&E2000.,((_5np+_5nd)*&abatmicserv.))),0);
		OPODtax=max(((_5op+_5pp)-max(&E2000.,((_5op+_5pp)*&abatmicserv.))),0);
		PPPDtax=max(((_5pp+_5pd)-max(&E2000.,((_5pp+_5pd)*&abatmicserv.))),0);

		BicMicNP=NONGtax+OOOGtax+POPGtax+NPNDtax+OPODtax+PPPDtax;

		/* Benef r��l sans cga*/
		BICnPns	 =&E2001.*((_5ni+_5oi+_5pi)-(_5nl+_5ol+_5pl));/*normal ou simplifi�*/
		BICnPl=&E2001.*((_5nk+_5ok+_5lm+_5pk+_5mm)-(_5nz+_5oz+_5pz));/*locations meubl�es*/

		/* Benef r��l avec cga*/
		BICnPcgans=(_5nc+_5oc+_5pc)-(_5nf+_5of+_5pf);/*normal ou simplifi�*/
		BICnPcgal=(_5na+_5oa+_5om+_5pa+_5pm)-(_5ny+_5oy+_5py);/*locations meubl�es*/

		/***********************************************/
		/* E5	Revenus non commerciaux professionnels */
		/*regime micro*/
		Hqtax=max((_5hq-max(&E2000.,(_5hq*&abatmicbnc.))),0);
		Iqtax=max((_5iq-max(&E2000.,(_5iq*&abatmicbnc.))),0);
		Jqtax=max((_5jq-max(&E2000.,(_5jq*&abatmicbnc.))),0);

		BNCSPEP=Hqtax+Iqtax+Jqtax;

		/* Autoentrepreneurs */
		TEtax=max((_5te-max(&E2000.,(_5te*&abatmicbnc.))),0);
		UEtax=max((_5ue-max(&E2000.,(_5ue*&abatmicbnc.))),0);
		VEtax=max((_5ve-max(&E2000.,(_5ve*&abatmicbnc.))),0);

		BncAuto=TEtax+UEtax+VEtax;
		AE_Rev=BicAuto+BncAuto;

		/*benef r��l sans aa*/
		Bncp=&E2001.*(_5qi+_5ri+_5si-(_5qk+_5rk+_5sk));
		/*benef r��l avec aa*/
		Bncpaa=_5qc+_5rc+_5sc-(_5qe+_5re+_5se);

		/***************************************************/
		/* E6	Revenus non commerciaux non professionnels */
		/*regime micro*/
		Kutax=max((_5ku-max(&E2000.,(_5ku*&abatmicbnc.))),0);
		Lutax=max((_5lu-max(&E2000.,(_5lu*&abatmicbnc.))),0);
		Mutax=max((_5mu-max(&E2000.,(_5mu*&abatmicbnc.))),0);
		BNCSPEnP=kutax+lutax+mutax;
		/*benef r��l sans aa*/
		bncnp=&E2001.*(_5sn+_5ns+_5os-(_5sp+_5nu+_5ou))+(_5tc+_5uc+_5vc);

		/*benef r��l avec aa*/
		bncnpaa=_5jg+_5rf+_5sf-(_5jj+_5rg+_5sg);


		rev5b=BA_reel2+BICRcgans+BICRcgal+BICnPcgans+BICnPcgal+bncpaa+bncnpaa;

		if 0<rev5b<&abat_avt2006_lim1. then rev5b=(1-&abat_avt2006_t1.)*rev5b;
		else if 0<rev5b<&abat_avt2006_lim2. then rev5b=rev5b-&abat_avt2006_t1.*&abat_avt2006_lim1.-&abat_avt2006_t2.*(rev5b-&abat_avt2006_lim1.);

		revagr	=BA_forfait+BA_reel1+BA_reel2;
		revbic	=BICRcgans+BICRns+BICRl+BICRcgal;
		revbicnp=BICnPcgans+BICnPcgal+BICnPns+BICnPl;
		revbncp	=bncp+bncpaa;
		revbncnp=bncnp+bncnpaa;

		/* la gestion des d�ficits est � revoir par rapport � l'abattement, par rapport aux revenus CAF, etc. (2011)*/
		revagr_caf	=revagr;
		revbicnp_caf=revbicnp;
		/*d�ficit ant�rieur*/
		deficitagr=_5qf+_5qg+_5qn+_5qo+_5qp+_5qq;/*agricole*/
		if revagr-defagr>=0 then revagr=max(revagr-deficitagr,0);
		deficitric=_5rn+_5ro+_5rp+_5rq+_5rr+_5rw;/*RICnonP*/
		if revbicnp-deficitric>=0 then revbicnp=max(revbicnp-deficitric,0);
		deficitrinc=_5ht+_5it+_5jt+_5kt+_5lt+_5mt;/*RnonCnonP*/
		/*modif : suppression des cases _5hd, _5id, _5jd,_5hj,_5ij,_5jj
		BAEXC=(_5hd+_5id+_5jd)/7 +&E2001.*(_5hj+_5ij+_5jj)/7; 
		les b�n�fices agricoles exceptionnels peuvent d�sormais �tre �tal�s sur 7 ann�es, 
		la fraction 1/7 correspondante �tant ajout�e par le d�clarant dans la case des revenus 
		imposables*/

		rev5=revbncnp+bncspenp + revbncp+bncspep + revbicnp +bicmicnp +revbic+bicmicp + revagr
				-BA_reel2-BICRcgans-BICRcgal-BICnPcgans-BICnPcgal-bncpaa-bncnpaa
				+rev5b+BicAuto+BncAuto;
		rev5_caf=revbncnp+bncspenp+revbncp+bncspep+revbicnp_caf+bicmicnp+revbic+bicmicp+revagr_caf
				-BA_reel2-BICRcgans-BICRcgal-BICnPcgans-BICnPcgal-bncpaa-bncnpaa
				+rev5b+BicAuto+BncAuto;
		AE=(_5ta+_5ua+_5va+_5tb+_5ub+_5vb+_5te+_5ue+_5ve)>0;

		/***************************************/
		/* E7	Plus values des r�gimes micros */
		/* Les d�ficits ne peuvent pas �tre plus grands que le revenu li� � l'activit�*/
		PVCTBICP	=_5kx+_5lx+_5mx-_5kj-_5lj-_5mj;	/*bicp*/
		PVCTBICnP	=_5nx+_5ox+_5px-_5iu;	/*bicnp*/
		PVCTBICnP	=max(PVCTBICnP,-bicmicnp);
		PVCTBnCP	=_5hv+_5iv+_5jv-_5kz-_5lz-_5mz;	/*bncp*/
		PVCTBInCnP	=_5ky+_5ly+_5my-_5ju;	/*bncnp*/
		PVCTBInCnP	=max(PVCTBInCnP,-BNCSPEnP);
		PVmicro		= PVCTBICP+PVCTBICnP+PVCTBnCP+PVCTBInCnP;



		/************************************************/
		/****** F -  D�finition de RBG ******************/
		/************************************************/


		/* revenus exon�r�s */
		exo1   =(_5hn+_5in+_5jn);					/*Revenus agricoles au forfait*/
		exo2   =(_5hb+_5ib+_5jb +_5hh+_5ih+_5jh); 	/*Revenus agricoles au b�n�f r�el*/
		exo3   =(_5kn+_5ln+_5mn);					/*bicp micro entreprise*/
		exo4   =(_5kb+_5lb+_5mb +_5kh+_5lh+_5mh);	/*ricp b�n�fice r��l*/
		exo5   =(_5nn+_5on+_5pn);					/*bicnp micro entreprise*/
		exo6   =(_5nb+_5ob+_5pb+_5nh+_5oh+_5ph);	/*ricnp*/
		exo7   =(_5hp+_5ip+_5jp);					/*rncp r�gime sp�cial ou micro*/
		exo8   =(_5qb+_5rb+_5sb +_5qh+_5rh+_5sh);	/*rncp d�cl control�e*/
		exo9   =(_5th+_5uh+_5vh);					/*rncnp sp�cial ou micro*/
		exo10  =(_5hk+_5jk+_5lk +_5ik+_5kk+_5mk);	/*rncnp d�cl control�e*/
		exo11  =(_8ti+_1ac+_1bc+_1cc+_1dc-_1ad-_1bd-_1cd-_1dd+_1ah+_1bh+_1ch+_1dh); /* Revenus de source �trang�re, 1ac � 1dc rendus nets d'imp�t */
		EXO=sum(of exo1-exo11);
		label EXO="EXO : Revenus exon�r�s";

		/*plus values taxables � 16%*/
		pvt1 =&P0750.*(_5hx+_5ix+_5jx);				/*agri au forfait*/
		pvt2 =&P0750.*(_5he+_5ie+_5je);				/*agr au b�n�f r��l*/
		pvt3 =&P0750.*((_5kq+_5lq+_5mq)-(_5kr+_5lr+_5mr));/*bicp micro entreprise*/
		pvt4 =&P0750.*(_5ke+_5le+_5me);				/*ricp b�n�fice r��l*/
		pvt5 =&P0750.*((_5nq+_5oq+_5pq)-(_5nr+_5or+_5pr));/*bicnp micro entreprise*/
		pvt6 =&P0750.*(_5ne+_5oe+_5pe);				/*ricnp*/
		pvt7 =&P0750.*((_5kv+_5lv+_5mv)-(_5kw+_5lw+_5mw));/*rncnp r�gime sp�cial ou micro*/
		pvt8 =&P0750.*(_5so+_5nt+_5ot);				/*rncnp d�cl control�e*/
		pvt9 =&P0750.*((_5hr+_5ir+_5jr)-(_5hs+_5is+_5js));/*rncnp r�gime sp�cial ou micro*/
		pvt10=&P0750.*(_5qd+_5rd+_5sd);				/*rncp d�cl control�e*/
		PVT=sum( of pvt1-pvt10);
		label PVT="PVT : Imp�t sur plus-values taxables � 16 %";

		RBG=rev1+rev2+rev3+rev4+rev5+_6gh+_8tk+PVmicro;
		RBG_caf= RBG 
				- rev4 + rev4_caf
				- rev5 + rev5_caf
				+ _8tm
				+ PVT/&P0750. /* montant total des PVT (calcul inverse) */
				+_PVCessionDom+_3vm+_3vt+_PVCession_entrepreneur+(_3sl+_3va)-(_3sm+_3vb)
				+RevCessValMob_Abatt
				+(RevCessValMob_HorsAbatt*(&anleg.<2014)) /* sinon d�j� inclus dans rev3 donc dans RBG */
				+_3vi+_3si+_3vf+_3sf+_3vd+_3sd		/* Gains de levee d'option au pr�l�vement liberatoire */
				+_3vz+_3wb+_3sj+_3sk
				+_2ee+_2dh+_2fa
				+(_hsupVous+_hsupConj+_hsupPac1+_hsupPac2)*((2008<&anleg.<2013)+7/12*(&anleg.=2013)+3/12*(&anleg.=2008))
				+EXO;

		%Init_Valeur(retir);
		IF RBG<=&P0320. AND defagr>0 THEN do; 
			RBG=RBG-defagr;
			retir=1;
			end;
		/* les deficits agricoles sont mis au d�ficit pour l'ann�e prochaine sauf si on a un certain revenu */

		deficit_ant=_6fa+_6fb+_6fc+_6fd+_6fe+_6fl;
		RBG=max(RBG-deficit_ant,0);


		/******************************************************************/
		/****** G -  Agr�gats et variables utiles pour la suite ***********/
		/******************************************************************/


		/* Autres revenus imposables selon le syst�me du quotient */
		/* Gains de lev�e d'options sur titre */
		quotient2=_1tw+_1uw;
		quotient3=_1tx+_1ux;
		/* Revenus exceptionnels ou diff�r�s */
		quotient4=_0xx; 

		/*on garde ici le chiffre d'affaire (des professionnels)*/
		chiffaff= BA_reel1 + BA_reel2+_5kc+_5lc+_5mc+_5ki+_5li+_5mi
				+_5qi+_5ri+_5si+_5qc+_5rc+_5sc;
		/*on garde aussi le montant des plus-values*/


		/***********************************************/
		/****** H -  D�finition d'un RBG individuel ****/
		/***********************************************/

		/* en prenant la d�claration d'imp�t, on ne cherche plus � sommer les diff�rentes 
		cases en lignes mais en colonnes (donc individuellement)*/
		%Macro CalculRevind(declarant);
		/* 	Le param�tre declarant doit �tre renseign� � d si on s'int�resse au vous, c au conjoint, et d1 pour la 1�re pac. 
			Cette macro calcule les variables
				zbag, zbag_cga, zbic, zbic_np, zbicnp_cga, zbncnp, znbcnp_cga, zbnc, zbnc_cga, 
				zbicmicro, zbicmicronp, zbncmicro, zbncmicronp, pvind, rev5ind et rev5ind_cga
				� chaque fois relatives au declarant pass� en param�tre. 
			Elle cr�e enfin les variables REVIND&i. et REV_CAT&i., o� i vaut 1 pour d, 2 pour c et 3 pour p1 */
				
			%DefinitCasesDeclarant(&declarant.);
			%if &declarant.=d %then %let indice=1;
			%else %if &declarant.=c %then %let indice=2;
			%else %if &declarant.=p1 %then %let indice=3;

			/*******************************/
			/* 1 - Revenus des ind�pendants*/
			/*******************************/
			/* Gr�ce � la macro appel�e plus haut on n'�crit qu'une seule fois les calculs, 
			comme s'il s'agissait du vous, mais les noms des cases sont suivis d'un '_' */
			/* R�gime agricole sans CGA + revenus exon�r�s avec CGA */
			zbag_&declarant.= _5hn_+_5ho_*&E2001.+_5hb_+_5hd_+_5hh_+_5hi_*&E2001.-_5hl_+_5hx_;
			/* R�gime agricole avec CGA : revenus imposables */
			zbag_&declarant._cga= _5hc_+_5he_-_5hf_;
			/* R�gime BIC professionnel r�gime du b�n�fice r�el sans CGA + revenus exon�r�s avec CGA  */
			zbic_&declarant.= _5kn_+_5kb_+_5kh_+_5ki_*&E2001.+_5ka_*&E2001.-_5kl_*&E2001.-_5qj_+_5kq_-_5kr_;
			/* R�gime BIC professionnel r�gime du b�n�fice r�el avec CGA : revenus imposables */
			zbic_&declarant._cga= _5kc_+_5ha_+_5ke_-_5kf_-_5qa_;
			/* R�gime BIC non professionnel au benefice r�el normal et simplifi� sans CGA + plus et moins values du r�gime de micro entreprise */
			zbicnp_&declarant.=	&E2001.*(_5ni_+_5nk_+_5km_+-_5nl_-_5nz_)+_5nq_-_5nr_;
			/* R�gime BIC non professionnel au benefice r�el normal et simplifi� avec CGA */
			zbicnp_&declarant._cga=	_5nc_+_5ne_-_5nf_+_5na_+_5nm_-_5ny_;
			/* R�gime BNC professionnel hors micro et plus value avec declaration controlee + revenus exon�r�s avec CGA */
			zbnc_&declarant.=_5hp_+_5qb_+_5qh_+_5qi_*&E2001.-_5qk_*&E2001.;
			/* R�gime BNC professionnel r�gime du b�n�fice r�el avec CGA : revenus imposables */
			zbnc_&declarant._cga=_5qc_-_5qe_+_5qd_;
			/* R�gime BNC non professionnel hors micro et plus value avec declaration controlee + revenus exon�r�s avec CGA */
			zbncnp_&declarant.=	&E2001.*_5sn_-&E2001.*_5sp_+_5kv_-_5kw_;
			/* R�gime BNC non professionnel r�gime du b�n�fice r�el avec CGA : revenus imposables */
			zbncnp_&declarant._cga=	_5jg_-_5jj_+_5so_+_5tc_;
			/* R�gime micro : calcul des correctifs abattements fiscaux a appliquer aux chiffres d'affaires pour retrouver des benefices */
			zbicmicro_&declarant.=	_5ko_+_5kp_+_5ta_+_5tb_
									-max(&abatmicmarch.*(_5ko_+_5ta_)+&abatmicserv.*(_5kp_+_5tb_),min(_5ko_+_5kp_+_5ta_+_5tb_,&E2000.));
			zbicmicronp_&declarant.=_5no_+_5ng_+_5nj_+_5np_+_5nd_
									-max(&abatmicmarch.*(_5no_+_5ng_+_5nj_)+&abatmicserv.*(_5np_+_5nd),min(_5no_+_5np_+_5ng_+_5nj_+_5nd_,&E2000.));
			zbncmicro_&declarant.=(_5hq_+_5te_)-max(&abatmicbnc.*(_5hq_+_5te_),min(_5hq_+_5te_,&E2000.));
			/*JD : avant ce n'�tait que l'abattement avec un signe moins, ai je bien fait de remettre (_5jq+_5ve) devant?*/
			zbncmicronp_&declarant.	=_5ku_-max(&abatmicbnc.*_5ku_,min(_5ku_,&E2000.));
			/*JD : idem, voir remarque pr�c�dente*/
			pvind_&declarant.=_5hw_+_5kx_+_5hv_-_5kj_-_5kz_;
			/* Application de l'abattement avec CGA */
			revind_&declarant._cga	=zbag_&declarant._cga+zbic_&declarant._cga+zbicnp_&declarant._cga+zbncnp_&declarant._cga+zbnc_&declarant._cga;
			if revind_&declarant._cga<&abat_avt2006_lim1. then revind_&declarant._cga=(1-&abat_avt2006_t1.)*revind_&declarant._cga;
			else if 0<revind_&declarant._cga<&abat_avt2006_lim2. then revind_&declarant._cga=revind_&declarant._cga-&abat_avt2006_t1.*&abat_avt2006_lim1.-&abat_avt2006_t2.*(revind_&declarant._cga-&abat_avt2006_lim1.);

			/* Regroupement des revenus des independants */
			revind&indice.=zbag_&declarant.+zbic_&declarant.+zbnc_&declarant.+zbicnp_&declarant.+zbncnp_&declarant.+zbicmicro_&declarant.+
								zbicmicronp_&declarant.+pvind_&declarant.+revind_&declarant._cga;


			/**************************/
			/* 2 - Revenu Global Brut */
			/**************************/

			/* on rajoute les revenus exoneres de la definition (� verifier)
			on fait sans l'abattement de 20 % sur les cga (� changer plus tard) */

			rev_cat&indice.=(sal&indice.
							+_hsupVous_*((2008<&anleg.<2013)+7/12*(&anleg.=2013)+3/12*(&anleg.=2008))
							-deduc&indice.
							+PRB&indice.
							-abatt&indice.)
							+revind&indice.
							-&abat_avt2006_t1.*max(0,min(sal&indice.+_hsupVous_*((2008<&anleg.<2013)+7/12*(&anleg.=2013)+3/12*(&anleg.=2008))-deduc&indice.+PRB&indice.-abatt&indice.,&abat_avt2006_lim1.));
			%Mend CalculRevind;

		%CalculRevind(d);
		%CalculRevind(c);
		%CalculRevind(p1);
		/* Ajustements pour le vous et calcul pour la pac2 */
		rev_cat1=	rev_cat1+RV+rev2+
					_PVCessionDom+_3vm+_3vt+_PVCession_entrepreneur+RevCessValMob_HorsAbatt+RevCessValMob_Abatt+(_3sl+_3va)-(_3sm+_3vb)+
					_3vi+_3vf+_3vd+_3vz+_3wb+_2ee+_2dh+_2fa
					+rev4_caf+_6gh+_8tm;
		revind4=0;
		rev_cat4=(	sal4
					+_hsupPac2*((2008<&anleg.<2013)+7/12*(&anleg.=2013)+3/12*(&anleg.=2008))
					-deduc4
					+PRB4
					-abatt4)
					+revind4
					-&abat_avt2006_t1.*max(0,min((sal4+_hsupPac2*((2008<&anleg.<2013)+7/12*(&anleg.=2013)+3/12*(&anleg.=2008))-deduc4+PRB4-abatt4),&abat_avt2006_lim1.));
		run;
	%Mend RBG;
%RBG;


/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
