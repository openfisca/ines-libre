/************************************************************************************
/*																					*/
/*								forf_log											*/
/*																					*/
/************************************************************************************/

/* Ce pgm calcule le forfait logement et finalise le calcul du rsa avant application*/
/* du taux de recours pour le rsa act)												*/
/* En entr�e : modele.basersa 														*/
/*			   base.menage&anr2														*/
/*			   modele.baseind														*/
/*			   modele.baselog														*/
/*			   imput.accedant														*/		
/* En sortie : modele.basersa                                     					*/
/************************************************************************************/
/* PLAN : 																			*/
/* A. Calcul des aides au logement per�ues par les foyers rsa						*/
/* B. Calcul du forfait logement													*/
/* C. Finalisation du calcul du RSA (dont d�finition du rsa activit�)				*/
/************************************************************************************/
/* NOTE : 																			*/
/* Ce programme calcule le forfait logement � d�duire du rsa pour les tous les 		*/
/* b�n�ficiaires d'un avantage en termes de logement :								*/
/* - avantage en nature : pour tous les propri�taires et personnes h�berg�es � 		*/
/*	titre gratuit																	*/
/* - avantage mon�taire : pour tous les acc�dants et locataires b�n�ficiaires d'AL	*/
/* Le pgm alimente la table modele.basersa cr��e dans le pgm rsa 					*/


/**************************************************************/
/* A. Calcul des aides au logement per�ues par les foyers rsa */
/**************************************************************/

%Macro ForfaitLogement;
	/* R�cup�ration de l'info statut d'occupation du logement de la base menage des foyers rsa */
	proc sort data=modele.basersa; by ident_rsa; run;
	data modele.basersa;
		merge modele.basersa (in=a) 
			  base.menage&anr2. (keep=ident logt); 
		by ident;
		if a; 
		run;

	/* R�cup�ration de l'ident_rsa des locataires b�n�ficiaires d'AL*/
	proc sort data=modele.baseind (keep= ident_log ident_rsa) 
		out=lien1 nodupkey; 
		by ident_log; 
		run; 
	data AL; 
		merge modele.baselog 
			  lien1; 
		by ident_log; 
		if AL>0;
		run;

	/* R�cup�ration de l'ident_rsa des propri�taires acc�dants b�n�ficiaires d'AL*/
	proc sort data=modele.baseind (keep= ident ident_rsa) 
		out=lien2 nodupkey; 
		by ident; 
		run; 
	proc sort data=imput.accedant; by ident; run; 
	data accedant; 
		merge imput.accedant 
			  lien2; 
		by ident; 
		if alaccedant>0;
		run;

	/* Somme des AL per�ues par les foyers rsa (propri�taires et acc�dants)*/
	data AL; set AL accedant ; run;
	proc sort data=AL; by ident_rsa; run; 
	proc means data=AL noprint nway;
		class ident_rsa;
		var AL alaccedant;
		output out=AL2(drop=_type_ _freq_) sum=; 
		run;

	/*********************************/
	/* B. Calcul du forfait logement */
	/*********************************/

	/* Finalisation du calcul du forfait logement par rapport � sa valeur th�orique FL_theorique calcul�e dans le pgm rsa */
	data basersa;
		merge modele.basersa(in=a) 
			  AL2;
		by ident_rsa; 
		if a;
		array tout _numeric_; 
		do over tout; 
			if tout=. then tout=0;
			end;
		%init_valeur(forf_log);
		forf_log=FL_theorique;

		/* Pour les locataires non aid�s: FL=0*/
		if logt in ('3','4','5') & al=0 then forf_log=0; 
		/* Pour les locataires/propri�taires ayant une aide : 
		si FL>montant de l'allocation => on retire le montant de l'AL du rsa.  
		Dans ces cas, on attribue directement au FL la valeur de l'AL � d�duire du rsa*/
		if logt in ('3','4','5') & al>0 & forf_log>al/12 then forf_log=al/12;
		if logt in ('1','2') & alaccedant>0 & forf_log>alaccedant/12 then forf_log=alaccedant/12; 

	/**********************************************************************/
	/* C. Finalisation du calcul du RSA (dont d�finition du rsa activit�) */
	/**********************************************************************/

		%do i=1 %to 4;
		/* D�duction du forfait logement des droits � rsa */
			m_rsa&i.=m_rsa_th&i.-3*forf_log;
			m_rsa_socle&i.=max(m_rsa_socle_th&i.-3*forf_log,0);
			/* Application du seuil de versement sur le rsa total et sur le socle */
			if m_rsa&i.<3*&rsa_min. then m_rsa&i.=0;
			if m_rsa&i.=0 then m_rsa_socle&i.=0;
			/* D�fnition du rsa activit� */
			rsaact_eli&i. = m_rsa&i. - m_rsa_socle&i.;
			%end;
		/* Calcul des montants annuels */
		rsasocle= sum(of m_rsa_socle1-m_rsa_socle4);
		rsaact_eli=sum(of rsaact_eli1-rsaact_eli4);
		rsatot_eli= sum(of m_rsa1-m_rsa4);
		/* Finalisation du calcul de la prime de Noel du rsa � partir de sa valeur th�orique 
		rsa_noel calcul�e dans le pgm rsa*/
		if m_rsa_socle4=0 then rsa_noel=0;
	run;

	data modele.basersa; 
		set basersa;
		drop AL alaccedant logt;
		label	m_rsa1='montant de RSA au T1 en plein recours'
				m_rsa2='montant de RSA au T2 en plein recours'
				m_rsa3='montant de RSA au T3 en plein recours'
				m_rsa4='montant de RSA au T4 en plein recours'
				m_rsa_socle1='montant de RSA socle au T1 en plein recours'
				m_rsa_socle2='montant de RSA socle au T2 en plein recours'
				m_rsa_socle3='montant de RSA socle au T3 en plein recours'
				m_rsa_socle4='montant de RSA socle au T4 en plein recours'
				rsaact_eli1='montant de RSa activite au T1 en plein recours'
				rsaact_eli2='montant de RSa activite au T2 en plein recours'
				rsaact_eli3='montant de RSa activite au T3 en plein recours'
				rsaact_eli4='montant de RSa activite au T4 en plein recours'
				forf_log='forfait logement effectif'
				rsasocle='RSA socle annuel en plein recours'
				rsaact_eli='RSA activit� annuel en plein recours'
				rsatot_eli='RSA total annuel en plein recours';
		run;

	%Mend ForfaitLogement;

%ForfaitLogement;

	 


/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
