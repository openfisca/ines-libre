*************************************************************************************;
/*																					*/
/*								Calcul du RSA										*/
/*																					*/
*************************************************************************************;

/* Calcul de l'�ligibilit� et des montants de RSA                  	*/
/* En entr�e : 	base.RessTrim                                      */
/* 				modele.baseind                                      */
/*				modele.basefam										*/
/*				base.foyer&anr2.									*/
/*				base.menage&anr2.									*/
/* En sortie : 	modele.basersa                                      */

/*ATTENTION : le calcul du RSA ne sera finalis� qu'apr�s le pgm "application_non_recours_rsa"*/
		
*************************************************************************************;
/* Plan

Partie I : calcul de la base ressources des foyers RSA � partir des revenus des 
	diff�rentes unit�s
	1. 	revenus individuels : revenus d'activit� (y c trimestrialisation et cumul int�gral) 
		et prestations individuelles;
	2. 	revenus de la famille CAF;
	3. 	revenus non individualisables des foyers fiscaux => on les attribue au d�clarant 
		(et donc au foyer rsa du d�clarant);
	4. 	revenus non individualisables du m�nage => on les attribue � la pr du m�nage ; 
	5. 	int�gration des infos ressources au niveau du foyer rsa => table res_rsa et calcul 
		des bases ressources trimestrielles;

Partie II : Travail sur les foyers rsa
	1. 	r�cup�ration des infos familiales.
	2. 	construction de la table foyers rsa �ligibles (�ge)
	3. 	cr�ation de la table des foyers avec toutes les infos pour le calcul du rsa

Partie III : calcul du rsa
	1. 	calcul du rsa et du forfait logement th�orique avant exclusion des pac ayant des 
		ressources
	2. 	Rep�rage des pac ayant des ressources, donv pouvant sortir du foyer rsa 
	3. 	calcul d'un rsa sans la pac en question pour les foyers concern�s
	4. 	recalcul du rsa pour les foyers concern�s par une exclusion
	5. 	cr�ation de la table de sortie modele.basersa */

*************************************************************************************;


*******************************************************************************************
*Partie I : calcul de la base ressources des foyers RSA � partir des ressources des 
diff�rentes unit�s
*******************************************************************************************;

/*1. LES RESSOURCES INDIVIDUELLES :
	- les revenus d'activit� et de remplacement, trimestrialis�s : 
	les salaires sont ensuite modifi�s selon la r�gle du cumul int�gral qui permet de conserver 
	le b�n�fice du RSA socle pendant les 3 mois qui suivent une reprise d'activit�
	- les prestations individuelles: dont l'aah, dont il faut distinguer le titulaire si c'est 1 pac*/

proc sort data=base.RessTrim; by ident noi; run;
proc sort data=modele.baseind; by ident noi; run;

data RessTrim;
	merge 	base.RessTrim  
	       	modele.baseind (keep=ident noi aah caah asi aspa statut_rsa in=a);
	by ident noi; 
	if a & ident_rsa ne '';

	/* rep�rage des ressources d'aah et de caah des futures pac et des civ des foyers rsa 
	(les pac aah seront exclues en II.2)*/
	%Init_Valeur(aahc_pac aahc_civ);
	if statut_rsa='pac' & aah then aahc_pac = aah+caah;
	if statut_rsa='' & aah then aahc_civ = aah+caah;

	/*CUMUL INTEGRAL: rep�rage de la 1�re reprise d'emploi au cours de l'ann�e et, selon, 
	le moment o� elle intervient, annulation ou diminution des revenus d'activit� du ou 
	des trimestres correspondants

		reprise en f�vrier : annulation simple des revenus d'activit� du trimestre en cours
		(par d�finition r�partis en f�vrier et mars) et, de tout moiti� ou un tiers des 
		revenus d'activit� du trimestre	suivant, selon le nb de mois de salaire comptabilis�*/

	if substr(cal0,11,2) in ('14','16','17','18','19') then do zsaliT1=0;
		if nbmois_salT2 = 1 then zsaliT2=0; 
		else if nbmois_salt2 = 2 then zsaliT2=1/2*zsaliT2;
		else if nbmois_salt2 = 3 then zsaliT2=2/3*zsaliT2; 
		end;
		/*mars : 1 mois � enlever au T1 et 2 au T2 */
	else if substr(cal0,10,2) in ('14','16','17','18','19') then do zsaliT1=0;
		if nbmois_salT2 = 3 then zsaliT2=1/3*zsaliT2;
		else if 0<=nbmois_salt2<3 then zsaliT2=0; 
		end;
		/*avril : on enl�ve tout au T2 */
	else if substr(cal0,9,2) in ('14','16','17','18','19') then zsaliT2=0;
		/*mai : comme f�vrier */
	else if substr(cal0,8,2) in ('14','16','17','18','19') then do zsaliT2=0;
		if nbmois_salT3 = 1 then zsaliT3=0; 
		else if nbmois_salt3 = 2 then zsaliT3=1/2*zsaliT3;
		else if nbmois_salt3 = 3 then zsaliT3=2/3*zsaliT3; 
		end;
		/*juin : comme mars */
	else if substr(cal0,7,2) in ('14','16','17','18','19') then do zsaliT2=0;
		if nbmois_salT3 = 3 then zsaliT3=1/3*zsaliT3;
		else if 0<=nbmois_salt3<3 then zsaliT3=0; 
		end;
		/*juillet : on enl�ve tout au T3 */
	else if substr(cal0,6,2) in ('14','16','17','18','19') then zsaliT3=0;
		/*aout : comme f�vrier */
	else if substr(cal0,5,2) in ('14','16','17','18','19') then do zsaliT3=0;
		if nbmois_salT4 = 1 then zsaliT4=0; 
		else if nbmois_salt4 = 2 then zsaliT4=1/2*zsaliT4;
		else if nbmois_salt4 = 3 then zsaliT4=2/3*zsaliT4; 
		end;
		/*septembre : */
	else if substr(cal0,4,2) in ('14','16','17','18','19') then do zsaliT3=0;
		if nbmois_salT4 = 3 then zsaliT4=1/3*zsaliT4;
		else if 0<=nbmois_salt4<3 then zsaliT4=0; 
		end;
		/*octobre : annulation de tout le T4*/
	else if substr(cal0,3,2) in ('14','16','17','18','19') then zsaliT4=0;
		/*novembre : annulation de tout le T4*/
	else if substr(cal0,2,2) in ('14','16','17','18','19') then zsaliT4=0;
		/*d�cembre : annulation de tout le T4*/
	else if substr(cal0,1,2) in ('14','16','17','18','19') then zsaliT4=0;
	run;

/* Regroupement des revenus individuels par foyer rsa => table res_ind */
proc sort data=RessTrim; by ident_rsa; run;
proc means data=RessTrim noprint nway;
	class ident_rsa;
	var zsaliT1-zsaliT4 zchoiT1-zchoiT4 zrstiT1-zrstiT4 zpiiT1-zpiiT4 zindiT1-zindiT4
		aahc_pac aahc_civ caah asi aspa zalri&anr2 zrtoi&anr2;
	output out=res_ind(drop=_type_ _freq_) sum=;
	run;


/*2. LES RESSOURCES DE LA FAMILLE CAF 
	- les prestations familiales + r�cup�ration de variables famille utiles (p_iso age_enf)*/

%Macro RessourcesFamille;
	data famille(drop = mpaje mois00);
		set modele.basefam(keep=ident_fam afxx0 asf_horsReval2014 paje_base droit_ab comxx majo_comxx clca pers_iso age_enf mois00);
		/* d�duction des ressources familiales d'une partie de l'AB de la paje la 1�re ann�e : 1 mois d�duit pour le RSA, 
		3 mois suppl�mentaires pour le RSA major� */
		if paje_base>0 & mois00 ne '' then do ; 
			%if &anleg. <= 2013 %then %do ; /* Jusqu'en 2013, l'allocation de base de la PAJE est exprim�e en fonction de la BMAF */
				if droit_AB='plein' then mpaje=&bmaf.*&paje_t.;
				if droit_AB='partiel' then mpaje=&bmaf.*&paje_t_partiel.; 
				%end ;
			%if &anleg. = 2014 %then %do ; /* A partir du 1er avril 2014, l'AB de la PAJE est gel�e et surtout d�connect�e de la BMAF */ 
				if droit_AB='plein' then mpaje=((3*&bmaf_n.*&paje_t.)+(9*&paje_m.))/12; /* pour les 3 premiers mois de l'ann�e, l'AB est encore exprim�e en fonction de la BMAF (valeur de celle-ci au 1er janvier) */
				if droit_AB='partiel' then mpaje=((3*&bmaf_n.*&paje_t_partiel.)+(9*&paje_m_partiel.))/12;
				%end ;
			%if &anleg. >= 2015 %then %do ; 
				if droit_AB='plein' then mpaje=&paje_m.; 
				if droit_AB='partiel' then mpaje=&paje_m_partiel.;
				%end ;
		if pers_iso = 0 then paje_base=paje_base - mpaje; 
		else if pers_iso=1 then do; 
				if mois00='12' then  paje_base=paje_base - mpaje; 
				else if mois00='11' then  paje_base=paje_base - 2*mpaje; 
				else if mois00='10' then  paje_base=paje_base - 3*mpaje; 
				else paje_base=paje_base - 4*mpaje; 
				end;
			end; 
		%if &anleg.<2004 %then %do;
			paje_base=0; /*on supprimait toute l'aide avant 2004*/
			%end;
		run;
	%Mend RessourcesFamille;
%RessourcesFamille;

/*r�cup�ration du num�ro ident_rsa de baseind pour le mettre dans la table famille (cr�ation de la table lien ident_fam<=>ident_rsa)*/
proc sort data=famille nodupkey; by ident_fam;
proc sort data=modele.baseind(keep=ident_fam ident_rsa) out=lien nodupkey; by ident_fam; run;
data famille;
	merge famille (in=a) lien;
	comxx=comxx-majo_comxx;/* on enl�ve la majoration du compl�ment familial de la base ressources (si pas de majorations alors majo_comxx=0) */
	by ident_fam;
	if a;
	run;
/* somme des ressources familiales par foyer rsa => table res_fam */
proc sort data=famille; by ident_rsa; run;
proc means data=famille noprint nway;
	class ident_rsa;
	var afxx0 asf_horsReval2014 paje_base comxx clca;
	output out=res_fam(drop=_type_ _freq_) sum=;
	run;


/*3. LES RESSOURCES DU FOYER FISCAL
	- toutes les ressources non individualisables du foyer (ressources de l'ann�e simul�e)
	HYPOTHESE : on les attribue au d�clarant du foyer fiscal, donc � son foyer rsa*/

data foyer(keep=ident noi declar zvalf zetrf zvamf zfonf zracf separation);
	set base.foyer&anr2.;
	/* omission dans les agr�gats des foyers des revenus imput�s par ailleurs � leurs m�nages
	et qui seront comptabilis�s � l'�tape 4 */
	zvamf=sum(0,zvamf,-_2ch,-_2fu);
	zvalf=zvalf-max(0,_2dh);
	/* rep�rage des divorces ou ruptures de pacs dans l'ann�e pour simuler du RSA major� */
	separation=(xyz='Y');
	run;
/* r�cup�ration du num�ro ident_rsa de baseind pour le mettre dans la table foyer*/
proc sort data=foyer; by ident noi;
proc sort data=modele.baseind; by ident noi; run;
data foyer;
	merge 	foyer (in=a) 	
			modele.baseind (keep=ident noi ident_rsa);
	by ident noi;
	if a;
	run;
/* somme des ressources du foyer fiscal par foyer rsa => table res_foy*/
proc sort data=foyer; by ident_rsa; run;
proc means data=foyer noprint nway;
	class ident_rsa;
	var zracf zfonf zetrf zvamf zvalf separation;
	output out=res_foy(drop=_type_ _freq_) sum=;
	run;


/*4. LES RESSOURCES DU MENAGE 
	- les produits financiers imput�s au m�nage l'ann�e simul�e :
	En th�orie, il faut prendre en compte dans la BR du rsa 
	les revenus du capital (lorsqu'ils sont d�clar�s) et � d�faut 3% (par an, soit 0,75% par trimestre) du capital d�tenu
	(y compris l'�pargne disponible, type livret A et l'�pargne non plac�e, type compte courant).
	cela n'�tant pas possible, on utilise les revenus des produits financiers imput�s aux m�nages par RPM.
	HYPOTHESE : on les attribue � la personne de r�f�rence du m�nage, donc � son foyer rsa*/

proc sort data=base.menage&anr2.; by ident; run;
proc sort data=modele.baseind; by ident; run;
data prodfin;
	merge 	modele.baseind (in=a) 
			base.menage&anr2. (keep=ident produitfin_i);
	by ident;
	if a;
	if lprm='1' then prodfin=produitfin_i;
	else prodfin=0;
	run;
/* somme des ressources du m�nage par foyer rsa => table res_men*/
proc means data=prodfin noprint nway;
	class ident_rsa;
	var prodfin;
	output out=res_men(drop=_type_ _freq_) sum=;
	run;


/*5. CONSTRUCTION DE LA BR DU RSA A PARTIR DES 4 ORIGINES DES RESSOURCES
	- cr�ation de la table res_rsa qui regroupe les ressources au niveau du foyer rsa
	- calcul des bases ressources trimestrielles*/

proc sort data=res_ind; by ident_rsa;
proc sort data=res_fam; by ident_rsa;
proc sort data=res_foy; by ident_rsa; 
proc sort data=res_men; by ident_rsa; run;
data res_rsa;
	merge 	res_ind (in=a) 
			res_fam 
			res_foy 
			res_men;
	by ident_rsa;
	if a ;
	array num _numeric_; do over num; if num=. then num=0; end;
	/* calcul des revenus d'activit� pris en compte chaque trimestre dans la base ressources du rsa (38%)*/
	rce1_rsa1=(1-&trsa.)*sum(0,zsaliT1,zindiT1);
	rce1_rsa2=(1-&trsa.)*sum(0,zsaliT2,zindiT2);
	rce1_rsa3=(1-&trsa.)*sum(0,zsaliT3,zindiT3);
	rce1_rsa4=(1-&trsa.)*sum(0,zsaliT4,zindiT4);

	/* calcul de toutes les autres ressources prises en compte chaque trimestre : les 
	revenus du ch�mage et de la retraite sont consid�r�s par trimestre et les autres 
	ressources sont liss�es sur l'ann�e en divisant par 4.
	Pour l'aah et le caah, on ne retient pas celui des pac, qui seront exclues du foyer 
	dans la partie III*/
	rce2_rsa1=max(0,sum(0,zchoiT1,zrstiT1,zpiiT1)+
		1/4*sum(0,zrtoi&anr2.,zalri&anr2.,zracf,zfonf,zvamf,zetrf,zvalf,prodfin,
				clca,afxx0,paje_base,comxx,asf_horsReval2014,aahc_civ,asi,aspa));
	rce2_rsa2=max(0,sum(0,zchoiT2,zrstiT2,zpiiT2)+
		1/4*sum(0,zrtoi&anr2.,zalri&anr2.,zracf,zfonf,zvamf,zetrf,zvalf,prodfin,
				clca,afxx0,paje_base,comxx,asf_horsReval2014,aahc_civ,asi,aspa));
	rce2_rsa3=max(0,sum(0,zchoiT3,zrstiT3,zpiiT3)+
		1/4*sum(0,zrtoi&anr2.,zalri&anr2.,zracf,zfonf,zvamf,zetrf,zvalf,prodfin,
				clca,afxx0,paje_base,comxx,asf_horsReval2014,aahc_civ,asi,aspa));
	rce2_rsa4=max(0,sum(0,zchoiT4,zrstiT4,zpiiT4)+
		1/4*sum(0,zrtoi&anr2.,zalri&anr2.,zracf,zfonf,zvamf,zetrf,zvalf,prodfin,
				clca,afxx0,paje_base,comxx,asf_horsReval2014,aahc_civ,asi,aspa));
	run; 


/**************************************
Partie II : CREATION DE LA TABLE FOYERS_RSA
**************************************/

/* 1. construction d'une table de foyers rsa � partir de la table famille */
proc sort data=famille; by ident_rsa ident_fam;run; 
data foyer_rsa (keep=ident_rsa ident_fam age_enf pers_iso); 
	set famille;  
	by ident_rsa ident_fam;
	if first.ident_rsa; 
	run; 
/*PB : dans les cas o� 2 familles composent un foyer rsa, on ne prend les infos que d'une famille */

/*2. construction d'une table de foyers rsa � partir des individus de baseind en deux temps
	- d�compte des adultes et des personnes � charge par foyer (hors pac aah)
	- construction de la table foyers en ne gardant que les foyers �ligibles de par leur �ge 
	ou la pr�sence d'enfant*/
proc sort data=modele.baseind; by ident_rsa; run;
data nb_pac; 
	set modele.baseind (keep=ident wpela&anr2. noi ident_rsa ident_fam noicon noienf01 naia quelfic 
			statut_rsa aah in=a where=(ident_rsa ne ''));
	by ident_rsa;
	/* on enl�ve les pac aah de la table individuelle servant � fabriquer les foyers rsa*/
	if statut_rsa='pac' & aah then delete;
	retain pac nbciv astucage; 
	if first.ident_rsa then do; pac=0; nbciv=0; astucage=0; end;
	if statut_rsa='pac' then pac=pac+1;
	else nbciv=nbciv+1;
	/* astuce pour d�terminer l'�ligibilit� de chaque foyer � partir de l'�ge ou la pr�sence d'enfant � charge */
	astucage=max(astucage,(&anref.-input(naia,4.))+&age_rsa_l.*(noienf01 ne '')); 
	%init_valeur(separation);
	run;
proc sort data=nb_pac; by ident_rsa; run;
data pac; set nb_pac(where=(astucage>=&age_rsa_l)); by ident_rsa; if last.ident_rsa; run; 

/*3. fusion des 3 tables de niveau foyer rsa constitu�es jusqu'� maintenant */
data rsa1;
	merge 	res_rsa 
			foyer_rsa 
			pac(in=a keep=ident_rsa pac nbciv wpela&anr2.);
	by ident_rsa; 
	if a; 
	run;


/***************************
*Partie III : calcul du rsa
***************************/

/*1. calcul du rsa en incluant toutes les pac du foyer (certaines seront exclues plus tard)
	- a. calcul du montant forfaitaire et du forfait logement th�orique pour toutes les configuations
	- b. calcul du rsa et du rsa socle */

%macro Calcul_RSA(nbciv,pac,m_rsa,m_rsa_socle);
	nb_foyer=&nbciv.+&pac.; 
	if nb_foyer=1 then do; 
		rsa=&rsa.; 
		FL_theorique=&rsa.*&forf_log1.; end;
	if nb_foyer=2 then do; 
		rsa=&rsa.*(1+&rsa2.); 
		FL_theorique=&rsa.*(1+&rsa2.)*&forf_log2.; end;
	if nb_foyer>2 then do; 
		rsa=&rsa.*(1+&rsa2.+&rsa3.*(nb_foyer-2)+ &rsa4.*(&pac-2)*(&pac>2)); 
		FL_theorique=&rsa.*(1+&rsa2.+&rsa3.)*&forf_log3.; end;
	rsa_noel=&rmi_noel.*rsa/&rsa.;
	/* �ligibilit� et application du rsa major� et ses l�gislations ant�rieures (api) */
	%nb_enf(enf03,0,3,age_enf);
	%nb_enf(e_c,0,&age_pf.,age_enf);
	if (enf03>0 & pers_iso=1) ! (e_c>0 & separation>0) then do;
		/* RSA major� */
		rsa=(&mrsa1.+&pac*&mrsa2.)*&rsa.;
		/* API */
		%if &anleg.<2007 %then %do;
			FL_theorique = &bmaf.*	(&forf_log_api1.*(e_c=0) 
								+ &forf_log_api2.*(e_c=1) 
								+ &forf_log_api2.*(e_c>1));
			%end;
		%if &anleg.<2009 %then %do;
			rsa=(&mapi1.+e_c*&mapi2.)*&bmaf.;
			%end;
		end;
	/*b. calul du rsa et du rsa socle sans tenir compte du forfait logement
	mise � 0 des montants de prestation trimestriels et vectorisation*/
	&m_rsa.1=0; &m_rsa.2=0; &m_rsa.3=0; &m_rsa.4=0;
	&m_rsa_socle.1=0; &m_rsa_socle.2=0; &m_rsa_socle.3=0; &m_rsa_socle.4=0;
	array rce1_rsa rce1_rsa1-rce1_rsa4;
	array rce2_rsa rce2_rsa1-rce2_rsa4;
	array &m_rsa &m_rsa.1-&m_rsa.4;
	array &m_rsa_socle &m_rsa_socle.1-&m_rsa_socle.4;
	/* calcul par trimestre */
	do over &m_rsa;
		&m_rsa = max(0,3*(rsa) - rce1_rsa - rce2_rsa); /* revenus activit� compt�s pour 38% dans la BR */
		&m_rsa_socle = max(0,3*(rsa) - rce1_rsa/(1-&trsa) - rce2_rsa); /* revenus activit� compt�s � 100% dans la BR */
		%if &anleg.<2009 %then %do;
			&m_rsa=&m_rsa_socle;
			%end;
		end;
	%if &anleg.=2009 %then %do;
		m_rsa1=m_rsa_socle1;
		m_rsa2=m_rsa_socle2+(m_rsa2-m_rsa_socle2)/3; /* Le RSA est distribu� � partir du mois de juin */
		%end;
	%mend Calcul_RSA;

data rsa1; 
	set rsa1;
	/*a. calcul du montant forfaitaire du rsa, du forfait logement et de la prime de noel */
	%Calcul_RSA(nbciv,pac,m_rsa,m_rsa_socle);
	/*cr�ation d'une variable rsa annuel qui sera utilis�e seulement � l'�tape 3 pour d�terminer l'�ventuelle exclusion de pac*/
	rsa_an = sum(0,of m_rsa1-m_rsa4);
	run;


/*2. Rep�rage des pac qui peuvent sortir du foyer rsa : pac de 20 � 25 ans avec des ressources personnelles*/
proc sort data=RessTrim; by ident noi;
proc sort data=nb_pac; by ident noi;
data exclu; 
	merge RessTrim nb_pac;
	by ident noi;
	if ident_rsa ne '' & statut_rsa='pac' & (&anref.-input(naia,4.)>=&age_pl.);
	/* d�s une pac ayant des ressources personnelles au moins 1 trimestre, elle est excluable*/
	%macro Exclusion_Potentielle;
		%do i=1 %to 4; 
			if  sum(0,zsaliT&i.,zindiT&i.,zchoiT&i.,zrstiT&i.,zpiiT&i.,
					1/4*sum(0,zrtoi&anr2.,zalri&anr2.,asi))>0 
				then pot_excluT&i.=1;
			else pot_excluT&i.=0; %end; 
		%mend Exclusion_Potentielle;
	%Exclusion_Potentielle;
	run; 


/*3. Calcul du rsa pour les foyers concern�s par une exclusion potentielle
- HYPOTHESE : une seule exclusion par foyer : dans le cas d'exclusions multiples, on garde la plus avantageuse pour le foyer*/
proc sort data=exclu; by ident_rsa noi; run;
data exclusion; 
	merge	exclu (in=a)
			rsa1(keep= ident_rsa m_rsa: rce1: rce2: pac nbciv age_enf pers_iso rsa_an);
	by ident_rsa;
	if a;
	if pot_excluT1 = 1 ! pot_excluT2 = 1 ! pot_excluT3 = 1 ! pot_excluT4 = 1 then do;
		/* calcul des ressources du foyer en otant les ressources personnelles de la pac*/
		rce1_rsa1 = rce1_rsa1-(1-&trsa.)*sum(0,zsaliT1,zindiT1);	
		rce1_rsa2 = rce1_rsa2-(1-&trsa.)*sum(0,zsaliT2,zindiT2);
		rce1_rsa3 = rce1_rsa3-(1-&trsa.)*sum(0,zsaliT3,zindiT3);
		rce1_rsa4 = rce1_rsa4-(1-&trsa.)*sum(0,zsaliT4,zindiT4);
		rce2_rsa1 = rce2_rsa1-(asi+zalri&anr2.+zrtoi&anr2.)/4;
		rce2_rsa2 = rce2_rsa2-(asi+zalri&anr2.+zrtoi&anr2.)/4;
		rce2_rsa3 = rce2_rsa3-(asi+zalri&anr2.+zrtoi&anr2.)/4;
		rce2_rsa4 = rce2_rsa4-(asi+zalri&anr2.+zrtoi&anr2.)/4;
		/*calcul du rsa du foyer si la pac �tait exclue du foyer*/
		%Calcul_RSA(nbciv,pac-1,m_rsa_exclu,m_rsasocl_exclu);
		rsa_exclu_an = sum(0,of m_rsa_exclu1-m_rsa_exclu4);
		/*comparaison du rsa du foyer avec et sans la pac : si le rsa sans la pac est plus �lev�, on on fixe exclu � 1*/
		exclu=(rsa_exclu_an > rsa_an); 
		end;
	if exclu;
	run;
proc sql;
	create table optim as
	select distinct *
	from exclusion
	group by ident_rsa
	having rsa_exclu_an=max(rsa_exclu_an);
	quit;

/*4. recalcul du rsa pour les foyers concern�s par une exclusion*/
proc sort data=rsa1;by ident_rsa;run;
data rsa2; 
	merge 	rsa1(in=a) 
			optim; 
	by ident_rsa; 
	if a;
	/*on affecte aux foyers qui perdent une pac les montants de rsa trimestriels calcul�s apr�s son exclusion en on enl�ve une pac */
	if exclu then do; 
		m_rsa_socle1=m_rsasocl_exclu1; 
		m_rsa_socle2=m_rsasocl_exclu2;
		m_rsa_socle3=m_rsasocl_exclu3; 
		m_rsa_socle4=m_rsasocl_exclu4;
		m_rsa1=m_rsa_exclu1; 
		m_rsa2=m_rsa_exclu2;
		m_rsa3=m_rsa_exclu3; 
		m_rsa4=m_rsa_exclu4;
		pac=pac-1;
	end;
	run;

/*5. cr�ation de la table de sortie modele.basersa*/
data modele.basersa;
	set rsa2(keep=ident_rsa m_rsa1-m_rsa4 m_rsa_socle1-m_rsa_socle4 FL_theorique rsa_noel enf03 pers_iso e_c separation wpela&anr2.
		rename=(m_rsa1-m_rsa4 = m_rsa_th1-m_rsa_th4 m_rsa_socle1-m_rsa_socle4 = m_rsa_socle_th1-m_rsa_socle_th4)); 
	format ident $8.;
	ident=substr(ident_rsa,1,8);
	label	m_rsa_th1='montant de RSA theorique au T1 avant calcul du FL'
			m_rsa_th2='montant de RSA theorique au T2 avant calcul du FL'
			m_rsa_th3='montant de RSA theorique au T3 avant calcul du FL'
			m_rsa_th4='montant de RSA theorique au T4 avant calcul du FL'
			m_rsa_socle_th1='montant de RSA socle theorique au T1 avant calcul du FL'
			m_rsa_socle_th2='montant de RSA socle theorique au T2 avant calcul du FL'
			m_rsa_socle_th3='montant de RSA socle theorique au T3 avant calcul du FL'
			m_rsa_socle_th4='montant de RSA socle theorique au T4 avant calcul du FL'
			FL_theorique='forfait logement theorique avant calcul des AL'
			rsa_noel='prime de Noel du RSA'
			enf03='enfants de moins de 3 ans dans le foyer'
			pers_iso='foyer monoparental'
			e_c='enfants � charge dans le foyer'
			separation='une separation conjugale au sens fiscal est intervenue cette annee';
	run;


/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
