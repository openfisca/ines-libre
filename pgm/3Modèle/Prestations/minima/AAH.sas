/************************************************************************/
/*																		*/
/*								AAH										*/
/*																		*/
/************************************************************************/


/* Mod�lisation de l'AAH    											*/
/* En entr�e : 	modele.baseind 											*/
/*				base.baseind											*/
/*				modele.basefam 											*/
/*				modele.rev_imp&anr.										*/
/* En sortie : 	modele.baseind                                     		*/

/* Remarque : d'apr�s l'article L. 821-1 du Code de la S�curit� sociale l'AAH est subsidiaire
			par rapport � un avantage vieilliesse (comme l'ASPA) ou un avantage invaliditi� (comme l'ASI).
			Un demandeur doit donc d'abord faire valoir ses droits � l'ASI ou l'ASPA avant l'AAH.
			Toutefois, si ces droits sont inf�rieurs � ceux de l'AAH � taux plein, l'AAH est vers�e
			sur le principe d'une allocation diff�rentielle. C'est pour calculer cette AAH diff�rentielle que
			l'on tient compte des montants per�us d'ASI ou d'ASPA (alors m�me que l'ASI et l'ASPA n'entrent pas
			th�oriquement dans la base ressource de l'AAH 				*/

/************************************************************************/
/* PLAN	:																*/
/* 	I - Construction du revenu (� partir de celui de la Paje)			*/ 
/* 	II - Calcul d'un abattement  										*/
/*	III - Calcul des montants d'AAH 									*/
/************************************************************************/

/************************************************************************/
/* 	I - Construction du revenu (� partir de celui de la Paje)			*/ 
/************************************************************************/
proc sort data=modele.baseind(keep=ident noi noicon ident_fam naia handicap quelfic aspa elig_aspa_asi  
		where=((handicap ne 0) & (quelfic not in ('FIP','EE_NRT')) ))
	out=Handicape; 
	by ident noi; 
	run;
proc sort data=modele.basefam(keep=ident_fam age_enf pers_iso) out=basefam;
	by ident_fam;
	run;

proc sql;
	create table revcatdecl1_ind as
		select a.ident, a.noi, a.persfip, case 	when (a.persfip1="decl") then (b.rev_cat1)
												when (a.persfip1="conj") then (b.rev_cat2)
												when (a.persfip1="p1") then (b.rev_cat3)
												when (a.persfip1="p2") then (b.rev_cat4)
												else 0 end as rev_cat
		from base.baseind(keep=ident noi persfip: declar1) as a 
		inner join modele.rev_imp&anr.(keep=declar rev_cat:) as b
		on a.declar1=b.declar
		order by ident,noi;
	create table revcatdecl2_ind as
		select a.ident, a.noi, a.persfip, case 	when (a.persfip2="decl") then (b.rev_cat1)
												when (a.persfip2="conj") then (b.rev_cat2)
												when (a.persfip2="p1") then (b.rev_cat3)
												when (a.persfip2="p2") then (b.rev_cat4)
												else 0 end as rev_cat
		from base.baseind(keep=ident noi persfip: declar2 where=(declar2 ne '')) as a 
		inner join modele.rev_imp&anr.(keep=declar rev_cat:) as b
		on a.declar2=b.declar
		order by ident,noi;
	create table revcat_ind as 
		select a.ident, a.noi, sum(a.rev_cat,b.rev_cat) as rev_cat
		from revcatdecl1_ind as a full join revcatdecl2_ind as b
		on a.ident=b.ident and a.noi=b.noi;
	create table handicape_ as
		select a.*,b.rev_cat
		from handicape as a inner join revcat_ind as b
		on a.ident=b.ident and a.noi=b.noi;
	create table FamEligAAH as
		select a.*, coalesce(b.rev_cat_conj,0) as rev_cat_conj 
		from handicape_ as a left join revcat_ind (rename=(noi=noicon rev_cat=rev_cat_conj)) as b
		on a.ident=b.ident and a.noicon=b.noicon
		order by ident_fam;
	quit;


/************************************************************************/
/* 	II - Calcul d'un abattement 	 									*/
/************************************************************************/
data FamEligAbatt; 
	set FamEligAAH;
	by ident_fam; 
	retain res_aah nb_elig_aah conjoint_aah 0; 
	/*Abattement de 20 % appliqu� aux:
	- pensions per�ues par l'allocataire
	- rentes viag�res � titre gratuit per�ues par l'allocataire 
	- revenus per�us par conjoint/concubin/partenaire li� par un pacte civil qui n'est pas allocataire de l'AAH.*/
	if first.ident_fam then do; 
		res_aah=0;
		nb_elig_aah=0;
		conjoint_aah=0;
		end;
	/* la condition conjoint_aah ne 1 permet que pour un couple d'�ligibles � l'AAH, 
		on ne fasse la somme de ses revenus et de celui de son conjoint qu'une seule fois */
	if (handicap ne 0 & conjoint_aah ne 1) then res_aah=res_aah + max(0,0.8*sum(rev_cat,rev_cat_conj) + aspa);
	if handicap ne 0 & (&anref.-input(naia,4.)<60 ! (elig_aspa_asi=0 & &anref.-input(naia,4.)<65)) then do;
		nb_elig_aah=nb_elig_aah+1;
		if noicon ne '' then conjoint_aah=1;
		end;
	if last.ident_fam & nb_elig_aah>0;
	run;


/************************************************************************/
/*	III - Calcul des montants d'AAH 									*/
/************************************************************************/
proc sort data=Handicape; by ident_fam; run;
data aah;
	merge 	basefam(keep=ident_fam age_enf pers_iso) 
			FamEligAbatt(in=b)
			Handicape (in=a);
	by ident_fam; 
	if a & b;
	if pers_iso=. then pers_iso=0; 

	%nb_enf(e_c,0,&age_pf.,age_enf); 
	aah=0;
	if res_aah<&aah_plf.*(2-pers_iso+0.5*e_c) then do; /*plafond doubl� pour un couple ie si pers_iso=0*/
		aah=min(&aah_mont.,&aah_plf.*(2-pers_iso+0.5*e_c)-res_aah);
		if aah>=&aah_mont. then aah_taux='p'; else aah_taux='r';
		end; 
	caah=0;
	label	aah=	"Allocation de l'adulte handicap�"
			caah=	"Compl�ments d'AAH";
	run;

/* Sauvegarde */
proc sort data=aah; by ident noi; run; 
proc sort data=modele.baseind; by ident noi; run; 
data modele.baseind;
	merge 	modele.baseind 
			aah(in=a keep=ident noi aah caah aah_taux);
	by ident noi;
	if not a then do; 
		aah=0;
		caah=0;
		aah_taux='';
		end; 
	run;


/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
