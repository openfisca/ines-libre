*************************************************************************************;
/*																					*/
/*				APPLICATION DU TAUX DE NON RECOURS DU RSA							*/
/*								 													*/
*************************************************************************************;

/* En entr�e : imput.recours_rsa 
			   modele.basersa					               	*/
/* En sortie : modele.rsa    

Les �ligibles non recourants au RSA activit� ne recourent pas au socle non plus. 
Leur �ligibilit� est conserv�e dans une variable d�di�e.
*/

*****************************************************************;


proc sort data=imput.recours_rsa ;by ident_rsa;run;
proc sort data=modele.basersa ;by ident_rsa;run;

data modele.rsa;
	merge modele.basersa (keep=ident_rsa ident pers_iso forf_log enf03 e_c separation
		     			  m_rsa_socle1-m_rsa_socle4 rsasocle rsaact_eli1-rsaact_eli4 
						  rsaact_eli rsa_noel wpela&anr2.)
		  imput.recours_rsa;
	by ident_rsa;

	%macro application_recours;
		%do i=1 %to 4;		
			rsaact&i.=rsaact_eli&i.*(substr(recours_rsa,&i.,1)='1');
			rsasocle_eli&i.=m_rsa_socle&i.; 
			if rsaact_eli&i.>0 & rsaact&i.=0 then m_rsa_socle&i. = 0;
		%end;
		%mend; %application_recours;
	if m_rsa_socle4=0 then rsa_noel=0; 
	rsaact=sum(0, of rsaact1-rsaact4);
	nonrec_socle=(rsasocle_eli1>m_rsa_socle1 ! rsasocle_eli2>m_rsa_socle2 ! rsasocle_eli3>m_rsa_socle3 ! rsasocle_eli4>m_rsa_socle4);
	rsasocle=sum(0, of m_rsa_socle1-m_rsa_socle4);
run;


/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
