/************************************************************************************/
/*																					*/
/*								enfant_a_naitre										*/
/*																					*/
/************************************************************************************/
/* Ajout des enfants "� naitre"	en janvier et f�vrier de l'ann�e suivante (suite aux*/ 
/* programmes probabilit�_imputation_enfant)										*/
/* En entr�e :  imput.imputnais_m 													*/
/*				travail.menage&anr. 												*/		
/*				modele.basefam 														*/	
/* En sortie :  imput.basefam 														*/
/*				modele.basefam				                     					*/
/************************************************************************************/

data imput(drop=tri proba_enf); 
	set imput.impunais_m; 
	if tri>0; 
	alea=ranuni(1);
run; 

proc sort data=imput; by ident; run;
proc sort data=travail.menage&anr.; by ident; run;
data imput; 
	merge 	imput(in=b) 
			travail.menage&anr. (in= a keep=ident module);
	by ident; 
	if a & b & module<=1;
run;

/*data enf_a_naitre; set travail.irf&anr.e&anr.(keep=ident choixech noi naia naim);
if naia=&anref.+1 & naim<3; run;*/

/* TODO : De la m�me fa�on que dans enfant_fip, on devrait cr�er des lignes pour ces enfants imput�s.
Pour l'instant, on intervient apr�s la cr�ation des familles donc ce n'est pas le cas. Ce n'est pas id�al, il faudrait donc s'en charger un jour */

proc sort data=modele.basefam; by ident_fam;run; 
data basefam; 
	set modele.basefam(keep=ident_fam); 
	by ident_fam;
	if ident_fam ne '';
	ident=substr(ident_fam,1,8);
run; 
proc sort data=basefam; by ident;run; 
proc sort data=imput; by ident;run;
data basefam(drop=alea); 
	merge 	basefam 
			imput(in=a keep=ident alea); 
	by ident;
	if a & first.ident ;
	if alea <0.5 then mois_1='01'; 
	if alea>=0.5 then mois_1='02'; 
run; 

proc sort data=basefam; by ident_fam;run; 
data imput.basefam; 
	merge  	modele.basefam (keep=ident_fam enf_1)
			basefam(in=a keep=ident_fam mois_1); 
	by ident_fam;
	if a;
	if mois_1 ne '' then do; mois_1=mois_1; enf_1=enf_1+1; end;
run; 

/* ajout des nouvelles informations � modele.basefam */
data modele.basefam;
	merge	modele.basefam (drop=mois_1 enf_1)
			imput.basefam;
	by ident_fam;
	if enf_1 ^= 1 then enf_1=0;
	label	enf_1="enfant � naitre en janvier ou f�vrier de l'ann�e suivante"
			mois_1="mois de naissance de l'enfant � na�tre";
run;


/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
