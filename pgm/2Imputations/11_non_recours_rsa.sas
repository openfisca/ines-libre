
/*******			  						********
	*****	*****	*****	*****	*****	*****	
		**		**		**		**		**		**

	      TIRAGE DU RECOURS AU RSA ACTIVITE

   **	   **	   **	   **	   **	   **		
	*****	*****	*****	*****	*****	*****
	  ********		 						 ********/

/* 
En entr�e :	modele.basersa
			base.menage&anr2
En sortie : imput.recours_rsa


PLAN du programme

	0 :	 Texte des macros de tirage des b�n�ficiaires du RSA activit�
	I.	 Tirage du recours pour le T1
	II.	 Tirage du recours pour le T2
	III. Tirage du recours pour le T3
	IV.	 Tirage du recours pour le T4


HYPOTHESES
	- on tire un nombre de foyers chaque trimestre parmi deux sous populations : �ligibles ou non au RSA socle
	- les �ligibles au RSA socle non recourants au RSA activit� ne recourront pas au socle (pgm application non recours)
	- chaque trimestre, sont consid�r�s d'office comme b�n�ficiaires du RSA activit� les foyers �ligibles
		au RSA activit� d�j� tir�s un trimestre pr�c�dent (quel que soit leur statut via-�-vis du RSA socle)
	- pour les foyers � tirer, la probabilit� d'�tre d�sign� augmente avec le montant de rsa activit� */


/***************************
0 : Texte de la macro de tirage des b�n�ficiaires du RSA activit� selon qu'il soient :
	- �ligibles au RSA socle le m�me trimestre
	- non �ligibles au socle le m�me trimestre 
****************************/


%macro tirage(table,nb_a_atteindre,trimestre,quel_rsa);
	%if %sysfunc(round(%sysevalf(&nb_a_atteindre./10000),1))=0 %then %do;
		data &quel_rsa.t&trimestre.;
			set _null_; 
			ident_rsa='';
			ident='';
			run ;

		data surplus_&quel_rsa.t&trimestre.; 
			set _null_; 
			ident_rsa='';
			ident='';
			run;
		%end;
		
	%else %do;
		data base;
			set &table.;
			if rsaact_eli&trimestre.>0;
			benef=(rsaact_eli&trimestre.>0);
		run;

		%if &quel_rsa.=act_seul %then %do;
			data base;
				set base;
				if m_rsa_socle&trimestre.>0 then delete;
			%end;

		%if &quel_rsa.=deux_rsa %then %do;
			data base;
				set base;
				if m_rsa_socle&trimestre.=0 then delete;
			%end;
		%end;

	/* pour le cas o� le nb est n�gatif */
	%if %sysfunc(round(%sysevalf(&nb_a_atteindre./10000),1))<0 %then %do;

		data trop;
			set base;
			alea=ranuni(1);
		run;
		proc sort data=trop; by alea; run;

		data surplus_&quel_rsa.t&trimestre. (keep=ident_rsa ident benefi); 
			set trop;
			retain benefi 0;
			benefi=benefi+wpela&anr2.;
			if benefi > abs(&nb_a_atteindre.) then delete;
		run;
		proc sort data=surplus_&quel_rsa.t&trimestre.; by ident_rsa; run;

		/* on cr�e une table de b�n�ficiaires tir�s vide pour qu'elle existe*/
		data &quel_rsa.t&trimestre.; 
			set _null_; 
			ident_rsa=''; ident='';
		run; 
		
	%end;

	/* pour le cas o� le nb est positif */
	%if %sysfunc(round(%sysevalf(&nb_a_atteindre./10000),1))>0 %then %do;  

		/*exclusion des personnes ayant d�ja recouru dans l'ann�e*/
		%if &trimestre.>1 %then %do;
			data base; 
				merge 	base (in=a)
						rsa (keep=ident_rsa recours_rsa);
				by ident_rsa;
				if a;
				if count(recours_rsa,'1')>0 then delete;
			run;
		%end;
		
		/*on r�cup�re le montant maximum de RSA activit� observ� */
		%global max_&trimestre. ;
		proc sql noprint;
			select max(rsaact_eli&trimestre) into: max_&trimestre.  from base;
		quit;

		%global poids_moy ;
			proc sql noprint;
				select mean(wpela&anr2.) into: poids_moy from base;
		quit;
		/* on r�cup�re le poids moyen des foyers dans base, pour d�terminer le nombre 
		d'observations � tirer pour obtenir environ 10 000 foyers RSA */
		%let borninf = %sysevalf(10000/(%sysevalf(&poids_moy.,floor)+40),floor);
		%let bornsup = %sysevalf(10000/(%sysevalf(&poids_moy.,floor)-40),floor); 

		data base;
			set base;
			proba=rsaact_eli&trimestre./ &&max_&trimestre.;
		run;
		proc sort data=base;by ident_rsa;run;

		data tirage;set base;run;
		option nonotes; 

		%do j=1 %to %sysfunc(round(%sysevalf(&nb_a_atteindre./10000),1));
			%let diff_min=10000;

			data in; set tirage; run;

			%do i=%eval(&borninf.) %to %eval(&bornsup.) %by 1;
				proc surveyselect data=in out=echantillon method=pps_sys seed=5 ranuni n=&i noprint;
				size proba; 
				run;
				proc means data=echantillon noprint; var wpela&anr2.; output out=sum sum=sum;
				data _null_; set sum; call symput('diff',compress(round(abs(10000-sum)))); run;
				%if &diff.<&diff_min. %then %do;
					%let i_min=&i; %let diff_min=&diff;
					data echantillon&j.; set echantillon; run;
				%end;
				%put i=&i diff=&diff diff_min=&diff_min;
			%end;
			proc sort data=tirage; by ident_rsa;
			proc sort data=echantillon&j.; by ident_rsa;
			data tirage; 
				merge tirage echantillon&j.(in=z keep=ident_rsa); 
				by ident_rsa; 
				if not z;
			run;
		%end;
		option notes;

		data &quel_rsa.t&trimestre.(keep=ident_rsa ident wpela&anr2.);
			set %do j=1 %to %sysfunc(round(%sysevalf(&nb_a_atteindre./10000),1)); echantillon&j. %end;;
		run;
		proc sort nodupkey; by ident_rsa ; run;

		/* compl�ment par tirage al�atoire pour atteindre la cible le cas �ch�ant*/
		proc means data= &quel_rsa.t&trimestre. noprint;
			var wpela&anr2.; 
			output out=sum sum=sum;

		data _null_; set sum; call symput('manque',(round(&nb_a_atteindre.-sum))); run;
		%put &manque;
		%if &manque>0 %then %do;

			data reste (keep=ident_rsa ident wpela&anr2. alea);
				merge	base 
						&quel_rsa.t&trimestre.(in=a);
				by ident_rsa;
				if not a;
				alea=ranuni(1);
			run;

			proc sort data=reste; by alea; run;

			data reste_&quel_rsa.t&trimestre.; 
				set reste;
				retain benefi 0;
				benefi=benefi+wpela&anr2.;
				if benefi > (&manque.) then delete;
			run;

			proc sort data=reste_&quel_rsa.t&trimestre.; by ident_rsa; run;

			data &quel_rsa.t&trimestre.;
				merge	&quel_rsa.t&trimestre. 
						reste_&quel_rsa.t&trimestre.;
				by ident_rsa;
			run;
		%end;

		/* on cr�e une table de b�n�ficiaires en trop vide pour qu'elle existe*/
		data surplus_&quel_rsa.t&trimestre.; 
			set _null_; 
			ident_rsa=''; ident='';
		run;
	%end;
	option notes;
%mend tirage;

%macro Tirage_NR_Trim;

	/*******************************************************
	 I. TIRAGE DU RECOURS AU T1
	*******************************************************/

	%tirage(modele.basersa,&eff_act_seul_t1.,1,act_seul); run;
	%tirage(modele.basersa,&eff_deux_rsa_t1.,1,deux_rsa); run;


	/* Cr�ation et alimentation de la table rsa */
	data rsa (keep=ident_rsa ident pers_iso forf_log enf03 m_rsa_socle1-m_rsa_socle4 rsasocle
		 rsaact_eli1-rsaact_eli4 rsaact_eli rsa_noel recours_rsa wpela&anr2.);
		merge		modele.basersa(in=e) 
					act_seult1 (in=a)
					deux_rsat1 (in=b) ; 
		by ident_rsa;
		if e;
		recours_rsa='0000';
		if a ! b then recours_rsa='1000';
		run;


	/*******************************************************
	 II. TIRAGE DU RECOURS AU T2
	*******************************************************/

	/* d�compte des b�n�ficiaires d'office au t2 */
	proc means data=rsa;
		var wpela&anr2.;
		where (rsaact_eli2>0 & m_rsa_socle2=0 & count(recours_rsa,'1')) ;
		output out=tas2(drop=_type_ _freq_) sum=benas2; 
		run;

	proc means data=rsa;
		var wpela&anr2.;
		where (rsaact_eli2>0 & m_rsa_socle2>0 & count(recours_rsa,'1')) ;
		output out=tdr2(drop=_type_ _freq_) sum=bendr2; 
		run;

	/* d�compte par soustraction du nombre de b�n�ficiaires restant � tirer */
	data _null_;
		if 0 then set tas2 nobs=NbObs;
		if NbObs=0 then do; call symputx("atireras2",&eff_act_seul_t2.);end;
		else do; set tas2; call symputx("atireras2",(round(&eff_act_seul_t2.-benas2))); end;
		stop;
		run;
	%put  &atireras2;

	%tirage(modele.basersa,&atireras2.,2,act_seul); run;

	data _null_;
		if 0 then set tdr2 nobs=NbObs;
		if NbObs=0 then do; call symputx("atirerdr2",&eff_deux_rsa_t2.);end;
		else do; set tdr2; call symputx("atirerdr2",(round(&eff_deux_rsa_t2.-bendr2))); end;
		stop;
		run;
	%put  &atirerdr2;

	%tirage(modele.basersa,&atirerdr2.,2,deux_rsa); run;

	/* alimentation de la table rsa */
	data rsa ; 
		merge 	rsa(in=e) 
				act_seult2 (in=a)
				deux_rsat2 (in=b)  
				surplus_act_seult2 (in=c)
				surplus_deux_rsat2 (in=d) ;
		by ident_rsa;
		if e;
		if a ! b ! (rsaact_eli2>0 & count(recours_rsa,'1')) then recours_rsa=substr(recours_rsa,1,1)||'100';
		if c ! d then recours_rsa=substr(recours_rsa,1,1)||'000';
		run;


	/*******************************************************
	 III. TIRAGE DU RECOURS AU T3
	*******************************************************/

	/* d�compte des b�n�ficiaires d'office au t3 */
	proc means data=rsa;
		var wpela&anr2.;
		where (rsaact_eli3>0 & m_rsa_socle3=0 & count(recours_rsa,'1')) ;
		output out=tas3(drop=_type_ _freq_) sum=benas3; 
		run;

	proc means data=rsa;
		var wpela&anr2.;
		where (rsaact_eli3>0 & m_rsa_socle3>0 & count(recours_rsa,'1')) ;
		output out=tdr3(drop=_type_ _freq_) sum=bendr3; 
		run;

	/* d�compte par soustraction du nombre de b�n�ficiaires restant � tirer */
	data _null_;
		if 0 then set tas3 nobs=NbObs;
		if NbObs=0 then do; call symputx("atireras3",&eff_act_seul_t3.);end;
		else do; set tas3; call symputx("atireras3",(round(&eff_act_seul_t3.-benas3))); end;
		stop;
		run;
	%put  &atireras3;

	%tirage(modele.basersa,&atireras3.,3,act_seul); run;

	data _null_;
		if 0 then set tdr3 nobs=NbObs;
		if NbObs=0 then do; call symputx("atirerdr3",&eff_deux_rsa_t3.);end;
		else do; set tdr3; call symputx("atirerdr3",(round(&eff_deux_rsa_t3.-bendr3))); end;
		stop;
		run;
	%put  &atirerdr3;

	%tirage(modele.basersa,&atirerdr3.,3,deux_rsa); run;

	/* alimentation de la table rsa */
	data rsa ; 
		merge 	rsa(in=e) 
				act_seult3 (in=a)
				deux_rsat3 (in=b) 
				surplus_act_seult3 (in=c)
				surplus_deux_rsat3 (in=d) ; 
		by ident_rsa;
		if e;
		if a ! b ! (rsaact_eli3>0 & count(recours_rsa,'1')) then recours_rsa=substr(recours_rsa,1,2)||'10';
		if c ! d then recours_rsa=substr(recours_rsa,1,2)||'00';
		run;


	/*******************************************************
	 IV. TIRAGE DU RECOURS AU T4
	*******************************************************/

	/* d�compte des b�n�ficiaires d'office au t4 */
	proc means data=rsa;
		var wpela&anr2.;
		where (rsaact_eli4>0 & m_rsa_socle4=0 & count(recours_rsa,'1')) ;
		output out=tas4(drop=_type_ _freq_) sum=benas4; 
		run;

	proc means data=rsa;
		var wpela&anr2.;
		where (rsaact_eli4>0 & m_rsa_socle4>0 & count(recours_rsa,'1')) ;
		output out=tdr4(drop=_type_ _freq_) sum=bendr4; 
		run;

	/* d�compte par soustraction du nombre de b�n�ficiaires restant � tirer */
	data _null_;
		if 0 then set tas4 nobs=NbObs;
		if NbObs=0 then do; call symputx("atireras4",&eff_act_seul_t4.);end;
		else do; set tas4; call symputx("atireras4",(round(&eff_act_seul_t4.-benas4))); end;
		stop;
		run;
	%put  &atireras4;

	%tirage(modele.basersa,&atireras4.,4,act_seul); run;


	data _null_;
		if 0 then set tdr4 nobs=NbObs;
		if NbObs=0 then do; call symputx("atirerdr4",&eff_deux_rsa_t4.);end;
		else do; set tdr4; call symputx("atirerdr4",(round(&eff_deux_rsa_t4.-bendr4))); end;
		stop;
		run;
	%put  &atirerdr4;

	%tirage(modele.basersa,&atirerdr4.,4,deux_rsa); run;

	/* alimentation de la table rsa */
	data rsa ; 
		merge 	rsa(in=e) 
				act_seult4 (in=a)
				deux_rsat4 (in=b) 
				surplus_act_seult4 (in=c)
				surplus_deux_rsat4 (in=d) ; 
		by ident_rsa;
		if e;
		if a ! b ! (rsaact_eli4>0 & count(recours_rsa,'1')) then recours_rsa=substr(recours_rsa,1,3)||'1';
		if c ! d then recours_rsa=substr(recours_rsa,1,3)||'0';
		run;

	/* sauvegarde dans la table imput.recours_rsa */
	data imput.recours_rsa;
		merge		modele.basersa (in=a keep=ident_rsa)	
					rsa (keep= ident_rsa recours_rsa);
		by ident_rsa;
		if a;
	run;

	proc datasets lib=work
		memtype=DATA;
		delete echantillon:;
		quit;

	%mend Tirage_NR_Trim;

%Tirage_NR_Trim;


/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
