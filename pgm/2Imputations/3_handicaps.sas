*************************************************************************************;
/*																					*/
/*									3_handicaps.sas									*/
/*								 													*/
*************************************************************************************;

/* Rep�rage des personnes en situation de handicap									*/ 
/* En entr�e : 	rpm.menage&anr														*/ 
/*				travail.rev															*/ 
/*				imput.baseind 														*/
/*				travail.irf&anr.e&anr.								           		*/
/* En sortie : 	imput.handicap														*/
/*				imput.enfant_h                                 						*/

/* PLAN : 
1 - Adultes handicap�s
 a) M�nages percevant l'AAH ou l'AEEH selon la CAF (uniquement sur le noyau)
 b) Individus d�clarant percevoir de l'AAH dans l'enqu�te emploi
 c) Individus handicap�s sur les d�clarations fiscales pour les adultes et les enfants
 d) Constitution table individu adulte handicap�

2 - Enfants handicap�s
 a) Tirage al�atoire d'enfants handicap�s dans les extensions
 b) Traitement des enfants handicap�s CAF dont on ne connait pas le num�ro individuel*/

/* NOTE 
- Dans les cases fiscales signalant des personnes � charge handicap�s se trouvent des 
enfants qui ont plus de 21 ans et qui sont donc �ligibles � l'AAH et non � l'AEEH. 
On traite les deux cas en en m�me temps dans la premi�re partie.					*/


/*variable de sortie : 
handicap 	(1 = handicap identifi� dans l'enqu�te emploi, 
			 2=  handicap identifi� dans les d�clarations fiscales mais pas dans l'enqu�te emploi)
handicap_caf (1= appartient au fichier CAF, 2= non)

handicap_e 	('source CNAF','source ficale','tirage au sort'); */

**************************************************************************************; 





/* 1 - Adultes handicap�s 			*/
/* a) M�nages percevant l'AAH ou l'AEEH selon la CAF (uniquement sur le noyau) */
%macro nom_variable_hand;/* Prise en compte du changement de nom de variables de l'ERFS */
	%global m_aah m_aeehm;
	%let m_aah = m_aahm;
	%let m_aeehm= m_aeehm;
	%if &anref. = 2008 %then %do;
		%let m_aah = m_aah_caahm;
		%end;
	%if &anref. <= 2007 %then %do;
		%let m_aeehm= m_aesm;
		%end;
	%mend;
%nom_variable_hand;

data menage_AAH_caf menage_AEEH_caf; 
	set rpm.menage&anr.(keep=ident&anr. &m_aah. &m_aeehm. rename=(ident&anr.=ident));
	if &m_aah.>0 then output menage_AAH_caf;
	if &m_aeehm.>0 then output menage_AEEH_caf;
	run;

/* b) Individus d�clarant percevoir de l'AAH dans l'enqu�te emploi 			*/ 
data individu_handicap_EE;
	set travail.rev(keep = ident noi cal_maahe cal_aah);
	if (index(cal_maahe,'1')>0 ! index(cal_aah,'1')>0 );
	if ident='08035820' & noi='03' then delete; 		/*on supprime les cas � 3 AAH dans le foyer*/ 
	if ident='08077079' & noi='03' then delete; 
	run;

/* c) Individus handicap�s sur les d�clarations fiscales pour les adultes et les enfants */ 
data individu_handicap_fisc;
	set imput.baseind(keep=ident noi declar1 declar2 sif1 sif2 naia anaisenf1 anaisenf2 persfip persfipd wpela&anr.);

	/* D�clarant : la case P indique s'il est titulaire d'une pension d'invalidit� d'au moins 40% ou carte d'invalidit� au moins 80% */
	if substr(sif1,21,1)='P' & noi=substr(declar1,1,2) then ah=1;	
	if substr(sif2,21,1)='P' & noi=substr(declar2,1,2) then ah=1;	

	/* Conjoint du d�clarant : la case F indique la m�me chose que la case P pour le conjoint */
	if substr(sif1,17,1)='F' & noi ne substr(declar1,1,2) & naia=substr(sif1,11,4) then ah=1;
	if substr(sif2,17,1)='F' & noi ne substr(declar2,1,2) & naia=substr(sif2,11,4) & persfipd ne '' then ah=1;

	/* Personnes � charge (qui peuvent �tre adultes (+21 ans))*/ 
	debut_F=find(sif1,' F0',20); /* � partir du 20�me caract�re car certains cas sont bizarres (on peut trouver un 'F' trop t�t dans le sif) */
	if debut_F=0 then debut_F=find(sif1,' F1',20);
	if debut_F ne 0 then do;
		nbenf1=substr(sif1,debut_F+1,24);
		nbg1=input(substr(nbenf1,5,2),2.); /* nombre d'enfants non mari�s � charge titulaires de la carte d'invalidit� sur declar1 */
		nbr1=input(substr(nbenf1,8,2),2.); /* nombre de personnes invalides vivant sous le m�me toit sur declar1*/
		nbi1=input(substr(nbenf1,20,2),2.); /* nombre d'enfants non mari�s en r�sidence altern�e titulaires de la carte d'invalidit�, sur declar1 */
		end;
	else do; nbg1=0; nbr1=0; nbi1=0; end;

	debut_F=find(sif2,' F0',20); /* � partir du 20�me caract�re car certains cas sont bizarres (on peut trouver un 'F' trop t�t dans le sif) */
	if debut_F=0 then debut_F=find(sif2,' F1',20);
	if debut_F ne 0 then do;
		nbenf2=substr(sif2,debut_F+1,24);
		nbg2=input(substr(nbenf2,5,2),2.);
		nbr2=input(substr(nbenf2,8,2),2.);
		nbi2=input(substr(nbenf2,20,2),2.);
		end;
	else do; nbg2=0; nbr2=0; nbi2=0; end;

	if nbg1>0 ! nbr1>0 ! nbi1>0 ! nbg2>0 ! nbr2>0 ! nbi2>0 then do; /*Identification d'une personne a charge invalide dans le foyer */
		do i=0 to 7;
			if substr(anaisenf1,1+i*5,1) in ('G','R','I') & naia=substr(anaisenf1,2+i*5,4) then do; 
				if &anref.-input(naia,4.) <=&b_age_PF. then eh=1; else ah=1;
				end;
			end;
		do i=0 to 7;
			if substr(anaisenf2,1+i*5,1) in ('G','R','I') & naia=substr(anaisenf2,2+i*5,4) then do; 
				if &anref.-input(naia,4.) <=&b_age_PF. then eh=1; else ah=1;
				end;
			end;
		end;
	if eh=1 or ah=1; /* adulte/enfant handicap� selon la d�claration fiscale*/
	drop debut_F i;
	run;

/* d ) Constitution table individu adulte handicap� */
proc sort data=individu_handicap_fisc; by ident noi; run; 
proc sort data=individu_handicap_EE; by ident noi; run; 

data individu_handicap_fisc_EE (keep=ident noi handicap); 
	merge 	individu_handicap_fisc(in=a where=(ah=1)) 
			individu_handicap_EE(in=b); 
	by ident noi; 
	if b then handicap=1; 
	else handicap=2; * appartient aux fichiers fiscaux mais pas � l'enqu�te emploi; 
	run;

data individu_handicap_fisc_EE (keep= ident noi handicap handicap_caf); 
	merge 	individu_handicap_fisc_EE (in=a) 
			menage_AAH_caf (in=b); 
	by ident; 
	if b then handicap_caf=1; * appartient aux fichiers CAF; 
	if a;
	run;

/* sauvegarde */
data imput.handicap;
	set individu_handicap_fisc_EE (in=a);
	if handicap=. then handicap=0; 
	if handicap_caf=. then handicap_caf=0;
	run;


********************************************************************************************************; 

/* 2 - enfants handicap�s */

/* a) Tirage al�atoire d'enfants handicap�s dans les extensions */ 

/* 	Si un enfant a un taux d'incapacit� compris entre 50% et 80% et qu'il fr�quente une �cole sp�cialis�e ou que son �tat 
	n�cessite des soins, il donne droit � l'AEEH mais ne poss�dera pas de carte d'invalidit� (pour laquelle il faut un taux
	d'incapacit� sup�rieur � 80%). Sans carte d'invalidit�, il ne peut �tre d�clar� comme invalide au fisc. 
	De ce fait, et peut-�tre � cause d'une sous-d�claration fiscale, il manque des enfants handicap�s qui permettraient
	de toucher l'AEEH qu'on souhaite simuler (il semble qu'on a qu'un tiers des enfants �ligibles � l'AEEH avec les d�clarations fiscales). 
	On choisit donc de tirer au hasard des enfants dont le taux d'incapacit� serait compris entre 50% et 80% dans l'extension.
	On en tire N1-N2/2 o� N1 est le nombre de m�nages percevant l'AEEH (noyau uniquement) et N2 est le nombre d'enfants invalides selon 
	les d�clarations fiscales (donc N2/2 est environ le nombre d'enfants invalides dans les d�clarations fiscales dans le noyau). */

data _null_ ;
	set menage_AEEH_caf;
	call symputx('nb_menage_AEEH_caf',_N_);
	run;
data _null_ ;
	set individu_handicap_fisc(in=a where=(eh=1));
	call symputx('nb_enf_handicap_fisc',_N_);
	run;

proc sort data=imput.baseind; by ident noi; run;
proc sort data=travail.irf&anr.e&anr.; by ident noi; run;
data tirage;
	merge 	imput.baseind (keep=naia noi ident wpela&anr. in=a) 
			travail.irf&anr.e&anr(keep=ident noi choixech);  
	by ident noi; 
	if &anref.-input(naia,4.)<=&b_age_PF. and choixech in ('EXST1','EXST2','EXST3','EXET1','EXET2','EXET3') and a; 
	tirage=ranuni(1); 
	run;

proc sort data=tirage; by tirage; run; 
data enfant_handicap_aleat; 
	set tirage (obs=%eval(&nb_menage_AEEH_caf.-&nb_enf_handicap_fisc./2));
	run; 

proc sort data=enfant_handicap_aleat; by ident noi; run; 
data enfant_handicap(keep= ident noi handicap_e wpela&anr.); 
	merge 	menage_AEEH_caf(in=a) 
			individu_handicap_fisc(in=b where=(eh=1))
			enfant_handicap_aleat(in=c); 
	by ident; 
	length handicap_e $ 14;
	if a then handicap_e='source CNAF'; /* m�nage percevant l'AEEH selon la CAF (noyau seulement) */
	else if b then handicap_e='source ficale'; /* enfant hadicap� selon les d�clarations fiscales mais hors m�nages percevant l'AEEH selon la CAF */
	else handicap_e='tirage au sort'; /* enfant tir� au sort */ 
	label handicap_e="Source du handicap";
	run;
/*proc freq; table handicap_e;run;*/

/* b) Traitement des enfants handicap�s CAF dont on ne connait pas le num�ro individuel (donc ceux qui ne sont pas identifi�s comme tels 
ficalement) */ 

/* On choisit un enfant au hasard dans les m�nages percevant de l'AEEH en faisant l'hypoth�se qu'il n'y a qu'un enfant handicap� par 
m�nage, ce qui est faux dans 4% des cas. Etant donn�s les effectifs en jeu (environ 10 observations), on ne raffine pas. */

data tirage2; 
	merge 	enfant_handicap(where=(noi='') in=a) 
			imput.baseind(keep=ident noi persfip naia wpela&anr.); 
	by ident; 
	if a; 
	if &anref.-input(naia,4.)<=&b_age_PF. and persfip in ('pac',''); 
	classement=ranuni(1); 
	run;
proc sort data=tirage2; by ident classement; run; 
data enfant_handicap_CAF; 
	set tirage2;
	by ident; 
	if first.ident;
	run; 

data imput.enfant_h(keep=ident noi handicap_e wpela&anr.); 
	set enfant_handicap(where=(noi ne ''))
		enfant_handicap_CAF;
	run; 

proc sort data=imput.enfant_h; by ident noi; run; 


/****************************************************************
� Logiciel �labor� par l��tat, via l�Insee et la Drees, 2016. 

Ce logiciel est un programme informatique initialement d�velopp� par l'Insee 
et la Drees. Il permet d'ex�cuter le mod�le de microsimulation Ines, simulant 
la l�gislation sociale et fiscale fran�aise.

Ce logiciel est r�gi par la licence CeCILL V2.1 soumise au droit fran�ais et 
respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, 
modifier et/ou redistribuer ce programme sous les conditions de la licence 
CeCILL V2.1 telle que diffus�e par le CEA, le CNRS et l'Inria sur le site 
http://www.cecill.info. 

En contrepartie de l'accessibilit� au code source et des droits de copie, de 
modification et de redistribution accord�s par cette licence, il n'est offert aux 
utilisateurs qu'une garantie limit�e. Pour les m�mes raisons, seule une 
responsabilit� restreinte p�se sur l'auteur du programme, le titulaire des 
droits patrimoniaux et les conc�dants successifs.

� cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au 
chargement, � l'utilisation, � la modification et/ou au d�veloppement et � 
la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de logiciel 
libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
d�veloppeurs et des professionnels avertis poss�dant des connaissances 
informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

Le fait que vous puissiez acc�der � ce pied de page signifie que vous avez pris 
connaissance de la licence CeCILL V2.1, et que vous en avez accept� les
termes.
****************************************************************/
